#!/usr/bin/env bash

IMAGE_NAME=truckviewer-builder
CONTAINER_NAME=truckviewer-prod-build
BUILD_PATH=${1-$(pwd)/public/build/}

mkdir -p ${BUILD_PATH}

docker build -t ${IMAGE_NAME} .

docker rm -f ${CONTAINER_NAME}
docker run --name ${CONTAINER_NAME} -it \
  -v ${BUILD_PATH}:/repo/public/prod \
  ${IMAGE_NAME}
