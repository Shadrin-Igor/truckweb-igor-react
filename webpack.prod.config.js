// Webpack config for production
var path = require('path');
var webpack = require('webpack');
var _ = require('lodash');
var WebpackShellPlugin = require('webpack-shell-plugin');
var envConfig = require('config');

var assetsPath = path.resolve(__dirname, envConfig.get('buildDir'));
var wpCommon = require('./webpack_common.js');

// replace 'babel?cacheDirectory=true' to just 'babel'
wpCommon.module.loaders = wpCommon.module.loaders.map(function (item) {
  if (_.isArray(item.loaders) && item.loaders[0].match(/babel/)) {
    item.loaders = ['babel'];
  }
  return item;
});

module.exports = {
  plugins: wpCommon.plugins.concat([
    new WebpackShellPlugin({
      onBuildStart: [
        // clear old build
        `rm -rf ${assetsPath}/*`,
        // create new build
        `mkdir -p ${assetsPath}`,
        `cp -r -H ${path.resolve(__dirname, './public/assets')} ${assetsPath} 2> /dev/null; exit 0`,
      ],
    }),
  ]),
  cache: false,
  output: {
    path: assetsPath,
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[chunkhash].js',
  },

  devtool: wpCommon.devtool,
  entry: wpCommon.entry,
  module: wpCommon.module,
  resolve: wpCommon.resolve,
  externals: wpCommon.externals,
};
