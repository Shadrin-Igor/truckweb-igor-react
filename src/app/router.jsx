import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { browserHistory, hashHistory } from 'react-router'
import {reducer as formReducer} from 'redux-form';

import { createStore, combineReducers, applyMiddleware } from 'redux';
//import { syncReduxAndRouter, routeReducer } from 'react-router-redux';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

import thunk from 'redux-thunk';

import reducers from 'redux/reducers';

import Routes from './../routes';
import clientMiddleware from './../redux/clientMiddleware.js';


//import actions from 'redux/actions';

// TODO: why here???
//import "../rubix/js/common/defaults.js";

const rootReducer = combineReducers({
  ...reducers,
  form: formReducer,
  routing: routerReducer,
});




var finishPageLoad = () => {
  // TODO: rise event
  console.log('---> finishPageLoad');
  setTimeout(() => {
    $('body').removeClass('fade-out');
  }, 500);
};

var onUpdate = (notReady) => {
  // cleanup (do not modify)
  // TODO: what this????
  // rubix_bootstrap.core.reset_globals_BANG_();
  // if(window.Rubix && notReady) window.Rubix.Cleanup();

  Pace.restart();

  //initGoogleAnalytics();
  if(!notReady) finishPageLoad();
};

export default function () {
  // create store
  const createStoreWithMiddleware = applyMiddleware(clientMiddleware, thunk)(createStore);
  const store = createStoreWithMiddleware(rootReducer);

  // Create an enhanced history that syncs navigation events with the store
  const history = syncHistoryWithStore(hashHistory, store);

  // Configure router
  // var routes = new Routes(history, onUpdate, store);
  // var configuredRoutes = routes.render();
  // configuredRoutes = (
  //   <Provider store={store}>
  //     {configuredRoutes}
  //   </Provider>
  // );

  var configuredRoutes = (
    <Provider store={store}>
      <Routes history={history} onUpdate={onUpdate} store={store}></Routes>
    </Provider>
  );

  // TODO: check
  onUpdate(true);

  ReactDOM.render(configuredRoutes, document.getElementById('app-container'), () => {
    // TODO: check
    finishPageLoad();
  });
};
