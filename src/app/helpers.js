import _ from 'lodash';
import moment from 'moment';

export default {
  getLocalTime(utcDate) {
    var currentOffsetMinutes = moment().utcOffset();
    return moment(utcDate).utcOffset(currentOffsetMinutes).format('LT');
  },

  localToUtcDate(localDate) {
    return moment(localDate).utcOffset(0).format();
  },

  generateDurationString(startDate, endDate) {
    if (!startDate || !endDate) return '';
    return moment(startDate).format('h:mm') + '-' + moment(endDate).format('h:mm');
  }
}
