import React, { PropTypes } from 'react';

import TopBar from './TopBar';
import SecondBar from './SecondBar';
import Sidebar from './Sidebar';
import FetchPreloader from 'components/FetchPreloader/FetchPreloader';
import LayoutBase from './LayoutBase.jsx';

export default class WideLayout extends LayoutBase {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="dashboard-layout" id="body">
        <SecondBar pageHeader={this.state.pageHeader} />
        <TopBar />
        <FetchPreloader />
        <Sidebar />

        <div id="content">
          <div id="wide-layout">
            {this.props.children}
          </div>
        </div>

        {this.renderNotificationSystem()}
      </div>
    );
  }
}
