import _ from 'lodash';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Nav, Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { connect } from 'react-redux'

import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({
    auth: state.auth,
    myCompany: state.myCompany,
  }),
  { getMyCompany }
)
export default class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.props.getMyCompany().then(() => {
      this.forceUpdate();
    });
  }

  render() {
    return (
      <Navbar fixedTop id="top-bar">
        <Navbar.Header>
          <Navbar.Brand>
            <input type="test" placeholder="Search" />
          </Navbar.Brand>
        </Navbar.Header>

        <Nav pullRight>
          <NavItem className="layout__topbar-divider" />

          <NavDropdown id="company-dropdown" eventKey={1} title={' ' + ((this.props.myCompany.company) ? this.props.myCompany.company.name_legal : '')} className="layout__company-settings">
            <li className="layout__company-settings_menu">
              <h3 className="layout__company-settings_main-header">{(this.props.myCompany.company) ? this.props.myCompany.company.name_legal : ''}</h3>
              <Row>
                <Col sm={4}>
                  <h4 className="layout__company-settings_section-header">Your company</h4>
                  <ul className="list-unstyled">
                    <li><Link to={''}>Settings Account</Link></li>
                    <li><Link to={'/company-settings'}>Company</Link></li>
                    <li><Link to={''}>Manage Users</Link></li>
                  </ul>
                </Col>
                <Col sm={4}>
                  <h4 className="layout__company-settings_section-header">Tools</h4>
                  <ul className="list-unstyled">
                    <li><Link to={''}>Import Data</Link></li>
                    <li><Link to={''}>Export Data</Link></li>
                    <li><Link to={''}>Attachments</Link></li>
                    <li><Link to={''}>Audit Log</Link></li>
                  </ul>
                </Col>
                <Col sm={4}>
                  <h4 className="layout__company-settings_section-header">Profile</h4>
                  <ul className="list-unstyled">
                    <li><Link to={''}>User Profile</Link></li>
                    <li><Link to={''}>Feedback</Link></li>
                    <li><Link to={''}>Refer a Friend</Link></li>
                    <li><Link to={''}>Privacy</Link></li>
                  </ul>
                </Col>
              </Row>
            </li>
          </NavDropdown>

          <NavItem className="layout__topbar-divider" />

          <NavItem eventKey={2} href="#">
            <span className="glyphicon glyphicon-question-sign" />
            <span> Help</span>
          </NavItem>

          <NavItem className="layout__topbar-divider" />

          {(_.isEmpty(this.props.auth.user)) ?
            <NavItem eventKey={2} href="#/login">
              <span className="glyphicon glyphicon-log-in" />
              <span> Log in</span>
            </NavItem>
            :
            <NavDropdown id="company-dropdown" eventKey={4} title={this.props.auth.user.disp_name || this.props.auth.user.email}>
              <MenuItem eventKey="4.1" href="#/logout">
                <span className="glyphicon glyphicon-log-out" />
                <span> Log out</span>
              </MenuItem>
            </NavDropdown>
          }
        </Nav>
      </Navbar>
    );
  }
}
