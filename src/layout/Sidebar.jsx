import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

import SidebarItem from './SidebarItem.jsx';

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id='sidebar'>
        <div id="sidebar-logo">
          <Link to="/"><Image src="assets/imgs/logo_green.svg" /></Link>
        </div>
        <div className="list-group">
          <SidebarItem path="/" icon="icon-dashbord">Dashboard</SidebarItem>
          <SidebarItem path="/loads" icon="icon-load">Loads</SidebarItem>
          <SidebarItem path="/trips" icon="icon-trip_s">Trips</SidebarItem>
          <SidebarItem path="/brokers" icon="icon-broker">Brokers</SidebarItem>
          <SidebarItem path="/drivers" icon="icon-driver">Drivers</SidebarItem>
          <SidebarItem path="/map" icon="icon-globe">Map</SidebarItem>
          <SidebarItem path="/trucks" icon="icon-truck_s">Trucks</SidebarItem>
          <SidebarItem path="/invoices" icon="icon-truck_s">Invoices</SidebarItem>
          {/*
           <Link to="/invoices" className="list-group-item"><i className="fa fa-file-text" /> Invoices</Link>
           <Link to="/report" className="list-group-item"><i className="fa fa-bar-chart" /> Report</Link>
           <Link to="/calendar" className="list-group-item"><i className="fa fa-calendar" /> Calendar</Link>
          */}
        </div>
      </div>
    );
  }
}
