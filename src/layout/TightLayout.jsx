import React, { PropTypes } from 'react';

import TopBar from './TopBar';
import SecondBar from './SecondBar';
import Sidebar from './Sidebar';
import FetchPreloader from 'components/FetchPreloader/FetchPreloader';
import LayoutBase from './LayoutBase.jsx';

export default class TightLayout extends LayoutBase {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="dashboard-layout" id="body">
        <SecondBar pageHeader={this.state.pageHeader} />
        <TopBar />
        <FetchPreloader />
        <Sidebar />

        <div id="content">
          <Grid id="content-grid">
            <Row>
              <Col xs={12} id="tight-layout">
                {this.props.children}
              </Col>
            </Row>
          </Grid>
        </div>

        {this.renderNotificationSystem()}
      </div>
    );
  }
}

