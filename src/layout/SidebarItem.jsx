import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class SidebarItem extends React.Component {
  static propTypes = {
    path: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired,
  };

  static contextTypes = {
    location: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Link to={this.props.path}
            className={classNames({'sidebar__active': this.props.path == this.context.location.pathname}, 'list-group-item')}>
        <i className={classNames(this.props.icon, 'sidebar__icon')} />
        <span className="sidebar__label"> {this.props.children}</span>
      </Link>
    );
  }
}
