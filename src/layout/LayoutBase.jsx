import React, { PropTypes } from 'react';
import NotificationSystem from 'react-notification-system';

import events from 'app/events';

export default class LayoutBase extends React.Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    location: React.PropTypes.object,
  };

  static childContextTypes = {
    location: React.PropTypes.object,
    setPageHeader: React.PropTypes.func,
    notify: React.PropTypes.func,
  };

  static contextTypes = {
    location: React.PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      pageHeader: '',
    };

    this._notificationSystem = null;

    events.on('NOTIFY', (message, level) => {
      this._notify(message, level);
    });
  }

  componentDidMount() {
    this._notificationSystem = this.refs.notificationSystem;
  }

  renderNotificationSystem() {
    return (<NotificationSystem ref="notificationSystem" />);
  }

  _notify(message, level='info') {
    var dissmiss = 5;
    if (level == 'error') dissmiss = 0;

    this._notificationSystem.addNotification({
      message,
      // success, error, warning, info
      level,
      position: 'br',
      autoDismiss: dissmiss,
    });
  }

  getChildContext() {
    return {
      location: this.props.location,
      setPageHeader: (newHeader) => {
        this.setState({pageHeader: newHeader});
      },
      notify: (message, level) => {
        this._notify(message, level);
      },
    }
  }
}
