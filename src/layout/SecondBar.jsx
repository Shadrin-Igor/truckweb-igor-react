import React, { PropTypes } from 'react';
import { Navbar} from 'react-bootstrap';
import { connect } from 'react-redux'

import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({myCompany: state.myCompany}),
  { getMyCompany }
)
export default class Header extends React.Component {
  static propTypes = {
    path: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.props.getMyCompany().then(() => {
      this.forceUpdate();
    });
  }

  render() {
    return (
      <Navbar fixedTop id="second-bar">
        <Navbar.Header>
          <Navbar.Brand>
            <h1 id="second-bar__page-header">{this.props.pageHeader}</h1>
          </Navbar.Brand>
        </Navbar.Header>
      </Navbar>
    );
  }
}
