'use strict';

import './app/preloader';

import router from './app/router';

Pace.once('hide', () => {
  $('body').removeClass('pace-big').addClass('pace-small');
});

console.log('NODE_ENV = ', NODE_ENV);

if (NODE_ENV == 'production') {
  Raven.config('http://99649d548f164cbfba9961861cd507f6@sentry.truckersys.tk:9000/3').install();
  console.log('Sentry is installed.')
}

router();
