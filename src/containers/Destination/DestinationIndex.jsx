import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import DestinationList from 'components/DestinationList/DestinationList.jsx';
import { getDestinationList, deleteDestination } from '../../redux/actions/destination';
import { getMyCompany } from 'redux/actions/company';
import { createOrderAddress, updateOrderAddress } from 'redux/actions/orderAddr';

@connect(
  state => ({
    myCompany: state.myCompany,
    destinationList: state.destinationList}),
  { getMyCompany,
    getDestinationList,
    createOrderAddress,
    deleteDestination,
    updateOrderAddress
  }
)
export default class DestinationIndex extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    myCompany: PropTypes.object,
    destinationList: PropTypes.object,
    getMyCompany: PropTypes.func,
    getDestinationList: PropTypes.func,
    createOrderAddress: PropTypes.func,
    updateOrderAddress: PropTypes.func,
    deleteDestination: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.context.setPageHeader('Destination list');
  }

  render() {
    return (
      <DestinationList list={this.props.destinationList.destinations} totalCount={this.props.destinationList.count}
                 myCompany={this.props.myCompany}
                 onSelect={(item) => this.context.router.push(`/destination/${item.id}`)}
                 getMyCompany={this.props.getMyCompany}
                 getDestinationList={this.props.getDestinationList}
                 createOrderAddress={this.props.createOrderAddress}
                 updateOrderAddress={this.props.updateOrderAddress}
                 deleteDestination={this.props.deleteDestination}
                  />
    );
  }
}
