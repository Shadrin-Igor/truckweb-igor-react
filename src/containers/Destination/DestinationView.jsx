import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import AddrForm from 'components/AddrForm/AddrForm';
import DestinationDetails from 'components/DestinationForm/DestinationDetails.jsx';
import { createOrderAddress, updateOrderAddress } from 'redux/actions/orderAddr';
import { getDestinationItem } from 'redux/actions/destination';
import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({
    myCompany: state.myCompany,
    destinationItem: state.destinationItem,
  }),
  { getMyCompany,
    getDestinationItem,
    createOrderAddress,
    updateOrderAddress
  }
)
export default class DestinationView extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    myCompany: PropTypes.object,
    getDestinationItem: PropTypes.object,
    getMyCompany: PropTypes.func,
    createOrderAddress: PropTypes.func,
    updateOrderAddress: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.props.getMyCompany().then(() => {
        this.props.getDestinationItem(this.props.params.id).then(() => {
          this.context.setPageHeader('Destination ' + this.props.destinationItem.destination.id);

          // TODO: check by another way
          if (this.props.destinationItem.destination.id >= 0) {
            // TODO: hack, use pending
            this.loaded = true;

            // TODO: hack, use async method
            this.forceUpdate();
          }
        });
      });
    }
    else {
      // TODO: if id doesn't exist, redirect to /trucks
      // TODO: rise an error
    }
  }

  handleEditSubmit(formState) {
    this.props.updateOrderAddress(formState).then(() => {
      this.props.getDestinationItem(this.props.params.id).then(() => {
        this.refs.editModal.close();
      });
    });
  }

  render() {
    return (
      <div>
        {(this.loaded) ?
          <div>
            <div className="form-toolbar">
              <Button className="btn-xs" onClick={() => this.refs.editModal.open()}>Edit</Button>
              <BaseModal ref="editModal" header={'Edit destination: ' + this.props.destinationItem.destination.id} options={{bsSize: 'large'}}>
                <AddrForm initialValues={this.props.destinationItem.destination}
                          updateOrderAddress={::this.props.updateOrderAddress}
                           onFormSubmit={::this.handleEditSubmit}
                           onCancel={() => this.refs.editModal.close()} />
              </BaseModal>
            </div>
            <DestinationDetails item={this.props.destinationItem.destination}
                          onClick={() => this.refs.editModal.open()} />
          </div>
          :
          <div>Loading...</div>
        }
      </div>
    );
  }
}
