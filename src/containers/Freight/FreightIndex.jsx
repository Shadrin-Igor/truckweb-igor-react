import React, { PropTypes } from 'react';
import { connect } from 'react-redux'
import FreightList from 'components/FreightList/FreightList.jsx';
import { getFreightList, createFreight, deleteFreight } from 'redux/actions/freight';
import { createInvoice } from 'redux/actions/invoice';
import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({
    myCompany: state.myCompany,
    createdInvoice: state.createdInvoice,
    freightList: state.freightList,
    freightCreated: state.freightCreated,
  }),
  { getMyCompany,
    createInvoice,
    getFreightList,
    createFreight,
    deleteFreight }
)
export default class FreightIndex extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    freightList: PropTypes.object,
    createdInvoice: PropTypes.object,
    freightCreated: PropTypes.object,
    myCompany: PropTypes.object,
    getFreightList: PropTypes.func,
    getMyCompany: PropTypes.func,
    createInvoice: PropTypes.func,
    createFreight: PropTypes.func,
    deleteFreight: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.updateList();
  }

  componentDidMount() {
    this.context.setPageHeader('Loads');
  }

  updateList() {
    this.props.getFreightList();
  }

  render() {
    return (
      <FreightList list={this.props.freightList.freights}
                   onSelect={(item) => this.context.router.push(`/loads/${item.id}`)}
                   createdInvoice={this.props.createdInvoice}
                   myCompany={this.props.myCompany}
                   freightCreated={this.props.freightCreated}
                   updateList={::this.updateList}
                   getMyCompany={this.props.getMyCompany}
                   createInvoice={this.props.createInvoice}
                   createFreight={this.props.createFreight}
                   deleteFreight={this.props.deleteFreight} />
    );
  }
}
