import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { confirm } from 'baseComponents/dialog/dialog';
import FreightCombinedForm from 'components/FreightForm/FreightCombinedForm.jsx';
import { getFreight, updateFreight, deleteFreight, createRpoint, updateRpoint, deleteRpoint } from 'redux/actions/freight';
import { checkUser } from 'redux/actions/auth';
import { createTrip } from 'redux/actions/trip';
import { createOrderAddress, updateOrderAddress } from 'redux/actions/orderAddr';
import { createCompany, getBrokerList } from 'redux/actions/company';

@connect(
  state => ({
    auth: state.auth,
    freightItem: state.freightItem,
    tripCreated: state.tripCreated,
  }),
  { checkUser,
    createCompany,
    getBrokerList,
    createOrderAddress,
    updateOrderAddress,
    createTrip,
    getFreight,
    updateFreight,
    deleteFreight,
    createRpoint,
    updateRpoint,
    deleteRpoint }
)
export default class FreightView extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    location: React.PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    params: PropTypes.object,
    auth: PropTypes.object,
    freightItem: PropTypes.object,
    tripCreated: PropTypes.object,
    checkUser: PropTypes.func,
    createOrderAddress: PropTypes.func,
    updateOrderAddress: PropTypes.func,
    createTrip: PropTypes.func,
    getFreight: PropTypes.func,
    updateFreight: PropTypes.func,
    deleteFreight: PropTypes.func,
    createRpoint: PropTypes.func,
    updateRpoint: PropTypes.func,
    deleteRpoint: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.props.getFreight(this.props.params.id).then(() => {
        this.context.setPageHeader('Load# ' + this.props.freightItem.freight.id);

        // TODO: check by another way
        if (this.props.freightItem.freight.id >= 0) {
          // TODO: hack, use pending
          this.loaded = true;

          // TODO: hack, use async method
          this.forceUpdate();
        }
      });
    }
    else {
      // TODO: if id doesn't exist, redirect to list
      // TODO: rise an error
    }
  }

  componentWillUnmount() {
    // delete freight on leaving page
    if (this.props.freightItem.pending) return;
    if (this._isFreightClean(this.props.freightItem.freight)) {
      this.props.deleteFreight(this.props.freightItem.freight);
    }
  }

  updateFreight() {
    return this.props.getFreight(this.props.freightItem.freight.id);
  }

  handleOk() {
    this.context.router.push(`/loads`);
  }

  handleCancel() {
    this.props.deleteFreight(this.props.freightItem.freight).then(() => {
      this.context.router.push(`/loads`);
    });
  }

  handleDispatch() {
    this.props.checkUser().then(() => {
      var newTrip = {
        dispatcher: this.props.auth.user.id,
        orders: [this.props.freightItem.freight.id],
      };
      this.props.createTrip(newTrip).then(() => {
        // redirects to edit page
        if (!this.props.tripCreated.trip || _.isNaN(_.toNumber(this.props.tripCreated.trip.id)))
          throw new Error(`Error - trip haven't been created.`);

        var tripId = _.toNumber(this.props.tripCreated.trip.id);

        console.log(`---> Redirecting to /trips/${tripId}`);
        this.context.router.push(`/trips/${tripId}`);
      });
    });
  }

  _isNewFreight() {
    return this.context.location.query.new;
  }

  _isFreightClean(item) {
    if (!item.broker) return true;
    return false;
  }

  render() {
    return (
      <div>
        {(this.loaded) ?
          <div>
            <FreightCombinedForm needUpdate={::this.updateFreight}
                                 freightItem={this.props.freightItem}
                                 createCompany={this.props.createCompany}
                                 getBrokerList={this.props.getBrokerList}
                                 createOrderAddress={this.props.createOrderAddress}
                                 updateOrderAddress={this.props.updateOrderAddress}
                                 updateFreight={this.props.updateFreight}
                                 deleteFreight={this.props.deleteFreight}
                                 createRpoint={this.props.createRpoint}
                                 updateRpoint={this.props.updateRpoint}
                                 deleteRpoint={this.props.deleteRpoint} />

            <Row>
              <Col sm={12} className="form-footer">
                <Button bsStyle="primary" className="pull-right"
                        onClick={::this.handleOk}>Ok</Button>
                {_.isNil(this.props.freightItem.freight.trip) &&
                <Button bsStyle="default" className="pull-right"
                        onClick={::this.handleDispatch}>Ok and dispatch</Button>
                }
                {this._isNewFreight() &&
                <Button bsStyle="default" className="pull-right"
                        onClick={::this.handleCancel}>Cancel</Button>
                }
              </Col>
            </Row>
          </div>
          :
          <div>Loading...</div>
        }
      </div>
    );
  }
}
