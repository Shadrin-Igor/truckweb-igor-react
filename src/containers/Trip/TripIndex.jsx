import React, { PropTypes } from 'react';
import { connect } from 'react-redux'
import TripList from 'components/TripList/TripList.jsx';
import { getFullTripList, deleteTrip } from '../../redux/actions/trip';

@connect(
  state => ({fullTrips: state.fullTrips}),
  { getFullTripList,
    deleteTrip}
)
export default class TripIndex extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.props.getFullTripList();
  }

  componentDidMount() {
    this.context.setPageHeader('Trips list');
  }

  handleRemove(item) {
    return this.props.deleteTrip(item);
  }

  render() {
    return (
      <TripList list={this.props.fullTrips.trips}
                onSelect={(item) => this.context.router.push(`/trips/${item.id}`)}
                onRemove={::this.handleRemove} />
    );
  }
}
