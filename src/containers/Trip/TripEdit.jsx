import React, { PropTypes } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux'

import TripForm from 'components/TripForm/TripForm.jsx';
import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import FreightCombinedForm from 'components/FreightForm/FreightCombinedForm.jsx';
import FreightList from 'components/FreightList/FreightList.jsx';
import { checkUser } from 'redux/actions/auth';
import { getTrip, updateTrip, deleteTrip } from 'redux/actions/trip';
import { getTripsFreightList, getFreeFreightList, createFreight, updateFreight, updateRpoint, createRpoint, deleteRpoint } from 'redux/actions/freight';
import { createOrderAddress, updateOrderAddress } from 'redux/actions/orderAddr';
import { createDriver } from '../../redux/actions/driver';

@connect(
  state => ({
    auth: state.auth,
    tripItem: state.tripItem,
    freeFreightList: state.freeFreightList,
    tripFreightList: state.tripFreightList}),
  { checkUser,
    createOrderAddress,
    updateOrderAddress,
    getTrip,
    updateTrip,
    deleteTrip,
    getTripsFreightList,
    getFreeFreightList,
    createFreight,
    updateFreight,
    updateRpoint,
    createRpoint,
    deleteRpoint,
    createDriver }
)
export default class TripEdit extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    auth: PropTypes.object,
    tripItem: PropTypes.object,
    tripFreightList: PropTypes.object,
    getFreeFreightList: PropTypes.func,
    checkUser: PropTypes.func,
    getTrip: PropTypes.func,
    updateTrip: PropTypes.func,
    deleteTrip: PropTypes.func,
    getTripsFreightList: PropTypes.func,
    createFreight: PropTypes.func,
    updateFreight: PropTypes.func,
    createRpoint: PropTypes.func,
    updateRpoint: PropTypes.func,
    deleteRpoint: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      freeFreightListIsLoaded: false,
    }
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.props.getTrip(this.props.params.id).then(() => {
        this.context.setPageHeader('Trip# ' + this.props.tripItem.trip.id);

        // TODO: check by another way
        if (this.props.tripItem.trip.id >= 0) {
          this.updateFreightList().then(() => {
            // TODO: hack, use pending
            this.loaded = true;

            // TODO: hack, use async method
            this.forceUpdate();
          });
        }
      });
    }
    else {
      // TODO: if route isn't /trips/new, redirect to /trips
      // TODO: rise an error
    }
  }

  handleFreightUnbind(freight) {
    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to unbind load?',
      onOk: () => {
        var newFreightState = {
          ...freight,
          trip: null,
        };
        this.props.updateFreight(newFreightState).then(() => {
          this.updateFreightList();
        });
      },
    });
  }

  handleRemove() {
    return new Promise((resolve, reject) => {
      // confirm delete
      confirm({
        okBtn: 'Delete',
        message: 'Do you really want to delete trip?',
        onOk: () => {
          this.props.deleteTrip(this.props.tripItem.trip).then(() => {
            this.context.router.push(`/trips`);
          }, reject);
        },
        onCancel: () => {
          reject();
        }
      });
    });
  }

  handleOpenFreeFreightModal() {
    this.props.getFreeFreightList().then(() => {
      this.refs.assignFreight.open();
      this.setState({freeFreightListIsLoaded: true});
    });
  }

  handleSelectFreeFreight(freight) {
    var newState = {
      id: freight.id,
      trip: this.props.tripItem.trip.id,
    };
    this.props.updateFreight(newState).then(() => {
      this.refs.assignFreight.close();
      this.updateFreightList();
    })
  }

  updateFreightList() {
    return this.props.getTripsFreightList(this.props.tripItem.trip.id)
  }

  handleTripOk() {
    this.context.router.push(`/trips`);
  }

  handleSendDriver() {
    var sendData = {
      id: this.props.tripItem.trip.id,
      set_active: true,
    };
    this.props.updateTrip(sendData).then(() => {
      this.context.router.push(`/trips`);
    });
  }

  render() {
    return (
      <div>
        {(this.loaded) ?
          <div>
            <Row className="trip__form-wrapper">
              <Col xs={12}>
                <TripForm
                  initialValues={this.props.tripItem.trip}
                  updateTrip={this.props.updateTrip}
                  createDriver={this.props.createDriver}
                  onPlusLoadClick={::this.handleOpenFreeFreightModal} />
              </Col>
            </Row>

            <BaseModal ref={'assignFreight'}
                       header={'Assign load'}
                       options={{bsSize: 'extra-large'}}
                       onClose={() => this.setState({freeFreightListIsLoaded: false})}>
              {(this.state.freeFreightListIsLoaded) ?
                <FreightList list={this.props.freeFreightList.freights}
                             allowActions={false}
                             allowCreate={false}
                             onSelect={::this.handleSelectFreeFreight} />
                :
                <span>Loading...</span>
              }
            </BaseModal>

            <div>
              {this.props.tripFreightList.freights.map((freight) => {
                return (
                  <div key={freight.id} className="trip__freight">
                    <div className="freight-form__toolbar">
                      <button type="button"
                              title="Unbind load from trip"
                              className="btn btn-default btn-xs pull-right"
                              onClick={this.handleFreightUnbind.bind(this, freight)}>
                        <span className="glyphicon glyphicon-trash" />
                      </button>
                    </div>

                    <FreightCombinedForm needUpdate={::this.updateFreightList}
                                         freightItem={{freight: freight}}
                                         createOrderAddress={this.props.createOrderAddress}
                                         updateOrderAddress={this.props.updateOrderAddress}
                                         updateFreight={this.props.updateFreight}
                                         createRpoint={this.props.createRpoint}
                                         updateRpoint={this.props.updateRpoint}
                                         deleteRpoint={this.props.deleteRpoint}
                                         allowEditBrokerInPlace={false}
                                         allowAddAddress={false} />
                  </div>
                );
              })}
            </div>

            <Row>
              <Col sm={12} className="form-footer">
                {!this.props.tripItem.trip.is_active &&
                  <Button bsStyle="primary" className="pull-right"
                          onClick={::this.handleSendDriver}>Ok & Send driver</Button>
                }
                <Button bsStyle="primary" className="pull-right"
                        onClick={::this.handleTripOk}>Ok</Button>

                <Button bsStyle="default" className="pull-right form-toolbar__remove"
                        onClick={::this.handleRemove}>Delete trip</Button>
              </Col>
            </Row>

          </div>
        :
          <div>Loading...</div>
        }

      </div>
    );
  }
}
