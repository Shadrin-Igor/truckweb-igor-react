import React, { PropTypes } from 'react';
import { connect } from 'react-redux'

import CompanyForm from 'components/Company/CompanyForm.jsx';
import { getMyCompany, updateMyCompany, uploadCompanyLogo } from 'redux/actions/company';

@connect(
  state => ({myCompany: state.myCompany}),
  { getMyCompany,
    updateMyCompany,
    uploadCompanyLogo }
)
export default class CompanySettings extends React.Component {
  static propTypes = {
    getMyCompany: PropTypes.func,
    updateMyCompany: PropTypes.func,
  };

  static contextTypes = {
    setPageHeader: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.props.getMyCompany().then(() => {
      this.loaded = true;
      this.forceUpdate();
    });
  }

  componentDidMount() {
    this.context.setPageHeader('Company settings');
  }

  render() {
    return (
      <div>
        {(this.loaded) ?
          <CompanyForm initialValues={this.props.myCompany.company}
                       updateMyCompany={this.props.updateMyCompany}
                       uploadCompanyLogo={this.props.uploadCompanyLogo} />
          :
          <div>Loading...</div>
        }
      </div>
    );
  }
}
