import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import BrokerForm from 'components/BrokerForm/BrokerForm';
import BrokerDetails from 'components/BrokerForm/BrokerDetails.jsx';

import { getCompany, updateCompany } from '../../redux/actions/company';

@connect(
  state => ({companyItem: state.companyItem}),
  { getCompany,
    updateCompany }
)
export default class BrokerView extends React.Component {
  static contextTypes = {
    setPageHeader: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.props.getCompany(this.props.params.id).then(() => {
        this.context.setPageHeader('Broker ' + this.props.companyItem.company.disp_name);

        // TODO: check by another way
        if (this.props.companyItem.company.id >= 0) {
          // TODO: hack, use pending
          this.loaded = true;

          // TODO: hack, use async method
          this.forceUpdate();
        }
      });
    }
    else {
      // TODO: if id doesn't exist, redirect to /brokers
      // TODO: rise an error
    }
  }

  handleEditSubmit(formState) {
    this.props.updateCompany(formState).then(() => {
      this.refs.editModal.close();
      this.props.getCompany(this.props.params.id);
    });
  }

  render() {
    return (
      <div>
        {(this.loaded) ?
          <div>
            <div className="form-toolbar">
              <Button className="btn-xs" onClick={() => this.refs.editModal.open()}>Edit</Button>
              <BaseModal ref="editModal" header={'Edit broker:' + this.props.companyItem.company.disp_name} options={{bsSize: 'large'}}>
                <BrokerForm initialValues={this.props.companyItem.company}
                            onFormSubmit={::this.handleEditSubmit}
                            onCancel={() => this.refs.editModal.close()} />
              </BaseModal>
            </div>
            <BrokerDetails item={this.props.companyItem.company}
                           onClick={() => this.refs.editModal.open()} />
          </div>
          :
          <div>Loading...</div>
        }
      </div>
    );
  }
}
