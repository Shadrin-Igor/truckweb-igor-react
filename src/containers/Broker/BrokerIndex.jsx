import React, { PropTypes } from 'react';
import { connect } from 'react-redux'

import BrokerList from 'components/BrokerList/BrokerList.jsx';
import { getBrokerList, getCompany, createCompany, updateCompany, deleteCompany } from '../../redux/actions/company';

@connect(
  state => ({
    companyList: state.companyList,
    companyItem: state.companyItem }),
  { getBrokerList,
    getCompany,
    createCompany,
    updateCompany,
    deleteCompany}
)
export default class BrokerIndex extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.context.setPageHeader('Brokers list');
  }

  render() {
    return (
      <BrokerList companyList={this.props.companyList}
                  onSelect={(item) => this.context.router.push(`/brokers/${item.id}`)}
                  getCompany={this.props.getCompany}
                  getBrokerList={this.props.getBrokerList}
                  createCompany={this.props.createCompany}
                  updateCompany={this.props.updateCompany}
                  deleteCompany={this.props.deleteCompany}
                  companyItem={this.props.companyItem} />
    );
  }
}
