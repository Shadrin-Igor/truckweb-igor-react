import React, { PropTypes } from 'react';
import { connect } from 'react-redux'
//import shouldPureComponentUpdate from 'react-pure-render/function';
import GoogleMap from 'google-map-react';
import _ from 'lodash';

import { getDriverList } from '../../redux/actions/driver';


const K_WIDTH = 40;
const K_HEIGHT = 40;

const greatPlaceStyle = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  width: K_WIDTH,
  height: K_HEIGHT,
  left: -K_WIDTH / 2,
  top: -K_HEIGHT / 2,

  border: '5px solid #f44336',
  borderRadius: K_HEIGHT,
  backgroundColor: 'white',
  textAlign: 'center',
  color: '#3f51b5',
  fontSize: 16,
  fontWeight: 'bold',
  padding: 4
};

class MyGreatPlace extends React.Component {
  static propTypes = {
    text: PropTypes.string
  };

  static defaultProps = {};

  //shouldComponentUpdate = shouldPureComponentUpdate;

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={greatPlaceStyle}>
        {this.props.text}
      </div>
    );
  }
}


@connect(
  state => ({driverList: state.driverList}),
  {getDriverList}
)
export default class extends React.Component {
  static propTypes = {
    center: PropTypes.array,
    zoom: PropTypes.number,
    greatPlaceCoords: PropTypes.any
  };

  static contextTypes = {
    setPageHeader: React.PropTypes.func,
  };

  static defaultProps = {
    center: [38.0788336, -92.8672493],
    zoom: 4,
    //greatPlaceCoords: {lat: 59.724465, lng: 30.080121}
  };

  //shouldComponentUpdate = shouldPureComponentUpdate;

  constructor(props) {
    super(props);
    this.props.getDriverList().then(() => {
      console.log(1111, this.props.driverList)
    });
  }

  componentDidMount() {
    this.context.setPageHeader('Map');
  }

  renderMarker(list) {
    var markers = [];
    _.each(list, (item) => {
      if (!item.point) return;

      var points = item.point.point.split(' ');
      markers.push({
        id: item.id,
        lat: points[0],
        lng: points[1],
      });
    });

    return _.map(markers, (item, index) => {
      return (
        <MyGreatPlace key={item.id} lat={item.lat} lng={item.lng} text={index + 1} />
      );
    });
  }

  render() {
    //apiKey={'AIzaSyB16BJkyN3MkLl0Qst4_P-pqHE-XZtplf0'} // set if you need stats etc ...
    return (
      <div style={{height: "600px"}}>
        <GoogleMap
          center={this.props.center}
          zoom={this.props.zoom}
        >
          {this.renderMarker(this.props.driverList.drivers)}
        </GoogleMap>

      </div>
    );
  }
}
