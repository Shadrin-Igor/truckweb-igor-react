import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import TruckForm from 'components/TruckForm/TruckForm';
import TruckDetails from 'components/TruckForm/TruckDetails.jsx';
import { getTruck, updateTruck } from '../../redux/actions/truck';
import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({
    myCompany: state.myCompany,
    truckItem: state.truckItem,
  }),
  { getMyCompany,
    getTruck,
    updateTruck}
)
export default class TruckView extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    myCompany: PropTypes.object,
    truckItem: PropTypes.object,
    getMyCompany: PropTypes.func,
    getTruck: PropTypes.func,
    updateTruck: PropTypes.func,
    getDriver: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.props.getMyCompany().then(() => {
        this.props.getTruck(this.props.myCompany.company, this.props.params.id).then(() => {
          this.context.setPageHeader('Truck ' + this.props.truckItem.truck.truckid);

          // TODO: check by another way
          if (this.props.truckItem.truck.id >= 0) {
            // TODO: hack, use pending
            this.loaded = true;

            // TODO: hack, use async method
            this.forceUpdate();
          }
        });
      });
    }
    else {
      // TODO: if id doesn't exist, redirect to /trucks
      // TODO: rise an error
    }
  }

  handleEditSubmit(formState) {
    this.props.updateTruck(this.props.myCompany.company, formState).then(() => {
      this.props.getTruck(this.props.myCompany.company, this.props.params.id).then(() => {
        this.refs.editModal.close();
      });
    });
  }

  render() {
    return (
      <div>
        {(this.loaded) ?
          <div>
            <div className="form-toolbar">
              <Button className="btn-xs" onClick={() => this.refs.editModal.open()}>Edit</Button>
              <BaseModal ref="editModal" header={'Edit truck: ' + this.props.truckItem.truck.truckid}>
                <TruckForm initialValues={this.props.truckItem.truck}
                           onFormSubmit={::this.handleEditSubmit}
                           onCancel={() => this.refs.editModal.close()} />
              </BaseModal>
            </div>
            <TruckDetails item={this.props.truckItem.truck}
                          onClick={() => this.refs.editModal.open()} />
          </div>
          :
          <div>Loading...</div>
        }
      </div>
    );
  }
}
