import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import TruckList from 'components/TruckList/TruckList.jsx';
import { getTruckList, getTruck, createTruck, updateTruck, deleteTruck } from '../../redux/actions/truck';
import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({
    myCompany: state.myCompany,
    truckList: state.truckList,
    truckItem: state.truckItem}),
  { getMyCompany,
    getTruckList,
    getTruck,
    createTruck,
    updateTruck,
    deleteTruck}
)
export default class TruckIndex extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    myCompany: PropTypes.object,
    truckList: PropTypes.object,
    truckItem: PropTypes.object,
    getMyCompany: PropTypes.func,
    getTruckList: PropTypes.func,
    getTruck: PropTypes.func,
    createTruck: PropTypes.func,
    updateTruck: PropTypes.func,
    deleteTruck: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.context.setPageHeader('Trucks list');
  }

  render() {
    return (
      <TruckList list={this.props.truckList.trucks}
                 myCompany={this.props.myCompany}
                 onSelect={(item) => this.context.router.push(`/trucks/${item.id}`)}
                 truckItem={this.props.truckItem}
                 getMyCompany={this.props.getMyCompany}
                 getTruck={this.props.getTruck}
                 getTruckList={this.props.getTruckList}
                 createTruck={this.props.createTruck}
                 updateTruck={this.props.updateTruck}
                 deleteTruck={this.props.deleteTruck} />
    );
  }
}
