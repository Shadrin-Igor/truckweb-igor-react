import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import DriverForm from 'components/DriverForm/DriverForm';
import DriverDetails from 'components/DriverForm/DriverDetails.jsx';
import { getDriver, updateDriver } from '../../redux/actions/driver';

@connect(
  state => ({driverItem: state.driverItem}),
  { getDriver,
    updateDriver}
)
export default class DriverView extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.props.getDriver(this.props.params.id).then(() => {
        this.context.setPageHeader('Driver ' + this.props.driverItem.driver.disp_name);

        // TODO: check by another way
        if (this.props.driverItem.driver.id >= 0) {
          // TODO: hack, use pending
          this.loaded = true;

          // TODO: hack, use async method
          this.forceUpdate();
        }
      });
    }
    else {
      // TODO: if id doesn't exist, redirect to /drivers
      // TODO: rise an error
    }
  }

  handleEditSubmit(formState) {
    this.props.updateDriver(formState).then(() => {
      this.refs.editModal.close();
    });
  }

  render() {
    return (
      <div>
        {(this.loaded) ?
          <div>
            <div className="form-toolbar">
              <Button className="btn-xs" onClick={() => this.refs.editModal.open()}>Edit</Button>
              <BaseModal ref="editModal" header={'Edit driver: ' + this.props.driverItem.driver.disp_name}>
                <DriverForm initialValues={this.props.driverItem.driver}
                            onFormSubmit={::this.handleEditSubmit}
                            onCancel={() => this.refs.editModal.close()} />
              </BaseModal>
            </div>
            <DriverDetails item={this.props.driverItem.driver}
                           onClick={() => this.refs.editModal.open()} />
          </div>
          :
          <div>Loading...</div>
        }
      </div>
    );
  }
}
