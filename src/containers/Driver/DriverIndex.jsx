import React, { PropTypes } from 'react';
import { connect } from 'react-redux'

import DriverList from 'components/DriverList/DriverList.jsx';
import { getDriverList, getDriver, createDriver, updateDriver, deleteDriver } from '../../redux/actions/driver';

@connect(
  state => ({
    driverList: state.driverList,
    driverItem: state.driverItem,
    createdDriver: state.createdDriver}),
  { getDriverList,
    getDriver,
    createDriver,
    updateDriver,
    deleteDriver}
)
export default class DriverIndex extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.props.getDriverList();
  }

  componentDidMount() {
    this.context.setPageHeader('Drivers list');
  }

  render() {
    return (
      <DriverList list={this.props.driverList.drivers}
                  onSelect={(item) => this.context.router.push(`/drivers/${item.id}`)}
                  driverItem={this.props.driverItem}
                  getDriver={this.props.getDriver}
                  createDriver={this.props.createDriver}
                  updateDriver={this.props.updateDriver}
                  deleteDriver={this.props.deleteDriver} />
    );
  }
}

