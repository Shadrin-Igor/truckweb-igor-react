import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import InvoiceList from 'components/InvoiceList/InvoiceList.jsx';
import { getInvoiceList, getInvoice, createInvoice, updateInvoice, deleteInvoice } from 'redux/actions/invoice';
import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({
    myCompany: state.myCompany,
    invoiceList: state.invoiceList,
    invoiceItem: state.invoiceItem,
    createdInvoice: state.createdInvoice }),
  { getMyCompany,
    getInvoiceList,
    getInvoice,
    createInvoice,
    updateInvoice,
    deleteInvoice }
)
export default class InvoiceIndex extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
    setPageHeader: React.PropTypes.func,
  };

  static propTypes = {
    myCompany: PropTypes.object,
    invoiceList: PropTypes.object,
    invoiceItem: PropTypes.object,
    createdInvoice: PropTypes.object,
    getMyCompany: PropTypes.func,
    getInvoiceList: PropTypes.func,
    getInvoice: PropTypes.func,
    createInvoice: PropTypes.func,
    updateInvoice: PropTypes.func,
    deleteInvoice: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.context.setPageHeader('Invoices list');
  }

  render() {
    return (
      <InvoiceList invoiceList={this.props.invoiceList}
                   myCompany={this.props.myCompany}
                   invoiceItem={this.props.invoiceItem}
                   createdInvoice={this.props.createdInvoice}
                   getMyCompany={this.props.getMyCompany}
                   getInvoice={this.props.getInvoice}
                   getInvoiceList={this.props.getInvoiceList}
                   createInvoice={this.props.createInvoice}
                   updateInvoice={this.props.updateInvoice}
                   deleteInvoice={this.props.deleteInvoice} />
    );
  }
}
