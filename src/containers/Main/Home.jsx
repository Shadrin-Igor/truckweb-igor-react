import React from 'react';
import FreightIndex from 'containers/Freight/FreightIndex.jsx';

export default class Home extends React.Component {
  static contextTypes = {
    setPageHeader: React.PropTypes.func,
  };

  componentDidMount() {
    this.context.setPageHeader('Dashboard');
  }
  
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <h1>Loads</h1>
        <FreightIndex />
      </div>
    );
  }
}
