import React from 'react';
import { connect } from 'react-redux'

import { logout } from 'redux/actions/auth';

@connect(
  state => ({}),
  { logout }
)
export default class Logout extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  componentDidMount() {
    this.props.logout();
    this.context.router.push('/login');
  }

  render() {
    return (<p>Loging out...</p>)
  }
}
