import React from 'react';

export default class NotFound extends React.Component {
  render() {
    return (
      <Panel>
        <div class="jumbotron">
          <h1>Error 404</h1>
          <p>
            <div className="alert alert-danger" role="alert">
              Page not found!
            </div>
          </p>
        </div>
      </Panel>
    );
  }
}
