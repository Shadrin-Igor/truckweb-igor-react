import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import _ from 'lodash';

import { login, checkUser } from 'redux/actions/auth';

@connect(
  state => ({auth: state.auth}),
  { login,
    checkUser }
)
export default class Login extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: null,
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    event.stopPropagation();
    this.setState({error: null});

    this.props.login(this.state.username, this.state.password);

    this.props.checkUser().then(() => {
      console.log('---> Login success', this.props.auth);
      setTimeout(() => {
        this.context.router.push(`/`);
      }, 1000);
    }, () => {
      console.log('---> Login error', this.props.auth);
      if (this.props.auth.error) {
        this.setState({error: this.props.auth.error});
      }
    });
  }

  handleUsername(event) {
    this.setState({username: event.target.value});
  }

  handlePassword(event) {
    this.setState({password: event.target.value});
  }

  render() {
    return (
      <Panel header="Sign in">
        {(_.isEmpty(this.props.auth.user)) ?
          <Form horizontal onSubmit={ ::this.handleSubmit }>
            <FormGroup>
              <Col componentClass={ControlLabel} sm={3}>Username</Col>
              <Col sm={9}>
                <FormControl componentClass="input" id='emailaddress' className='border-focus-blue' placeholder='Your e-mail' onChange={::this.handleUsername} value={this.state.username} />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} sm={3}>Password</Col>
              <Col sm={9}>
                <FormControl componentClass="input" type='password' id='password' className='border-focus-blue' onChange={::this.handlePassword} value={this.state.password} />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col sm={3}></Col>
              <Col sm={9}>
                <Row>
                  <Col xs={6} collapseLeft collapseRight style={{paddingTop: 10}}>
                    <Link to='/app/signup'>Create account</Link>
                  </Col>
                  <Col xs={6} collapseLeft collapseRight className='text-right'>
                    <Button outlined lg type='submit' bsStyle='primary'>Login</Button>
                  </Col>
                </Row>
              </Col>
            </FormGroup>
          </Form>
          :
          <h2>You are logged in. Redirecting to dashboard.</h2>
        }

        {this.state.error &&
          <div class="alert alert-danger" role="alert">
            {this.state.error.details}
          </div>
        }

      </Panel>
    );
  }
}

// export default reduxForm({
//   form: 'login',
//   fields: [
//     'username',
//     'password'
//   ]
// })(Login)
