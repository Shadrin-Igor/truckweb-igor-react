import _ from 'lodash';

import events from 'app/events';
import fetch from './fetch.js';
import { _REQUEST, _SUCCESS, _FAILURE } from 'redux/baseTypes';
import { startSave, endSave } from 'redux/actions/fetch';

function checkStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    throw response;
  }
}

export default ({dispatch, getState}) => next => action => {
  // TODO: см redux-thunk
  if (typeof action === 'function') {
    return action(dispatch, getState);
  }

  const { endpoint, type, body, method = 'GET', contentType, ...rest } = action;

  // If isn't api request - do next
  if (!endpoint) return next(action);

  const clearType = type.replace(/_REQUEST|_SUCCESS|_FAILURE$/, '');

  // do next _REQUEST type
  next({...rest, type: clearType + _REQUEST});

  if (_.includes(['POST', 'PUT', 'PATCH', 'DELETE'], method)) dispatch(startSave(endpoint));

  return fetch(endpoint, body, method, contentType)
  .then(checkStatus)
  .catch((error) => {
    if (_.isUndefined(error.status)) {
      error = {
        status: undefined,
        statusText: error.toString(),
      }
    }

    function handleError(errMsg) {
      events.emit('NOTIFY', `Request Error ${_.isNumber(error.status) && error.status}: ${errMsg}`, 'error');

      next({
        errorRaw: error,
        error: errMsg,
        result: {},
        type: clearType + _FAILURE
      });
    }

    if (error.bodyUsed) {
      error.json()
        .then((result) => {
          handleError(result);
        }, (err) => {
          handleError(err);
        });
    }
    else {
      handleError(error.statusText);
    }
  })
  // parse success json
  .then((response) => {
    if (_.includes(['POST', 'PUT', 'PATCH', 'DELETE'], method)) dispatch(endSave(endpoint));

    if (!response) return;

    // TODO: refactor
    if (response.status == 204) {
      return new Promise((resolve, reject) => {
        next({
          ...rest,
          result: {},
          type: clearType + _SUCCESS
        });
        resolve({});
      });
    }
    else {
      return response.json()
      .then((result) => {
        next({
          ...rest,
          result,
          type: clearType + _SUCCESS
        });
      }, (error) => {
        events.emit('NOTIFY', 'Can\'t parse json', 'error');
        throw new Error(`Can't parse json: `, error);
      })
    }
  });
};
