import { _REQUEST } from 'redux/baseTypes';
import {
  SEARCH_TRUCK} from 'redux/actions/actionTypes';

function searchTruck(company, query) {
  return {
    type: SEARCH_TRUCK + _REQUEST,
    endpoint: (query) ?
      `orgs/company/${company.id}/trucks/?search=${query}` :
      `orgs/company/${company.id}/trucks/`,
  };
}

export {
  searchTruck,
};
