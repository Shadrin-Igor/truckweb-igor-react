import { _REQUEST } from 'redux/baseTypes';
import {
  SEARCH_FREIGHT_ADDR} from 'redux/actions/actionTypes';

function searchFreightAddr(query) {
  return {
    type: SEARCH_FREIGHT_ADDR + _REQUEST,
    endpoint: (query) ?
      `orders/addrs/?search=${query}` :
      `orders/addrs/`,
  };
}

export {
  searchFreightAddr,
};
