import { _REQUEST } from 'redux/baseTypes';
import {
  SEARCH_DRIVER} from 'redux/actions/actionTypes';

function searchDriver(query) {
  return {
    type: SEARCH_DRIVER + _REQUEST,
    endpoint: (query) ?
      `users/driver/?search=${query}` :
      `users/driver/`,
  };
}

export {
  searchDriver,
};
