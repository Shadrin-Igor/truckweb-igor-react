import { _REQUEST } from 'redux/baseTypes';
import {
  LOGIN,
  LOGOUT,
  USER_CHECK,
  USER_UPDATE} from 'redux/actions/actionTypes';

function checkUser() {
  return async (dispatch) => {
    await dispatch({
      type: USER_CHECK + _REQUEST,
      endpoint: `users/me/`
    });
  };
}

function login(username, password) {
  return {
    type: LOGIN,
    username,
    password
  };

  // return async (dispatch) => {
  //   await dispatch({
  //     type: LOGIN,
  //     username,
  //     password
  //   });
  //   await dispatch(checkUser());
  // };
}

function logout() {
  return {
    type: LOGOUT
  };
}

function updateUser(url, body) {
  return {
    type: USER_UPDATE + _REQUEST,
    endpoint: url,
    body
  };
}

export {
  login,
  logout,
  checkUser,
  updateUser
};
