import { _REQUEST } from 'redux/baseTypes';
import {
  SEARCH_USER_ADDR} from 'redux/actions/actionTypes';

function searchUserAddr(query) {
  return {
    type: SEARCH_USER_ADDR + _REQUEST,
    endpoint: `users/addr/?search=${query}`,
  };
}

export {
  searchUserAddr,
};
