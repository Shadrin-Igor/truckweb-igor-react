import _ from 'lodash';

import { _REQUEST } from 'redux/baseTypes';
import {
  GET_COMPANY,
  GET_MY_COMPANY,
  GET_COMPANY_LIST,
  CREATE_COMPANY,
  UPDATE_COMPANY,
  DELETE_COMPANY,
  UPLOAD_COMPANY_LOGO } from 'redux/actions/actionTypes';

function _addProtocolToSite(site) {
  if (site && !site.match(/^\w{3,5}:\/\//)) return `http://${site}`;
  return site;
}

function _collectAddresses(newState) {
  return _.compact([
    !_.isEmpty(_.pickBy(newState.billing_address)) && {
      ...newState.billing_address,
      addr_type: 1,
    },
    !_.isEmpty(_.pickBy(newState.shipping_address)) && {
      ...newState.shipping_address,
      addr_type: 2,
    },
    !_.isEmpty(_.pickBy(newState.legal_address)) && {
      ...newState.legal_address,
      addr_type: 3,
    },
    !_.isEmpty(_.pickBy(newState.company_address)) && {
      ...newState.company_address,
      addr_type: 4,
    },
    !_.isEmpty(_.pickBy(newState.customer_facing_address)) && {
      ...newState.customer_facing_address,
      addr_type: 5,
    },
  ]);
}

const getMyCompany = () => {
  return {
    type: GET_MY_COMPANY + _REQUEST,
    endpoint: `orgs/my/`,
  }
};

function getCompany(id) {
  return {
    type: GET_COMPANY + _REQUEST,
    endpoint: `orgs/company/${id}/`,
  };
}

function getBrokerList() {
  return {
    type: GET_COMPANY_LIST + _REQUEST,
    endpoint: `orgs/company/?kind__icontains=broker`,
    //endpoint: `orgs/company/`
  };
}

function createCompany(newState) {
  return async (dispatch) => {
    await dispatch({
      type: CREATE_COMPANY + _REQUEST,
      method: 'POST',
      endpoint: `orgs/company/`,
      body: {
        ..._.omit(newState, 'billing_address', 'shipping_address'),
        site: _addProtocolToSite(newState.site),
        address: _collectAddresses(newState),
      },
    });
  };
}

function updateCompany(newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_COMPANY + _REQUEST,
      method: 'PATCH',
      endpoint: `orgs/company/${newState.id}/`,
      body: {
        ..._.omit(newState, 'billing_address', 'shipping_address'),
        site: _addProtocolToSite(newState.site),
        address: _collectAddresses(newState),
      },
    });
  };
}

function updateMyCompany(newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_COMPANY + _REQUEST,
      method: 'PATCH',
      endpoint: `orgs/my/`,
      body: {
        ..._.omit(newState, 'billing_address', 'shipping_address'),
        site: _addProtocolToSite(newState.site),
        address: _collectAddresses(newState),
      },
    });
  };
}

function deleteCompany(item) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_COMPANY + _REQUEST,
      method: 'DELETE',
      endpoint: `orgs/company/${item.id}/`,
    });
  };
}

function uploadCompanyLogo(newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_COMPANY + _REQUEST,
      method: 'PATCH',
      contentType: 'multipart/form-data',
      endpoint: `orgs/company/${newState.id}/`,
      body: {
        file: newState.logo,
      },
    });
  };
}

export {
  getMyCompany,
  getCompany,
  getBrokerList,
  createCompany,
  updateCompany,
  updateMyCompany,
  deleteCompany,
  uploadCompanyLogo,
}
