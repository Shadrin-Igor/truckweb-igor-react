import { _REQUEST } from 'redux/baseTypes';
import {
  SEARCH_ORG} from 'redux/actions/actionTypes';

function searchOrg(query) {
  return {
    type: SEARCH_ORG + _REQUEST,
    endpoint: `orgs/company/?search=${query}`,
  };
}

function searchBroker(query) {
  return {
    type: SEARCH_ORG + _REQUEST,
    endpoint: (query) ?
      `orgs/company/?search=${query}&kind__icontains=broker` :
      `orgs/company/?kind__icontains=broker`,
  };
}

export {
  searchOrg,
  searchBroker,
};
