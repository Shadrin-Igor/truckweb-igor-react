import {
  FETCH_START_SAVE,
  FETCH_STOP_SAVE} from 'redux/actions/actionTypes';

function startSave(fetchUrl) {
  console.log(`---> start saving ${fetchUrl}`);
  return {
    type: FETCH_START_SAVE,
    fetchUrl: fetchUrl,
  }
}

function endSave(fetchUrl) {
  console.log(`---> finish saving ${fetchUrl}`);
  return {
    type: FETCH_STOP_SAVE,
    fetchUrl: fetchUrl,
  }
}

export {
  startSave,
  endSave,
};
