const actionTypes =  {
  GET_TRUCK_LIST: 'GET_TRUCK_LIST',
  GET_TRUCK: 'GET_TRUCK',
  CREATE_TRUCK: 'CREATE_TRUCK',
  UPDATE_TRUCK: 'UPDATE_TRUCK',
  DELETE_TRUCK: 'DELETE_TRUCK',
};

export default actionTypes;
