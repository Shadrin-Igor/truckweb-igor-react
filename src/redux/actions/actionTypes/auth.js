const actionTypes =  {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  USER_CHECK: 'USER_CHECK',
  USER_UPDATE: 'USER_UPDATE',
};

export default actionTypes;
