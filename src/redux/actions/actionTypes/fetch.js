const actionTypes =  {
  FETCH_START_SAVE: 'FETCH_START_SAVE',
  FETCH_STOP_SAVE: 'FETCH_STOP_SAVE',
};

export default actionTypes;
