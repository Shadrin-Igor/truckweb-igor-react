import _ from 'lodash';

import { _REQUEST } from 'redux/baseTypes';
import {
  GET_DRIVER_LIST,
  GET_DRIVER,
  CREATE_DRIVER,
  UPDATE_DRIVER,
  DELETE_DRIVER} from 'redux/actions/actionTypes';

function getDriverList() {
  return {
    type: GET_DRIVER_LIST + _REQUEST,
    endpoint: `users/driver/`
  }
}

function getDriver(id) {
  return {
    type: GET_DRIVER + _REQUEST,
    endpoint: `users/driver/${id}/`
  };
}

function createDriver(newState) {
  return async (dispatch) => {
    await dispatch({
      type: CREATE_DRIVER + _REQUEST,
      method: 'POST',
      endpoint: `users/driver/`,
      body: {
        ...newState,
        attributes: {
          ...newState.attributes,
          companies_truck: newState.selectedTruck,
        },
        truck: undefined,
      },
    });
    await dispatch(getDriverList());
  };
}

function updateDriver(newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_DRIVER + _REQUEST,
      method: 'PATCH',
      endpoint: `users/driver/${newState.id}/`,
      body: {
        ...newState,
        company: newState.company && newState.company.id,
        attributes: {
          ...newState.attributes,
          companies_truck: newState.selectedTruck,
        },
        truck: undefined,
      },
    });
    await dispatch(getDriver(newState.id));
  };
}

function deleteDriver(newState) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_DRIVER + _REQUEST,
      method: 'DELETE',
      endpoint: `users/driver/${newState.id}/`,
    });
    await dispatch(getDriverList());
  };
}

export {
  getDriverList,
  getDriver,
  createDriver,
  updateDriver,
  deleteDriver,
};
