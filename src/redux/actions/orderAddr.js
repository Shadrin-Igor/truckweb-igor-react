import _ from 'lodash';

import { _REQUEST } from 'redux/baseTypes';
import {
  CREATE_ORDER_ADDRESS,
  UPDATE_ORDER_ADDRESS,
  DELETE_ORDER_ADDRESS} from 'redux/actions/actionTypes';

function createOrderAddress(newState) {
  return async (dispatch) => {
    await dispatch({
      type: CREATE_ORDER_ADDRESS + _REQUEST,
      method: 'POST',
      endpoint: `orders/addrs/`,
      body: newState,
    });
  };
}

function updateOrderAddress(newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_ORDER_ADDRESS + _REQUEST,
      method: 'PATCH',
      endpoint: `orders/addrs/${newState.id}/`,
      body: newState,
    });
  };
}

function deleteOrderAddress(newState) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_ORDER_ADDRESS + _REQUEST,
      method: 'DELETE',
      endpoint: `orders/addrs/${newState.id}/`,
    });
  };
}

export {
  createOrderAddress,
  updateOrderAddress,
  deleteOrderAddress,
};
