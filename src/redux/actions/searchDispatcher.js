import { _REQUEST } from 'redux/baseTypes';
import {
  SEARCH_DISPATCHER} from 'redux/actions/actionTypes';

function searchDispatcher(query) {
  return {
    type: SEARCH_DISPATCHER + _REQUEST,
    endpoint: `users/user/?groups=2&search=${query}`,
  };
}

export {
  searchDispatcher,
};
