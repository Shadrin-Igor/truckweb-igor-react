import { _REQUEST } from 'redux/baseTypes';
import {
  GET_FULL_TRIP_LIST,
  GET_TRIP,
  CREATE_TRIP,
  UPDATE_TRIP,
  DELETE_TRIP} from 'redux/actions/actionTypes';

function getFullTripList() {
  return {
    type: GET_FULL_TRIP_LIST + _REQUEST,
    endpoint: `trips/fulltrip/`,
  }
}

function getTrip(id) {
  return {
    type: GET_TRIP + _REQUEST,
    endpoint: `trips/trip/${id}/`,
  };
}

function createTrip(newState) {
  return async (dispatch) => {
    await dispatch({
      type: CREATE_TRIP + _REQUEST,
      method: 'POST',
      endpoint: `trips/trip/`,
      body: newState,
    });
    await dispatch(getFullTripList(newState.id));
  };
}

function updateTrip(newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_TRIP + _REQUEST,
      method: 'PATCH',
      endpoint: `trips/trip/${newState.id}/`,
      body: {
        ...newState,
        dispatcher: newState.dispatcher && newState.dispatcher.id,
        driver: newState.driver && newState.driver.id,
        driver2: newState.driver2 && newState.driver2.id,
        truck: newState.truck && newState.truck.id,
        carrier: newState.carrier && newState.carrier.id,
        company: newState.company && newState.company.id,
      },
    });
    await dispatch(getTrip(newState.id));
  };
}

function deleteTrip(newState) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_TRIP + _REQUEST,
      method: 'DELETE',
      endpoint: `trips/trip/${newState.id}/`,
    });
    await dispatch(getFullTripList(newState.id));
  };
}

export {
  getFullTripList,
  getTrip,
  createTrip,
  updateTrip,
  deleteTrip
};
