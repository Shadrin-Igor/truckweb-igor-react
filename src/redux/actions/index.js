import trip from './trip';
import auth from './auth';
import company from './company';
import freight from './freight';
import fetch from './fetch';
import driver from './driver';
import truck from './truck';
import invoice from './invoice.js';
import orderAddr from './orderAddr';
import searchTruck from './searchTruck';
import searchDriver from './searchDriver';
import searchFreightAddr from './searchFreightAddr';
import searchDispatcher from './searchDispatcher.js';
import searchOrg from './searchOrg.js';
import searchRawAddr from './searchRawAddr.js';
import searchUserAddr from './searchUserAddr.js';
import destination from './destination';

module.exports = {
  ...trip,
  ...auth,
  ...company,
  ...freight,
  ...fetch,
  ...driver,
  ...truck,
  ...invoice,
  ...orderAddr,
  ...searchTruck,
  ...searchDriver,
  ...searchFreightAddr,
  ...searchDispatcher,
  ...searchOrg,
  ...searchRawAddr,
  ...searchUserAddr,
  ...destination
};
