import { _REQUEST } from 'redux/baseTypes';
import {
  GET_TRUCK_LIST,
  GET_TRUCK,
  CREATE_TRUCK,
  UPDATE_TRUCK,
  DELETE_TRUCK} from 'redux/actions/actionTypes';

function getTruckList(company) {
  return {
    type: GET_TRUCK_LIST + _REQUEST,
    endpoint: `orgs/company/${company.id}/trucks/`,
  }
}

function getTruck(company, id) {
  return {
    type: GET_TRUCK + _REQUEST,
    endpoint: `orgs/company/${company.id}/trucks/${id}/`,
  };
}

function createTruck(company, newState) {
  return async (dispatch) => {
    await dispatch({
      type: CREATE_TRUCK + _REQUEST,
      method: 'POST',
      endpoint: `orgs/company/${company.id}/trucks/`,
      body: {
        ...newState,
        company: company.id,
      },
    });
    await dispatch(getTruckList(company));
  };
}

function updateTruck(company, newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_TRUCK + _REQUEST,
      method: 'PATCH',
      endpoint: `orgs/company/${company.id}/trucks/${newState.id}/`,
      body: {
        ...newState,
        company: company.id,
      },
    });
    //await dispatch(getTruck(company, newState.id));
  };
}

function deleteTruck(company, newState) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_TRUCK + _REQUEST,
      method: 'DELETE',
      endpoint: `orgs/company/${company.id}/trucks/${newState.id}/`,
    });
    await dispatch(getTruckList(company));
  };
}

export {
  getTruckList,
  getTruck,
  createTruck,
  updateTruck,
  deleteTruck,
};
