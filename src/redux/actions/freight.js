import _ from 'lodash';

import { _REQUEST } from 'redux/baseTypes';
import {
  GET_FREIGHT_LIST,
  GET_TRIP_FREIGHT_LIST,
  GET_FREE_FREIGHT_LIST,
  GET_FREIGHT,
  CREATE_FREIGHT,
  UPDATE_FREIGHT,
  DELETE_FREIGHT,
  CREATE_RPOINT,
  UPDATE_RPOINT,
  DELETE_RPOINT,
  GET_RPOINT_AFTER_CHANGE } from 'redux/actions/actionTypes';

function getFreightList() {
  return {
    type: GET_FREIGHT_LIST + _REQUEST,
    endpoint: `orders/order/`,
  }
}

function getTripsFreightList(tripId) {
  return {
    type: GET_TRIP_FREIGHT_LIST + _REQUEST,
    endpoint: `orders/order/?trip=${tripId}`,
  }
}

function getFreeFreightList() {
  return {
    type: GET_FREE_FREIGHT_LIST + _REQUEST,
    endpoint: `orders/order/?trip__isnull=True`,
  }
}


function getFreight(freightId) {
  return {
    type: GET_FREIGHT + _REQUEST,
    endpoint: `orders/order/${freightId}/`,
  }
}

function createFreight(newState) {
  return async (dispatch) => {
    await dispatch({
      type: CREATE_FREIGHT + _REQUEST,
      method: 'POST',
      endpoint: `orders/order/`,
      body: {
        ...newState,
        //broker: (_.isObject(newState.broker)) ? newState.broker.id : newState.broker,
      },
    });
  };
}

function updateFreight(newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_FREIGHT + _REQUEST,
      method: 'PATCH',
      endpoint: `orders/order/${newState.id}/`,
      body: {
        ...newState,
        broker: (_.isObject(newState.broker)) ? newState.broker.id : newState.broker,
        company: newState.company && newState.company.id,
        // TODO: хак
        pay_terms: undefined,
        // TODO: хак с price
        origins: _.map(newState.origins, (item) => {return {...item, price: item.price || 0}}),
        destinations: _.map(newState.destinations, (item) => {return {...item, price: item.price || 0}}),
      },
    });
    //await dispatch(getFreight(newState.id));
  };
}

function deleteFreight(newState) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_FREIGHT + _REQUEST,
      method: 'DELETE',
      endpoint: `orders/order/${newState.id}/`,
    });
  };
}

function createRpoint(newState) {
  var fixedState = _.clone(newState);
  if (fixedState.point) fixedState.point = newState.point.id;

  return async (dispatch) => {
    await dispatch({
      type: CREATE_RPOINT + _REQUEST,
      method: 'POST',
      endpoint: `orders/order/${fixedState.order}/rpoint/`,
      body: fixedState,
    });
    //await dispatch(getFreight(fixedState.order));
  };
}

function updateRpoint(newState) {
  var fixedState = _.clone(newState);
  if (fixedState.point) fixedState.point = newState.point.id;

  return async (dispatch) => {
    await dispatch({
      type: UPDATE_RPOINT + _REQUEST,
      method: 'PATCH',
      endpoint: `orders/order/${fixedState.order}/rpoint/${fixedState.id}/`,
      body: fixedState,
    });
    //await dispatch(getFreight(fixedState.order));
  };
}

function deleteRpoint(newState) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_RPOINT + _REQUEST,
      method: 'DELETE',
      endpoint: `orders/rpoints/${newState.id}/`,
    });
    await dispatch(getFreight(newState.order));
  };
}

export {
  getFreightList,
  getFreight,
  getTripsFreightList,
  getFreeFreightList,
  createFreight,
  updateFreight,
  deleteFreight,
  createRpoint,
  updateRpoint,
  deleteRpoint,
};
