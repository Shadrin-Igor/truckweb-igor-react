import {_REQUEST } from 'redux/baseTypes';
import {
    GET_DESTINATION_LIST,
    GET_DESTINATION_ITEM,
    DELETE_DESTINATION
} from 'redux/actions/actionTypes';

function getDestinationList(pageNumber = 0) {
  if( pageNumber && parseInt( pageNumber ) > 1 ) var type = GET_DESTINATION_LIST + _REQUEST;
    else var type = GET_DESTINATION_LIST + _REQUEST

  return {
    type: type,
    endpoint: `orders/addrs/`,
}
}

function getDestinationItem(id) {
    return {
      type: GET_DESTINATION_ITEM + _REQUEST,
      endpoint: `orders/addrs/${id}/`
  }
}

function deleteDestination(newState) {//, currentPage
  return async (dispatch) => {
    await dispatch({
      type: DELETE_DESTINATION + _REQUEST,
      method: 'DELETE',
      endpoint: `orders/addrs/${newState.id}/`,
    });
    await dispatch(getDestinationList());
  };
}

export {
  getDestinationList,
  getDestinationItem,
  deleteDestination
};
