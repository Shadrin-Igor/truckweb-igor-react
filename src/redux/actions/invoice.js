import { _REQUEST } from 'redux/baseTypes';
import {
  GET_INVOICE_LIST,
  GET_INVOICE,
  CREATE_INVOICE,
  UPDATE_INVOICE,
  DELETE_INVOICE} from 'redux/actions/actionTypes';

function getInvoiceList(company) {
  return {
    type: GET_INVOICE_LIST + _REQUEST,
    endpoint: `orgs/company/${company.id}/invoices/`,
  }
}

function getInvoice(company, id) {
  return {
    type: GET_INVOICE + _REQUEST,
    endpoint: `orgs/company/${company.id}/invoices/${id}/`,
  };
}

function createInvoice(company, newState) {
  return {
    type: CREATE_INVOICE + _REQUEST,
    method: 'POST',
    endpoint: `orgs/company/${company.id}/invoices/`,
    body: newState,
  };
}

function updateInvoice(company, newState) {
  return async (dispatch) => {
    await dispatch({
      type: UPDATE_INVOICE + _REQUEST,
      method: 'PATCH',
      endpoint: `orgs/company/${company.id}/invoices/${newState.id}/`,
      body: {
        ...newState,
        broker: newState.broker && newState.broker.id,
        date: newState.date || null,
        due_date: newState.due_date || null,
      },
    });
    await dispatch(getInvoiceList(company));
  };
}

function deleteInvoice(company, newState) {
  return async (dispatch) => {
    await dispatch({
      type: DELETE_INVOICE + _REQUEST,
      method: 'DELETE',
      endpoint: `orgs/company/${company.id}/invoices/${newState.id}/`,
    });
    await dispatch(getInvoiceList(company));
  };
}

export {
  getInvoiceList,
  getInvoice,
  createInvoice,
  updateInvoice,
  deleteInvoice,
};
