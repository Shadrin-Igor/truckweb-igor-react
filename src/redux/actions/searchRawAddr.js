import { _REQUEST } from 'redux/baseTypes';
import {
  SEARCH_RAW_ADDR} from 'redux/actions/actionTypes';

function searchRawAddr(query) {
  return {
    type: SEARCH_RAW_ADDR + _REQUEST,
    endpoint: `orgs/addr/?search=${query}`,
  };
}

export {
  searchRawAddr,
};
