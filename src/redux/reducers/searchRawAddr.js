import { _SUCCESS } from 'redux/baseTypes';
import {
  SEARCH_RAW_ADDR} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  addrs: [],
};
function rawAddrSearchResult(state = initialState, action) {
  switch (action.type) {
    case SEARCH_RAW_ADDR + _SUCCESS:
      return {
        ...state,
        addrs: action.result.results,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  rawAddrSearchResult,
};
