import { _SUCCESS } from 'redux/baseTypes';
import {
  SEARCH_TRUCK} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  trucks: [],
};
function searchTruckResult(state = initialState, action) {
  switch (action.type) {
    case SEARCH_TRUCK + _SUCCESS:
      return {
        ...state,
        trucks: action.result.results,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  searchTruckResult,
};
