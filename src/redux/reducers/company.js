import { _SUCCESS, _REQUEST } from 'redux/baseTypes';
import {
  GET_COMPANY_LIST,
  GET_COMPANY,
  GET_MY_COMPANY,
} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  company: null
};

const ListInitialState = {
  pending: true,
  companies: [],
};

function companyList(state = ListInitialState, action) {
  switch (action.type) {
    case GET_COMPANY_LIST + _SUCCESS:
      return {
        ...state,
        companies: action.result.results,
        pending: false,
      };

    default:
      return state;
  }
}

function myCompany(state = initialState, action) {
  switch (action.type) {
    case GET_MY_COMPANY + _REQUEST:
      return {
        ...state,
        pending: true,
        company: null
      };

    case GET_MY_COMPANY + _SUCCESS:
      return {
        ...state,
        company: action.result,
        pending: false
      };

    default:
      return state;
  }
}

const CompanyInitialState = {
  pending: true,
  company: null
};

function companyItem(state = CompanyInitialState, action) {
  switch (action.type) {
    case GET_COMPANY + _SUCCESS:
      return {
        ...state,
        company: action.result,
        pending: false
      };

    default:
      return state;
  }
}

module.exports = {
  companyList,
  myCompany,
  companyItem,
};
