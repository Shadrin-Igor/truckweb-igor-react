import { _SUCCESS } from 'redux/baseTypes';
import {
  SEARCH_ORG} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  orgs: [],
};
function searchOrgResult(state = initialState, action) {
  switch (action.type) {
    case SEARCH_ORG + _SUCCESS:
      return {
        ...state,
        orgs: action.result.results,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  searchOrgResult,
};
