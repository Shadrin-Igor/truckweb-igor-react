import { _SUCCESS } from 'redux/baseTypes';
import {
  GET_DRIVER_LIST,
  GET_DRIVER,
  CREATE_DRIVER} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  drivers: [],
};
function driverList(state = initialState, action) {
  switch (action.type) {
    case GET_DRIVER_LIST + _SUCCESS:
      return {
        ...state,
        drivers: action.result.results,
        pending: false,
      };
    default:
      return state;
  }
}

const ItemInitialState = {
  pending: true,
  driver: {},
};
function driverItem(state = ItemInitialState, action) {
  switch (action.type) {
    case GET_DRIVER + _SUCCESS:
      return {
        ...state,
        driver: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

const createdInitialState = {
  pending: true,
  driver: {},
};
function createdDriver(state = createdInitialState, action) {
  switch (action.type) {
    case CREATE_DRIVER + _SUCCESS:
      return {
        ...state,
        driver: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  driverList,
  driverItem,
  createdDriver,
};
