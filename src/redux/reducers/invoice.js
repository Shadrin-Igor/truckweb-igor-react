import { _SUCCESS } from 'redux/baseTypes';
import {
  GET_INVOICE_LIST,
  GET_INVOICE,
  CREATE_INVOICE} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  invoices: [],
};
function invoiceList(state = initialState, action) {
  switch (action.type) {
    case GET_INVOICE_LIST + _SUCCESS:
      return {
        ...state,
        invoices: action.result.results,
        pending: false,
      };
    default:
      return state;
  }
}

const ItemInitialState = {
  pending: true,
  invoice: {},
};
function invoiceItem(state = ItemInitialState, action) {
  switch (action.type) {
    case GET_INVOICE + _SUCCESS:
      return {
        ...state,
        invoice: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

const createdInitialState = {
  pending: true,
  invoice: {},
};
function createdInvoice(state = createdInitialState, action) {
  switch (action.type) {
    case CREATE_INVOICE + _SUCCESS:
      return {
        ...state,
        invoice: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  invoiceList,
  invoiceItem,
  createdInvoice,
};
