import { _SUCCESS } from 'redux/baseTypes';
import {
  SEARCH_DRIVER} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  drivers: [],
};
function searchDriversResult(state = initialState, action) {
  switch (action.type) {
    case SEARCH_DRIVER + _SUCCESS:
      return {
        ...state,
        drivers: action.result.results,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  searchDriversResult,
};
