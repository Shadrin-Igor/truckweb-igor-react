import { _REQUEST, _SUCCESS, _FAILURE } from 'redux/baseTypes';
import {
  LOGOUT,
  LOGIN,
  USER_CHECK } from 'redux/actions/actionTypes';
import Base64 from 'js-base64';
import localStorage from 'localStorage';

const { encode } = Base64.Base64;

const initialState = {
  isAuthenticated: false,
  token: null,
  user: {},
  pending: true,
  error: null,
};

function auth (state = initialState, action) {
  switch (action.type) {

    case LOGIN:
      const token = encode(action.username + ":" + action.password);
      localStorage.setItem('token', token);
      return {
        ...state,
        token,
        pending: true
      };

    case USER_CHECK + _REQUEST:
      return {
        ...state,
        pending: true,
        token: localStorage.getItem('token')
      };

    case USER_CHECK + _FAILURE:
      console.log(111111111, state, action)
      return {
        ...state,
        pending: false,
        token: null,
        user: {},
        isAuthenticated: false,
        error: action.error,
      };

    case USER_CHECK + _SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: action.result,
        pending: false,
        token: localStorage.getItem('token')
      };

    case LOGOUT:
      localStorage.removeItem('token');
      return {
        ...state,
        isAuthenticated: false,
        token: null,
        user: null
      };

    default:
      return state;
  }

};

module.exports = {
  auth,
};
