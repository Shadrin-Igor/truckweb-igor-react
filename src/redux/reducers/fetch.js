import { _SUCCESS } from 'redux/baseTypes';
import _ from 'lodash';
import {
  FETCH_START_SAVE,
  FETCH_STOP_SAVE} from 'redux/actions/actionTypes';

const initialState = {
  saveInProgress: [],
};
function fetch(state = initialState, action) {
  switch (action.type) {
    case FETCH_START_SAVE:
      return {
        ...state,
        saveInProgress: state.saveInProgress.concat([action.fetchUrl]),
      };
    case FETCH_STOP_SAVE:
      var indexToRemove = _.indexOf(state.saveInProgress, action.fetchUrl);
      return {
        ...state,
        saveInProgress: _.remove(state.saveInProgress, function(n, i) {return i != indexToRemove})
      };
    default:
      return state;
  }
}

module.exports = {
  fetch,
};

