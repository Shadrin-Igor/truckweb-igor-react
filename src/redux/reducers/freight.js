import _ from 'lodash';
import { _SUCCESS } from 'redux/baseTypes';
import {
  GET_FREIGHT_LIST,
  GET_TRIP_FREIGHT_LIST,
  GET_FREE_FREIGHT_LIST,
  GET_FREIGHT,
  CREATE_FREIGHT } from 'redux/actions/actionTypes';

function mergeList(current, fromServer) {
  return _.map(fromServer, (value) => {
    return _.defaultsDeep(_.cloneDeep(value), _.find(current, {id: value.id}));
  });
}

const freightListInitialState = {
  pending: true,
  freights: [],
};

function freightList(state = freightListInitialState, action) {
  var currentState;

  switch (action.type) {
    case GET_FREIGHT_LIST + _SUCCESS:
      return {
        ...state,
        freights: mergeList(state, action.result.results),
        pending: false,
      };

    case GET_FREIGHT + _SUCCESS:
      currentState = {
        ...state,
        pending: false,
      };

      currentState.freights = _.map(currentState.freights, (freight) => {
        if (freight.id === action.result.id) {
          var updatedFreight = _.defaultsDeep(_.cloneDeep(action.result), freight);
          updatedFreight.origins = mergeList(freight.origins, action.result.origins);
          updatedFreight.destinations = mergeList(freight.destinations, action.result.destinations);
          return updatedFreight;
        }
        return freight;
      });

      return currentState;

    default:
      return state;
  }
}

const tripFreightInitialState = {
  pending: true,
  freights: [],
};

function tripFreightList(state = tripFreightInitialState, action) {
  var currentState;

  switch (action.type) {
    case GET_TRIP_FREIGHT_LIST + _SUCCESS:
      return {
        ...state,
        freights: mergeList(state, action.result.results),
        pending: false,
      };

    case GET_FREIGHT + _SUCCESS:
      currentState = {
        ...state,
        pending: false,
      };

      currentState.freights = _.map(currentState.freights, (freight) => {
        if (freight.id === action.result.id) {
          var updatedFreight = _.defaultsDeep(_.cloneDeep(action.result), freight);
          updatedFreight.origins = mergeList(freight.origins, action.result.origins);
          updatedFreight.destinations = mergeList(freight.destinations, action.result.destinations);
          return updatedFreight;
        }
        return freight;
      });

      return currentState;

    default:
      return state;
  }
}

function freeFreightList(state = tripFreightInitialState, action) {
  var currentState;

  switch (action.type) {
    case GET_FREE_FREIGHT_LIST + _SUCCESS:
      return {
        ...state,
        freights: mergeList(state, action.result.results),
        pending: false,
      };

      return currentState;

    default:
      return state;
  }
}

const freightItemInitialState = {
  pending: true,
  freight: {},
};

function freightItem(state = freightItemInitialState, action) {
  switch (action.type) {
    case GET_FREIGHT + _SUCCESS:
      return {
        ...state,
        freight: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

const freightCreatedInitialState = {
  pending: true,
  freight: {},
};
function freightCreated(state = freightCreatedInitialState, action) {
  switch (action.type) {
    case CREATE_FREIGHT + _SUCCESS:
      return {
        state,
        freight: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  freightList,
  tripFreightList,
  freeFreightList,
  freightItem,
  freightCreated,
};
