import { _SUCCESS } from 'redux/baseTypes';
import { GET_DESTINATION_LIST,
  GET_DESTINATION_ITEM
} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  count :0,
  destinations: [],
};
function destinationList(state = initialState, action) {
  switch (action.type) {
    case GET_DESTINATION_LIST + _SUCCESS:
      return {
        ...state,
        destinations: action.result.results,
        count: action.result.count,
        pending: false,
      };
    default:
      return state;
  }
}

const ItemInitialState = {
  pending: true,
  destination: [],
};
function destinationItem(state = ItemInitialState, action) {
  switch (action.type) {
    case GET_DESTINATION_ITEM + _SUCCESS:
      return {
        ...state,
        destination: action.result,
      pending: false,
  };
default:
  return state;
}
}

module.exports = {
  destinationList,
  destinationItem
};
