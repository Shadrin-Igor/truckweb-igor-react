import { _SUCCESS } from 'redux/baseTypes';
import {
  SEARCH_DISPATCHER} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  dispatchers: [],
};
function dispatcherSearchResult(state = initialState, action) {
  switch (action.type) {
    case SEARCH_DISPATCHER + _SUCCESS:
      return {
        ...state,
        dispatchers: action.result.results,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  dispatcherSearchResult,
};
