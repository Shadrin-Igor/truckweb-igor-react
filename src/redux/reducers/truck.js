import { _SUCCESS } from 'redux/baseTypes';
import {
  GET_TRUCK_LIST,
  GET_TRUCK,
  CREATE_TRUCK} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  trucks: [],
};
function truckList(state = initialState, action) {
  switch (action.type) {
    case GET_TRUCK_LIST + _SUCCESS:
      return {
        ...state,
        trucks: action.result.results,
        pending: false,
      };
    default:
      return state;
  }
}

const ItemInitialState = {
  pending: true,
  truck: {},
};
function truckItem(state = ItemInitialState, action) {
  switch (action.type) {
    case GET_TRUCK + _SUCCESS:
      return {
        ...state,
        truck: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  truckList,
  truckItem,
};
