import { _SUCCESS } from 'redux/baseTypes';

import {
  GET_FULL_TRIP_LIST,
  GET_TRIP,
  CREATE_TRIP} from 'redux/actions/actionTypes';

function mergeList(current, fromServer) {
  return _.map(fromServer, (value) => {
    return _.defaultsDeep(_.cloneDeep(value), _.find(current, {id: value.id}));
  });
}


const fullTripsInitialState = {
  pending: true,
  trips: [],
};
function fullTrips(state = fullTripsInitialState, action) {
  switch (action.type) {
    case GET_FULL_TRIP_LIST + _SUCCESS:
      return {
        ...state,
        trips: mergeList(state, action.result.results),
        pending: false,
      };

    default:
      return state;
  }
}


const ItemInitialState = {
  pending: true,
  trip: {},
};
function tripItem(state = ItemInitialState, action) {
  switch (action.type) {
    case GET_TRIP + _SUCCESS:
      return {
        ...state,
        trip: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

const tripCreatedInitialState = {
  pending: true,
  trip: {},
};
function tripCreated(state = tripCreatedInitialState, action) {
  switch (action.type) {
    case CREATE_TRIP + _SUCCESS:
      return {
        ...state,
        trip: action.result,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  tripItem,
  fullTrips,
  tripCreated,
};
