import { _SUCCESS } from 'redux/baseTypes';
import {
  SEARCH_FREIGHT_ADDR} from 'redux/actions/actionTypes';

const initialState = {
  pending: true,
  addrs: [],
};
function freightAddrSearchResult(state = initialState, action) {
  switch (action.type) {
    case SEARCH_FREIGHT_ADDR + _SUCCESS:
      return {
        ...state,
        addrs: action.result.results,
        pending: false,
      };

    default:
      return state;
  }
}

module.exports = {
  freightAddrSearchResult,
};
