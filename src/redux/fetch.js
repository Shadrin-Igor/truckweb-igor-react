import isomorphic_fetch from 'isomorphic-fetch';
import localStorage from 'localStorage';
import appConfig from 'app/appConfig';

export default function(url, body, method, contentType) {
  let preparedContentType, preparedBody;
  if (method == 'POST') {
    //preparedContentType = 'application/x-www-form-urlencoded';
    preparedContentType = 'application/json';
    //preparedBody = '';
    preparedBody = JSON.stringify(body);
  }
  else {
    preparedContentType = 'application/json';
    preparedBody = JSON.stringify(body);
  }

  if (contentType) {
    preparedContentType = contentType;

    preparedBody = new FormData();
    preparedBody.append('logo', body.file, body.file.name);
  }

  if(Boolean(url.indexOf('http') + 1)) {
    return isomorphic_fetch(`${url}`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Basic ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(body)
    });
  }

  return isomorphic_fetch(`${appConfig.apiBaseUrl}/${appConfig.apiVersion}/${url}`, {
    method,
    headers: {
      'Content-Type': preparedContentType,
      'Accept': 'application/json',
      'Authorization': `Basic ${localStorage.getItem('token')}`
    },
    //FormData(body),
    //body: JSON.stringify(body),
    body: preparedBody,
  });
}
