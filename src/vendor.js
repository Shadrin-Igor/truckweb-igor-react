//import EventEmitter2 from 'eventemitter2';
import $ from 'jquery';
import moment from 'moment';
import * as reactBootstrap from 'react-bootstrap';

// need only for rubix
global.React = require('react');
global.ReactDOM = require('react/lib/ReactDOM');
// need only for x-editable
window.jQuery = $;

// TODO: remove
// need only for rubix
window.$ = $;

require('bootstrap');
require('x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js');
require('perfect-scrollbar/jquery')($);

global.isBrowser = 'document' in window;

// Setup ReactBootstrap
// global.ReactBootstrap = {
//   Dispatcher: new EventEmitter2({
//     maxListeners: 999999999
//   })
// };

moment.locale('en');

global.Grid = reactBootstrap.Grid;
global.Row = reactBootstrap.Row;
global.Col = reactBootstrap.Col;
global.Table = reactBootstrap.Table;
global.Panel = reactBootstrap.Panel;
global.Image = reactBootstrap.Image;
global.Pagination = reactBootstrap.Pagination;
global.Page = reactBootstrap.Page;
global.Tabs = reactBootstrap.Tabs;
global.Tab = reactBootstrap.Tab;

global.Form = reactBootstrap.Form;
global.FormGroup = reactBootstrap.FormGroup;
global.ControlLabel = reactBootstrap.ControlLabel;
global.Label = reactBootstrap.Label;
global.Input = reactBootstrap.Input;
global.Button = reactBootstrap.Button;
global.Checkbox = reactBootstrap.Checkbox;
global.HelpBlock = reactBootstrap.HelpBlock;
global.FormControl = reactBootstrap.FormControl;
global.DropdownButton = reactBootstrap.DropdownButton;
global.MenuItem = reactBootstrap.MenuItem;
