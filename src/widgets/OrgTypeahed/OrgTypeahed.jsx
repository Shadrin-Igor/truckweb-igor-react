// TODO: !!!! unused

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Typeahead } from 'react-typeahead';

import { searchOrg } from 'redux/actions/searchOrg';

@connect(
  state => ({searchOrgResult: state.searchOrgResult}),
  {searchOrg}
)
export default class OrgTypeahed extends React.Component {
  static propTypes = {
    onSelect: PropTypes.func,
    point: PropTypes.object,
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      options: [],
    };
  }

  handleKeyUp(event) {
    // TODO: add debounce
    var query = event.target.value;

    if (query.length > 0) {
      this.props.searchOrg(query).then(() => {
        this.setState({options: [].concat(this.props.searchOrgResult.orgs)});
      });
    }
  }

  handleSelect(point) {
    this.props.onSelect(point);
  }

  render() {
    return (
      <Typeahead className="dropdown" customClasses={{results: "dropdown-menu"}}
                 value={(this.props.point) ? this.props.point.name_legal : ''}
                 options={this.state.options}
                 displayOption="name_legal"
                 formInputOption="name_legal"
                 filterOption="name_legal"
                 disabled={this.props.disabled}
                 onKeyUp={::this.handleKeyUp}
                 onOptionSelected={this.props.onSelect} />
    );
  }
}
