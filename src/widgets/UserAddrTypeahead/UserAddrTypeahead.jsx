// TODO: !!!! unused

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Typeahead } from 'react-typeahead';
import _ from 'lodash';

import { searchUserAddr } from 'redux/actions/searchUserAddr';

@connect(
  state => ({
    userAddrSearchResult: state.userAddrSearchResult}),
  { searchUserAddr}
)
export default class UserAddrTypeahead extends React.Component {
  static propTypes = {
    onSelect: PropTypes.func,
    point: PropTypes.object,
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      options: [],
    };

    //this.type = this.props.showParam || 'street';
  }

  handleKeyUp(event) {
    // TODO: add debounce
    var query = event.target.value;
    if (query.length > 0) {
      this.props.searchUserAddr(query).then(() => {
        if (_.isEmpty(this.props.userAddrSearchResult.addrs)) return;
        this.setState({options: this.props.userAddrSearchResult.addrs});
      });
    }
  }

  handleSelect(point) {
    this.props.onSelect(point);
  }

  render() {
    return (
      <Typeahead className="dropdown" customClasses={{results: "dropdown-menu"}}
                 value={(this.props.point) ? this.props.point.addr_full : ''}
                 options={this.state.options}
                 displayOption="addr_full"
                 formInputOption="addr_full"
                 filterOption="addr_full"
                 disabled={this.props.disabled}
                 onKeyUp={::this.handleKeyUp}
                 onOptionSelected={this.props.onSelect} />
    );
  }
}
