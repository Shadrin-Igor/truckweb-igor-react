import React, { PropTypes } from 'react';
import { connect } from 'react-redux'
import _ from 'lodash';
import Highlighter from 'react-highlight-words';
import Select from 'react-select';

import { searchBroker } from 'redux/actions/searchOrg';

@connect(
  state => ({searchOrgResult: state.searchOrgResult}),
  { searchBroker }
)
export default class SelectBroker extends React.Component {
  static propTypes = {
    value: PropTypes.object,
    defaultValue: PropTypes.object,
    placeholder: PropTypes.string,
    clearable: PropTypes.bool,
    disabled: PropTypes.bool,
    onSelect: PropTypes.func,
  };

  static defaultProps = {
    clearable: true,
  };

  constructor(props) {
    super(props);

    this._inputValue = '';

    this.state = {
      selectValue: (this.props.value) ? this.props.value : this.props.defaultValue,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value && !_.isEqual(nextProps.value, this.props.value)) {
      this.setState({selectValue: nextProps.value});
    }
  }
  
  handleChange(selectValue) {
    if (this.props.onSelect) {
      if (this.props.onSelect(selectValue) !== false) {
        this.setState({selectValue: selectValue});
      }
    }
    else {
      this.setState({selectValue: selectValue});
    }
  }

  handleFilterOptions(options, inputValue) {
    this._inputValue = inputValue;
    return options;
  }

  loadData(input) {
    return new Promise((resolve, reject) => {
      this.props.searchBroker(input).then(() => {
        resolve({
          options: this.props.searchOrgResult.orgs,
        });
      }, reject);
    });
  }

  renderOption(option) {
    return (
      <Highlighter
        highlightClassName='search-field__highlight'
        searchWords={[this._inputValue]}
        textToHighlight={option.disp_name || ''}
      />
    );
  }

  render() {
    return (
      <Select.Async
        placeholder={this.props.placeholder}
        value={this.state.selectValue}
        valueKey="id"
        labelKey="disp_name"
        cache={false}
        clearable={this.props.clearable}
        disabled={this.props.disabled}
        loadOptions={::this.loadData}
        onChange={::this.handleChange}
        optionRenderer={::this.renderOption}
        // workaround for update list after search request
        filterOptions={::this.handleFilterOptions}
      />
    );
  }
}
