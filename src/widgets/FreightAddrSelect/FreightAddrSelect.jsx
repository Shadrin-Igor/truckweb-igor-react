import React, { PropTypes } from 'react';
import { connect } from 'react-redux'
import _ from 'lodash';
import Highlighter from 'react-highlight-words';
import Select from 'react-select';

import { searchFreightAddr } from 'redux/actions/searchFreightAddr';

@connect(
  state => ({freightAddrSearchResult: state.freightAddrSearchResult}),
  { searchFreightAddr }
)
export default class FreightAddrSelect extends React.Component {
  static propTypes = {
    value: PropTypes.object,
    labelKey: PropTypes.string,
    defaultValue: PropTypes.number,
    placeholder: PropTypes.string,
    clearable: PropTypes.bool,
    disabled: PropTypes.bool,
    onSelect: PropTypes.func,
  };

  static defaultProps = {
    clearable: true,
    labelKey: 'addr_full',
  };

  constructor(props) {
    super(props);

    this._inputValue = '';

    this.state = {
      selectValue: (this.props.value) ? this.props.value : this.props.defaultValue,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value && !_.isEqual(nextProps.value, this.props.value)) {
      this.setState({selectValue: nextProps.value});
    }
  }

  handleChange(selectValue) {
    if (this.props.onSelect) {
      if (this.props.onSelect(selectValue) !== false) {
        this.setState({selectValue});
      }
    }
    else {
      this.setState({selectValue});
    }
  }

  handleFilterOptions(options, inputValue) {
    this._inputValue = inputValue;
    return options;
  }

  loadData(input) {
    return new Promise((resolve, reject) => {
      this.props.searchFreightAddr(input).then(() => {
        resolve({
          options: this.props.freightAddrSearchResult.addrs,
        });
      }, reject);
    });
  }

  renderOption(option) {
    return (
      <div>
        <div className="search-field__title">
          <Highlighter
            highlightClassName='search-field__highlight'
            searchWords={[this._inputValue]}
            textToHighlight={option.title || ''}
          />
        </div>
        <div className="search-field__body">
          {option.addr_full}
        </div>
      </div>
    );
  }

  render() {
    return (
      <Select.Async
        placeholder={this.props.placeholder}
        value={this.state.selectValue}
        valueKey="id"
        labelKey={this.props.labelKey}
        cache={false}
        clearable={this.props.clearable}
        disabled={this.props.disabled}
        loadOptions={::this.loadData}
        onChange={::this.handleChange}
        optionRenderer={::this.renderOption}
        // workaround for update list after search request
        filterOptions={::this.handleFilterOptions}
        //noResultsText={this.props.placeholder}
      />
    )
  }
}
