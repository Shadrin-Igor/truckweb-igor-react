import React, { PropTypes } from 'react';
import { connect } from 'react-redux'
import _ from 'lodash';
import Highlighter from 'react-highlight-words';
import Select from 'react-select';

import { searchTruck } from 'redux/actions/searchTruck';
import { getMyCompany } from 'redux/actions/company';

@connect(
  state => ({
    searchTruckResult: state.searchTruckResult,
    myCompany: state.myCompany,
  }),
  { searchTruck,
    getMyCompany }
)
export default class SelectTruck extends React.Component {
  static propTypes = {
    value: PropTypes.object,
    defaultValue: PropTypes.object,
    placeholder: PropTypes.string,
    clearable: PropTypes.bool,
    disabled: PropTypes.bool,
    onSelect: PropTypes.func,
    searchTruck: PropTypes.func,
    getMyCompany: PropTypes.func,
  };

  static defaultProps = {
    clearable: true,
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
    };

    this._inputValue = '';

    this.state = {
      selectValue: (this.props.value) ? this.props.value : this.props.defaultValue,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value && !_.isEqual(nextProps.value, this.props.value)) {
      this.setState({selectValue: nextProps.value});
    }
  }

  handleChange(selectValue) {
    if (this.props.onSelect) {
      if (this.props.onSelect(selectValue) !== false) {
        this.setState({selectValue: selectValue});
      }
    }
    else {
      this.setState({selectValue: selectValue});
    }
  }

  handleFilterOptions(options, inputValue) {
    this._inputValue = inputValue;
    return options;
  }

  loadData(input) {

    // this.props.getMyCompany().then(() => {
    //   this.props.searchTruck(this.props.myCompany.company).then(() => {
    //     this.setState({isLoaded: true});
    //   });
    // });

    return new Promise((resolve, reject) => {
      this.props.searchTruck(this.props.myCompany.company, input).then(() => {
        resolve({
          options: this.props.searchTruckResult.trucks,
        });
      }, reject);
    });
  }

  renderOption(option) {
    return (
      <Highlighter
        highlightClassName='search-field__highlight'
        searchWords={[this._inputValue]}
        textToHighlight={option.truckid || ''}
      />
    );
  }

  render() {
    return (
      <Select.Async
        placeholder={this.props.placeholder}
        value={this.state.selectValue}
        valueKey="id"
        labelKey="truckid"
        cache={false}
        clearable={this.props.clearable}
        disabled={this.props.disabled}
        loadOptions={::this.loadData}
        onChange={::this.handleChange}
        optionRenderer={::this.renderOption}
        // workaround for update list after search request
        filterOptions={::this.handleFilterOptions}
      />
    );
  }
}
