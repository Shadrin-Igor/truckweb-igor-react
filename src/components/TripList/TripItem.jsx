import React, { PropTypes } from 'react';
import _ from 'lodash';

import BrockerBlock from 'baseComponents/table/BrockerBlock';
import AddressBlock from 'baseComponents/table/AddressBlock';
import DateDurationFormatter from 'baseComponents/formatters/DateDurationFormatter';

export default class TripItem extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onSelect: PropTypes.func,
    onRemove: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  getDriversNames() {
    var names = [];
    if (this.props.item.driver) names.push(this.props.item.driver.disp_name);
    if (this.props.item.driver2) names.push(this.props.item.driver2.disp_name);
    return names.join(', ');
  }

  handleRemove(event) {
    event.stopPropagation();
    this.props.onRemove();
  }

  render() {
    return (
      <tr>
        {/* Trip */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block">{this.props.item.id}</div>
        </td>

        {/* Load# */}
        <td className="table-grid__load-col" onClick={() => this.props.onSelect(this.props.item)}>
          {_.map(this.props.item.orders, (freight) => {
            return <div className="table-block">
              <span className="mark-color">{freight.reference}</span>
            </div>
          })}
        </td>

        {/* Broker/tel. */}
        <td className="table-grid__broker-col" onClick={() => this.props.onSelect(this.props.item)}>
          {_.map(this.props.item.orders, (freight) => {
            return freight.broker && <div className="table-block" key={'brokers' + freight.id}>
                <BrockerBlock broker={freight.broker} />
              </div>
          })}
        </td>

        {/* Driver/Carrier */}
        <td className="trip__driver-col" onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block">{this.getDriversNames()}</div>
        </td>

        {/* truck */}
        <td className="trip__truck-col" onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block">{this.props.item.truck && this.props.item.truck.id}</div>
        </td>

        {/* Origin Address */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          {_.map(this.props.item.orders, (freight) => {
            return (
              <div key={'origins' + freight.id}>
                {_.map(freight.origins, (rpoint) => {
                  return (
                    <div className="table-block" key={'origins-rpoint' + rpoint.id}>
                      <AddressBlock rpoint={rpoint} />
                    </div>
                  )
                })}
              </div>
            )
          })}
        </td>

        {/* Pickup Time */}
        <td className="table-grid__date-col" onClick={() => this.props.onSelect(this.props.item)}>
          {_.map(this.props.item.orders, (freight) => {
            return (
              <div key={'origins-pick' + freight.id}>
                {_.map(freight.origins, (rpoint) => {
                  return (
                    <div className="table-block" key={'origins-pick-rpoint' + rpoint.id}>
                      {rpoint.date && <DateDurationFormatter dateStart={rpoint.date} dateEnd={rpoint.date_end} />}
                    </div>
                  )
                })}
              </div>
            )
          })}
        </td>

        {/* Destination Address */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          {_.map(this.props.item.orders, (freight) => {
            return (
              <div key={'destinations' + freight.id}>
                {_.map(freight.destinations, (rpoint) => {
                  return (
                    <div className="table-block" key={'destinations-rpoint' + rpoint.id}>
                      <AddressBlock rpoint={rpoint} />
                    </div>
                  )
                })}
              </div>
            )
          })}
        </td>

        {/* Delivery Time */}
        <td className="table-grid__date-col" onClick={() => this.props.onSelect(this.props.item)}>
          {_.map(this.props.item.orders, (freight) => {
            return (
              <div key={'destinations-pick' + freight.id}>
                {_.map(freight.destinations, (rpoint) => {
                  return (
                    <div key={'destinations-pick-rpoint' + rpoint.id} className="table-block">
                      {rpoint.date && <DateDurationFormatter dateStart={rpoint.date} dateEnd={rpoint.date_end} />}
                    </div>
                  )
                })}
              </div>
            )
          })}
        </td>

        {/* Control call time */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          {_.map(this.props.item.orders, (freight) => {
            return <div className="table-block">{freight.cct}</div>
          })}
        </td>

        {/* Note */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block font-size-90">
            <p title={this.props.item.note}>{this.props.item.note}</p>
          </div>
        </td>

        {/* Action */}
        <td>
          <DropdownButton id={`action-dropdown-${this.props.item.id}`} bsStyle={'link'} title="Action">
            <MenuItem eventKey="1" onClick={this.props.onSelect.bind(this, this.props.item)}>Edit</MenuItem>
            <MenuItem eventKey="2" onClick={::this.handleRemove}>Delete</MenuItem>
          </DropdownButton>
        </td>
      </tr>
    );
  }
}
