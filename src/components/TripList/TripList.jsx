import React, { PropTypes } from 'react';

import { confirm } from 'baseComponents/dialog/dialog';
import TripItem from './TripItem';

export default class extends React.Component {
  static propTypes = {
    list: PropTypes.array,
    onSelect: PropTypes.func,
    onRemove: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  handleRemove(item) {
    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to delete this trip?',
      onOk: () => {
        item.removing = true;
        this.forceUpdate();
        try {
          this.props.onRemove(item);
          // TODO: если произошла ошибка нужно item.removing сделать false или сделать это в редьюсере при обновлении
        } catch (err) {
          console.error(err);
        }

      },
    });
  }

  render() {
    return (
      <div>
        <table className="table table-bordered table-hover table-grid" cellSpacing="0" width="100%">
          <thead>
            <tr>
              <th>Trip</th>
              <th>Load#</th>
              <th>Broker/tel.</th>
              <th>Driver/Carrier</th>
              <th>Truck</th>
              <th>Origin Address</th>
              <th>Pickup Time</th>
              <th>Destination Address</th>
              <th>Delivery Time</th>
              <th>Control call time</th>
              <th>Note</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          {this.props.list.map((item) => {
            if (item && !item.removing) return (
              <TripItem key={item.id}
                        item={item}
                        onSelect={this.props.onSelect}
                        onRemove={this.handleRemove.bind(this, item)} />
            );
          })}
          </tbody>
        </table>
      </div>
    );
  }
}
