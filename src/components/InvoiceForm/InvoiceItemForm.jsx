import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import _ from 'lodash';

const fields = ['amount', 'destination', 'id', 'item_num', 'origin', 'quantity', 'rate', 'ref', 'service'];

const validate = values => {
  const errors = {};
  return errors;
};

class InvoiceItemForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    onRemove: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this._caclulateAmount(this.props.fields.quantity.value, this.props.fields.rate.value);
  }

  
  handleFieldChange(event) {
    this.props.fields[event.target.name].onChange(event);
    this.props.onChange(event.target.name, event.target.value);
  }

  handleQtyChange(event) {
    this.handleFieldChange(event);
    this._caclulateAmount(event.target.value, this.props.fields.rate.value);
  }

  handleRateChange(event) {
    this.handleFieldChange(event);
    this._caclulateAmount(this.props.fields.quantity.value, event.target.value);
  }

  _caclulateAmount(rawQty, rawRate) {
    var qtyNum = _.toNumber(rawQty);
    var rateNum = _.toNumber(rawRate);
    var result = qtyNum * rateNum;
    if (!_.isNaN(qtyNum)) {
      this.props.fields.amount.onChange(result);
    }
    this.props.onChange('amount', result);
  }

  render() {
    const {
      fields: { amount, destination, item_num, origin, quantity, rate, ref, service },
    } = this.props;

    return (
      <tr>
        <td></td>
        <td className="invoice-item__item-num-col">{item_num.value}</td>
        <td className="invoice-item__service-col">
          <input type='text' className="form-control" {...service}
                 onChange={::this.handleFieldChange} />
        </td>
        <td className="invoice-item__ref-col">
          <input type='text' className="form-control" {...ref}
                 onChange={::this.handleFieldChange} />
        </td>
        <td>
          <input type='text' className="form-control" {...origin}
                 onChange={::this.handleFieldChange} />
        </td>
        <td>
          <input type='text' className="form-control" {...destination}
                 onChange={::this.handleFieldChange} />
        </td>
        <td className="invoice-item__qty-col">
          <input type='text' className="form-control" {...quantity}
                 onChange={::this.handleQtyChange} />
        </td>
        <td className="invoice-item__rate-col">
          <div className="invoice__currency">
            <div className="invoice__currency_sign">$</div>
            <div>
              <input type='text' className="form-control" {...rate}
                     onChange={::this.handleRateChange} />
            </div>
          </div>
        </td>
        <td className="invoice-item__amount-col">
          <div className="invoice__currency">
            <div className="invoice__currency_sign">$</div>
            <div>
              <input type='text' className="form-control" {...amount}
                     onChange={::this.handleFieldChange} />
            </div>
          </div>
        </td>
        <td className="invoice-item__action-col">
          <button type="button" className="btn btn-default btn-xs" onClick={this.props.onRemove}>
            <span className="glyphicon glyphicon-trash" /></button>
        </td>
      </tr>
    );
  }
}

export default reduxForm({
  form: 'InvoiceItemForm',
  fields: fields,
  validate,
})(InvoiceItemForm)
