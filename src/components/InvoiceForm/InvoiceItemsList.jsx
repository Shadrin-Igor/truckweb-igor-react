import React, { PropTypes } from 'react';
import _ from 'lodash';

import InvoiceItemForm from 'components/InvoiceForm/InvoiceItemForm';

export default class extends React.Component {
  static propTypes = {
    items: PropTypes.array,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      items: this.props.items || [],
    }
  }

  handleRemoveItem(item, index) {
    var newState = _.clone(this.state.items);
    newState[index] = null;
    newState = _.compact(newState);

    this.setState({items: _.compact(newState)});
    this.props.onChange(newState);
  }

  addLine(event) {
    event.preventDefault();
    var newState = [].concat(this.state.items, [{
      quantity: 1,
    }]);

    this.setState({
      items: newState,
    });
    this.props.onChange(newState);
  }

  clearLines(event) {
    event.preventDefault();
    var newState = [];
    this.setState({
      items: newState,
    });
    this.props.onChange(newState);
  }

  handleItemChange(index, fieldName, newValue) {
    var newState = [].concat(this.state.items);

    newState[index][fieldName] = newValue;

    this.setState({
      items: newState,
    });
    this.props.onChange(newState);
  }

  render() {
    return (
      <div>
        <table className="table table-bordered table-hover table-grid invoice__items" cellSpacing="0" width="100%">
          <thead>
          <tr>
            <th></th>
            <th>#</th>
            <th>Service</th>
            <th>Ref#</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>QTY</th>
            <th>RATE</th>
            <th>AMOUNT</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
            {_.map(this.state.items, (item, index) => {
              return (
                <InvoiceItemForm initialValues={item}
                                 key={'item' + index}
                                 formKey={'itemForm' + index}
                                 onChange={this.handleItemChange.bind(this, index)}
                                 onRemove={this.handleRemoveItem.bind(this, item, index)} />
              );
            })}
          </tbody>
        </table>

        <Row>
          <Col sm={6} className="invoice__list-buttons">
            <button className="add-btn" onClick={::this.addLine}>Add lines</button>
            <button className="add-btn" onClick={::this.clearLines}>Clear lines</button>
            {/*
             <button className="add-btn" onClick={::this.addSubtotal}>Add subtotal</button>
            */}
          </Col>
          {/*
          <Col sm={6} className="invoice__sub-total">
            Subtotal $ 1234
          </Col>
           */}
        </Row>
      </div>

    );
  }
}
