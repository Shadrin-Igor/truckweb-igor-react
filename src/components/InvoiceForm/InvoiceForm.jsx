import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';

import Field from 'baseComponents/Field/Field';
import DateField from 'baseComponents/DateField/DateField';
import SelectBroker from 'widgets/SelectBroker/SelectBroker';
import InvoiceItemsList from 'components/InvoiceForm/InvoiceItemsList';

const fields = ['id', 'bill_addr', 'broker', 'date', 'due_date', 'email', 'items', 'memo', 'message', 'terms'];

const validate = values => {
  const errors = {};
  return errors;
};

class InvoiceForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    onCancel: PropTypes.func,
    onFormSubmit: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      total: 0,
    };
  }

  componentDidMount() {
    this._calculateTotal(this.props.fields.items.value);
  }

  handleFormSubmit(data) {
    this.props.onFormSubmit(data);
  }

  handleCustomerSelect(customer) {
    this.props.fields.broker.onChange(customer);
    // copy email
    this.props.fields.email.onChange(customer.email);
    // copy billing address
    _.find(customer.address, (addr) => {
      if (addr.addr_type === 1) {
        this.props.fields.bill_addr.onChange(addr.addr_full);
      }
    });
  }

  handleTermsChange(event) {
    this.props.fields.terms.onChange(event);

    this._calculateDueDate(event.target.value, this.props.fields.date.value);
  }

  handleInvoiceDateChange(dateTime) {
    if (!moment(dateTime).isValid()) return;

    //it converts from 03/11/16 to 2016-03-11T12:00:00Z
    var formatted = moment.utc(dateTime).format();
    this.props.fields.date.onChange(formatted);

    this._calculateDueDate(this.props.fields.terms.value, formatted);
  }

  handleItemsChange(items) {
    this.props.fields.items.onChange(items);
    this._calculateTotal(items);
  }

  _calculateTotal(items) {
    var total = 0;
    _.each(items, (item) => {
      var num = _.toNumber(item.amount);
      if (!_.isNaN(num)) total += num;
    });
    this.setState({total});
  }

  _calculateDueDate(rawTerms, rawInvoiceDate) {
    var term = _.toNumber(rawTerms);
    if (_.isNaN(term)) return;

    var invoiceDate = moment(rawInvoiceDate);
    if (!invoiceDate.isValid()) return;

    var dueDate = invoiceDate.add(term, 'days');

    this.props.fields.due_date.onChange(dueDate);
  }

  render() {
    const {
      fields: { bill_addr, broker, date, due_date, email, items, memo, message, terms },
      handleSubmit,
      submitting
    } = this.props;

    return (
      <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <div className="invoice__line1-bg">
          <table width="100%" className="invoice__line1">
            <tbody>
              <tr>
                <td className="invoice__customer-col">
                  <label className="control-label">Truck Inc</label>
                  <SelectBroker
                    allowEmpty={true}
                    value={broker.value}
                    placeholder="Choose a customer"
                    onSelect={::this.handleCustomerSelect} />
                </td>
                <td className="invoice__email-col">
                  <FormGroup>
                    <Col componentClass={ControlLabel} sm={3}>Email</Col>
                    <Col sm={9}>
                      <Field errorMsg="">
                        <input type='text' className="form-control" {...email}
                               placeholder="e-mail" />
                      </Field>
                    </Col>
                  </FormGroup>
                </td>
                <td className="invoice__balance-col">
                  <label className="control-label">Total</label>
                  <div className="invoice__balance-value">$ {this.state.total}</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <table className="invoice__line2">
          <tbody>
            <tr>
              <td>
                <label className="control-label">Billing address</label>
                <textarea rows='3' className="form-control" {...bill_addr} />
              </td>
              <td>
                <label className="control-label">Terms</label>
                <input type='text' className="form-control" {...terms}
                       onChange={::this.handleTermsChange} />
              </td>
              <td>
                <label className="control-label">Invoice date</label>
                <DateField value={date.value}
                           onChange={::this.handleInvoiceDateChange} />
              </td>
              <td>
                <label className="control-label">Due date</label>
                <p className="form-control-static">{moment(due_date.value).format('L')}</p>
              </td>
            </tr>
          </tbody>
        </table>

        <InvoiceItemsList items={items.value} onChange={::this.handleItemsChange} />

        <div className="invoice__balance-total">
          Total $ {this.state.total}
        </div>

        <Row>
          <Col sm={3}>
            <label className="control-label">Message displayed on invoice</label>
            <div>
              <textarea rows='3' className="form-control" {...message} />
            </div>
          </Col>
          <Col sm={3}>
            <label className="control-label">Statement memo</label>
            <div>
              <textarea rows='3' className="form-control" {...memo} />
            </div>
          </Col>
        </Row>

        <FormGroup className="form-buttons">
          <Col sm={12}>
            <Button bsStyle="default" onClick={this.props.onCancel}>Cancel</Button>
            <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
          </Col>
        </FormGroup>

      </Form>
    );
  }
}

export default reduxForm({
  form: 'InvoiceForm',
  fields: fields,
  validate,
})(InvoiceForm)
