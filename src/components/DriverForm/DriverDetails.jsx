import React, { PropTypes } from 'react';

export default class BrokerDetails extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onClick: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="form-horizontal details" onClick={this.props.onClick}>
        <div className="form-group">
          <label className="col-sm-3 control-label">Contact person</label>
          <Col sm={9}>
            <p className="form-control-static">
              <span>
                {this.props.item.attributes.title && this.props.item.attributes.title == 1 && 'Mr.'}
                {this.props.item.attributes.title && this.props.item.attributes.title == 2 && 'Msr.'}
              </span>
              <span> {this.props.item.first_name} </span>
              <span>{this.props.item.last_name}</span>
            </p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Display name as</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.disp_name}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Mobile</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.attributes.mobile}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">E-mail</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.email}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Address</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.address}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Driver License</label>
          <Col sm={9}>
            <p className="form-control-static">
              <span>{this.props.item.attributes.drv_license_dl}</span>
              <span>{this.props.item.attributes.drv_license_exp}</span>
            </p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Truck</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.truck && this.props.item.truck.truckid}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Truck class</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.attributes.drv_class}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Salary</label>
          <Col sm={9}>
            <p className="form-control-static">
              <span>
                {(!this.props.item.attributes.salary_type || this.props.item.attributes.salary_type == 5) && 'None'}
                {this.props.item.attributes.salary_type && this.props.item.attributes.salary_type == 3 && 'Flat'}
                {this.props.item.attributes.salary_type && this.props.item.attributes.salary_type == 4 && 'Per mile'}
              </span>
              <span> {this.props.item.attributes.salary_value} </span>
              <span>% mile</span>
            </p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">Note</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.attributes.note}</p>
          </Col>
        </div>

      </div>
    );
  }
}
