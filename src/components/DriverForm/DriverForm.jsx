import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import _ from 'lodash';

import Field from 'baseComponents/Field/Field';
import SelectTruck from 'widgets/SelectTruck/SelectTruck';

const fields = ['id', 'disp_name', 'email', 'first_name', 'last_name', 'address', 'truck', 'selectedTruck',
  'attributes.title', 'attributes.note', 'attributes.mobile', 'attributes.bank_account',
  'attributes.drv_class', 'attributes.drv_license_dl', 'attributes.drv_license_exp',
  'attributes.salary_type', 'attributes.salary_value'
];

const validate = values => {
  const errors = {attributes: {}};
  if (!values.last_name) {
    errors.last_name = 'Required'
  }
  if (values.last_name && !values.disp_name) {
    errors.disp_name = 'Required'
  }
  if (values.attributes.salary_value && !(_.toNumber(values.attributes.salary_value) >=0)) {
    errors.attributes.salary_value = 'Only numbers are allow here'
  }
  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  return errors;
};

class DriverForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    onFormSubmit: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isDispNamePrestine: true,
    };
  }

  handleLastNameChange(event) {
    var disp_name = _.get(this.props.fields, 'disp_name');
    if (this.state.isDispNamePrestine)
      disp_name.onChange(event);

    _.get(this.props.fields, event.target.name).onChange(event);
  }

  handleDispNameChange(event) {
    if (event.target.value)
      this.setState({isDispNamePrestine: false});
    else
      this.setState({isDispNamePrestine: true});

    _.get(this.props.fields, event.target.name).onChange(event);
  }

  handleFormSubmit(data) {
    this.props.onFormSubmit(data);
  }

  handleTruckSelect(truck) {
    this.props.fields.selectedTruck.onChange((truck) ? truck.id : null);
  }

  render() {
    const {
      fields: { disp_name, address, email, first_name, last_name, truck, attributes },
      handleSubmit,
      submitting,
    } = this.props;

    return (
      <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Contact person</Col>
          <Col sm={2}>
            <FormControl componentClass="select" {...attributes.title}
                    defaultValue='1' >
              <option value='1'>Mr.</option>
              <option value='2'>Msr.</option>
            </FormControl>
          </Col>
          <Col sm={3}>
            <input type='text' className="form-control" placeholder="First name" {...first_name} />
          </Col>
          <Col sm={4}>
            <Field errorMsg={(this.props.dirty) ? last_name.error : ''}>
              <input type='text' className="form-control" placeholder="Last name" {...last_name}
                     onChange={::this.handleLastNameChange}/>
            </Field>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Display name as</Col>
          <Col sm={9}>
            <Field errorMsg={disp_name.error}>
              <input type='text' className="form-control" {...disp_name}
                     onChange={::this.handleDispNameChange} />
            </Field>
            <HelpBlock>Short name</HelpBlock>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Mobile / Email</Col>
          <Col sm={4}>
            <input type='text' className="form-control" placeholder="Mobile" {...attributes.mobile} />
          </Col>
          <Col sm={5}>
            <Field errorMsg={(this.props.dirty) ? email.error : ''}>
              <input type='text' className="form-control" placeholder="email" {...email} />
            </Field>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Address</Col>
          <Col sm={9}>
            <Row>
              <Col sm={12}>
                <input type='text' className="form-control" {...address} />
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <span className="text-muted">City/Town: </span>{address.value && address.value[0] && address.value[0].addr_city}
              </Col>
              <Col sm={3}>
                <span className="text-muted">State: </span>{address.value && address.value[0] && address.value[0].addr_region}
              </Col>
              <Col sm={3}>
                <span className="text-muted">ZIP: </span>{address.value && address.value[0] && address.value[0].addr_postalcode}
              </Col>
            </Row>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Bank Account</Col>
          <Col sm={9}>
            <textarea rows='3' {...attributes.bank_account} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Driver License</Col>
          <Col sm={5}>
            <input type='text' className="form-control" placeholder="DL" {...attributes.drv_license_dl} />
          </Col>
          <Col sm={4}>
            <input type='text' className="form-control" placeholder="EXP" {...attributes.drv_license_exp} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Truck ID</Col>
          <Col sm={3}>
            <SelectTruck
              allowEmpty={true}
              value={truck.value}
              onSelect={::this.handleTruckSelect}/>
          </Col>
          <Col sm={3}>
            <div>Class</div>
          </Col>
          <Col sm={3}>
            <input type='text' className="form-control" {...attributes.drv_class}
                   placeholder="Truck class" />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Salary</Col>
          <Col sm={3}>
            <FormControl componentClass="select" {...attributes.salary_type}
              defaultValue='5'>
              <option value='5'>None</option>
              <option value='3'>Flat</option>
              <option value='4'>Per mile</option>
            </FormControl>
          </Col>
          <Col sm={3}>
            <Field errorMsg={attributes.salary_value.error}>
              <input type='text' className="form-control" {...attributes.salary_value} />
            </Field>
          </Col>
          <Col sm={3}>
            <div>% mile</div>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Note</Col>
          <Col sm={9}>
            <textarea rows='3' {...attributes.note} />
          </Col>
        </FormGroup>

        <FormGroup className="form-buttons">
          <Col sm={12}>
            <Button bsStyle="default" onClick={this.props.onCancel}>Cancel</Button>
            <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'DriverForm',
  fields: fields,
  validate,
})(DriverForm)
