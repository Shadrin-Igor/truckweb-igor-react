import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import _ from 'lodash';

import AddrBaseFields from 'components/AddrForm/AddrBaseFields';
import Field from 'baseComponents/Field/Field';

const fields = ['id', 'disp_name', 'email', 'name_legal', 'address',
  'billing_address.addr_country', 'billing_address.addr_state', 'billing_address.addr_city', 'billing_address.addr_street', 'billing_address.addr_postalcode',
  'shipping_address.addr_country', 'shipping_address.addr_state', 'shipping_address.addr_city', 'shipping_address.addr_street', 'shipping_address.addr_postalcode',
  'dot', 'ein', 'mc', 'scac',
  'main_phone', 'note', 'site',
  'attributes.mobphone', 'attributes.fax', 'attributes.typerate', 'attributes.contact'];

const validate = values => {
  const errors = {};

  if (values.dot && !(_.toNumber(values.dot) >=0)) {
    errors.dot = 'Only numbers are allow here';
  }
  if (values.mc && !(_.toNumber(values.mc) >=0)) {
    errors.mc = 'Only numbers are allow here';
  }

  if (values.site && !values.site.match(/.+\..+/)) {
    errors.site = 'Enter a valid site url';
  }
  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.disp_name) {
    errors.disp_name = 'Required';
  }

  return errors;
};

class BrokerForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    onFormSubmit: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isDispNamePrestine: true,
      isShipingAddrSame: true,
    };

  }

  componentDidMount() {
    if (!_.isEmpty(this.props.fields.address.value)) {
      _.map(this.props.fields.address.value, (addr) => {
        if (addr.addr_type === 1) {
          this._initAddr('billing_address', addr);
        }
        else if (addr.addr_type === 2) {
          this._initAddr('shipping_address', addr);
          this.setState({ isShipingAddrSame: false });
        }
      })
    }
  }

  handleDispNameChange(event) {
    if (event.target.value)
      this.setState({isDispNamePrestine: false});
    else
      this.setState({isDispNamePrestine: true});

    _.get(this.props.fields, event.target.name).onChange(event);
  }

  handleFormSubmit(data) {
    var preparedData = {
      ...data,
      shipping_address: (this.state.isShipingAddrSame) ? undefined : data.shipping_address,
    };

    this.props.onFormSubmit(preparedData);
  }

  _initAddr(field, addr) {
    this.props.fields[field].addr_country.onChange(addr.addr_country);
    this.props.fields[field].addr_state.onChange(addr.addr_state);
    this.props.fields[field].addr_city.onChange(addr.addr_city);
    this.props.fields[field].addr_street.onChange(addr.addr_street);
    this.props.fields[field].addr_postalcode.onChange(addr.addr_postalcode);
  }

  render() {
    const {
      fields: { disp_name, email, name_legal,
        dot, ein, mc, scac,
        billing_address, shipping_address,
        main_phone, note, site, attributes },
      handleSubmit,
      submitting
    } = this.props;

    return (
      <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <Row>
          <Col sm={6}>
            <label className="control-label">Company</label>
            <div>
              <Field errorMsg={name_legal.error}>
                <input type='text' className="form-control" {...name_legal}
                       placeholder="Title Company" />
              </Field>
            </div>
          </Col>
          <Col sm={3}>
            <label className="control-label">Dot #</label>
            <div>
              <input type='text' className="form-control" {...dot}
                     placeholder="DOT # 1234567" />
            </div>
          </Col>
          <Col sm={3}>
            <label className="control-label">MC #</label>
            <div>
              <input type='text' className="form-control" {...mc}
                     placeholder="MC # 123456" />
            </div>
          </Col>
        </Row>

        <Row>
          <Col sm={6}>
            <label className="control-label">Display name as</label>
            <div>
              <Field errorMsg={(this.props.dirty || this.props.submitFailed) && disp_name.error}>
                <input type='text' className="form-control" {...disp_name}
                       placeholder="Print on check as display name"
                       onChange={::this.handleDispNameChange} />
              </Field>
            </div>
          </Col>
          <Col sm={3}>
            <label className="control-label">SCAC code</label>
            <div>
              <input type='text' className="form-control" {...scac}
                     placeholder="SCAC code" />
            </div>
          </Col>
          <Col sm={3}>
            <label className="control-label">Employer ID(EIN)</label>
            <div>
              <input type='text' className="form-control" {...ein}
                     placeholder="Employer ID(EIN)" />
            </div>
          </Col>
        </Row>

        <Row>
          <Col sm={6}>
            <label className="control-label">Contact person</label>
            <div>
              <input type='text' className="form-control" {...attributes.contact}
                     placeholder="Contact person" />
            </div>
          </Col>
          <Col sm={3}>
            <label className="control-label">Email</label>
            <div>
              <Field errorMsg={email.error}>
                <input type='text' className="form-control" {...email}
                       placeholder="Email" />
              </Field>
            </div>
          </Col>
          <Col sm={3}>
            <label className="control-label">Website</label>
            <div>
              <Field errorMsg={site.error}>
                <input type='text' className="form-control" {...site}
                       placeholder="Website" />
              </Field>
            </div>
          </Col>
        </Row>

        <Row className="form-group">
          <Col sm={3}>
            <label className="control-label">Type rate</label>
            <div>
              <FormControl componentClass="select" {...attributes.typerate}
                           defaultValue='5'>
                <option value='5'>None</option>
                <option value='3'>Flat</option>
                <option value='4'>Per mile</option>
              </FormControl>
            </div>
          </Col>
          <Col sm={3}></Col>
          <Col sm={2}>
            <label className="control-label">Mobile</label>
            <div>
              <input type='text' className="form-control" {...attributes.mobphone}
                     placeholder="Mobile" />
            </div>
          </Col>
          <Col sm={2}>
            <label className="control-label">Phone</label>
            <div>
              <input type='text' className="form-control" {...main_phone}
                     placeholder="Phone" />
            </div>
          </Col>
          <Col sm={2}>
            <label className="control-label">Fax</label>
            <div>
              <input type='text' className="form-control" {...attributes.fax}
                     placeholder="Fax" />
            </div>
          </Col>
        </Row>

        <Row>
          <Col sm={12}>
            <Tabs defaultActiveKey={1}>
              <Tab eventKey={1} title="Address">
                <Row>
                  <Col sm={6}>
                    <label className="control-label">Billing address</label>

                    <AddrBaseFields country={billing_address.addr_country}
                                    state={billing_address.addr_state}
                                    city={billing_address.addr_city}
                                    street={billing_address.addr_street}
                                    postalcode={billing_address.addr_postalcode} />
                  </Col>
                  <Col sm={6}>
                    <label className="control-label">Shipping address</label>
                    <Checkbox className="broker__shiping-same"
                              checked={this.state.isShipingAddrSame}
                              onClick={() => this.setState({isShipingAddrSame: !this.state.isShipingAddrSame})}>
                      Same as billing address
                    </Checkbox>
                    {!this.state.isShipingAddrSame &&
                      <AddrBaseFields country={shipping_address.addr_country}
                                      state={shipping_address.addr_state}
                                      city={shipping_address.addr_city}
                                      street={shipping_address.addr_street}
                                      postalcode={shipping_address.addr_postalcode}/>
                    }
                  </Col>
                </Row>
              </Tab>
              <Tab eventKey={2} title="Payment" disabled>Tab 2 content</Tab>
              <Tab eventKey={3} title="Attachments" disabled>Tab 3 content</Tab>
            </Tabs>
          </Col>
        </Row>

        <Row>
          <Col componentClass={ControlLabel} sm={1}>Note</Col>
          <Col sm={8}>
            <textarea rows='3' className="form-control" {...note}/>
          </Col>
        </Row>

        <FormGroup className="form-buttons">
          <Col sm={12}>
            <Button bsStyle="default" onClick={this.props.onCancel}>Cancel</Button>
            <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
          </Col>
        </FormGroup>

      </Form>
    );
  }
}

export default reduxForm({
  form: 'BrokerForm',
  fields: fields,
  validate,
})(BrokerForm)
