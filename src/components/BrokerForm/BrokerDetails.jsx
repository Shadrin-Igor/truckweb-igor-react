import _ from 'lodash';
import React, { PropTypes } from 'react';

export default class BrokerDetails extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onClick: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  _addrTypeFull(typeId) {
    if (_.isEmpty(this.props.item.address)) return;
    var addr = _.find(this.props.item.address, (addr) => {
      return addr.addr_type === typeId;
    });
    if (addr) return addr.addr_full;
  }

  render() {
    return (
      <div className="form-horizontal details" onClick={this.props.onClick}>
        <div className="form-group">
          <label className="col-sm-3 control-label">Company</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.name_legal}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Display name as</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.disp_name}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Contact person</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.attributes.contact}</p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">Phone</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.main_phone}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Mobile</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.attributes.mobphone}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Fax</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.attributes.fax}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">E-mail</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.email}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Site</label>
          <Col sm={9}>
            <p className="form-control-static">
              <a target="_blank" href={this.props.item.site}>{this.props.item.site}</a>
            </p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Type Rate</label>
          <Col sm={9}>
            <p className="form-control-static">
              {(!this.props.item.attributes.typerate || this.props.item.attributes.typerate == 5) && 'None'}
              {this.props.item.attributes.typerate && this.props.item.attributes.typerate == 3 && 'Flat'}
              {this.props.item.attributes.typerate && this.props.item.attributes.typerate == 4 && 'Per mile'}
            </p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">DOT #</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.dot}</p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">MC #</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.mc}</p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">SCAC #</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.scac}</p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">Employer ID(EIN)</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.ein}</p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">Billing address</label>
          <Col sm={9}>
            <p className="form-control-static">{this._addrTypeFull(1)}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Shipping address</label>
          <Col sm={9}>
            <p className="form-control-static">{this._addrTypeFull(2)}</p>
          </Col>
        </div>

        <div className="form-group">
          <label className="col-sm-3 control-label">Note</label>
          <Col sm={9}>
            <p className="form-control-static">{this.props.item.note}</p>
          </Col>
        </div>

      </div>
    );
  }
}
