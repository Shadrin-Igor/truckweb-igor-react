import React, { PropTypes } from 'react';
import TruckFromFields from './TruckFormFields.jsx'
import moment from 'moment';

export default class TruckDetails extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onClick: PropTypes.func
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if( this.props.item.owned >0  )
      var _this = this;
      this.props.getDriver(this.props.item.owned).then( (  )=>{

        if (this.props.driverItem.driver.id >= 0) {
          this.forceUpdate();
        }
      });
  }

  getFieldSelectValue( field, value ){
    return TruckFromFields.map( ( item => {
      if( !Array.isArray( item ) ) {
        if( item.name == field ){
          if( Array.isArray( item.options ) )
            return item.options.map( itemValue =>{
              if( itemValue.id == value )return itemValue.text;
            } )
        }
      }else {
        return item.map( subItem => {
          if( subItem.name == field ){
            if( Array.isArray( subItem.options ) )
              return subItem.options.map( subItemValue =>{
                if( subItemValue.id == value )return subItemValue.text;
              } )
          }
        } )
      }
    } ) )
  }

  getDateInFormat( value ) {
    if( value )
      return moment.utc(value).format("DD.MM.YYYY");
  }

  render() {
    return (
    <div className="form-horizontal details" onClick={this.props.onClick}>
      <div className="form-group">
        <label className="col-sm-3 control-label">Truck id</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.truckid}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Make/Model</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.make} {this.props.item.model} {this.props.item.year ? 'G.'+this.props.item.year : ''}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Vin</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.vin}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Tare</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.tare}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">License</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.license} {this.props.item.lic_state}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Truck type</label>
        <Col sm={9}>
          <p className="form-control-static">{this.getFieldSelectValue( 'truck_type', this.props.item.truck_type)}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Chain length</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.chained}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Ownership</label>
        <Col sm={9}>
          <span><p className="form-control-static">{this.props.item.ownership} {this.props.item.owned && this.props.item.owned.disp_name ? this.props.item.owned.disp_name : '' }</p></span>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Purchase</label>
        <Col sm={9}>
          <span><p className="form-control-static">{this.props.item.purchase_new ? 'condition: new' : ''} {this.getDateInFormat(this.props.item.purchase_date)} {this.props.item.purchase_price ? '$'+this.props.item.purchase_price : ''}</p></span>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Insurance</label>
        <Col sm={9}>
          <span><p className="form-control-static">{this.props.item.insurance ? 'Yes' : 'No'} {this.getDateInFormat(this.props.item.insurance_date)} {this.props.item.insurance_value ? '$'+this.props.item.insurance_value : ''}</p></span>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Note</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.note}</p>
        </Col>
      </div>
    </div>
    )
  }
}
