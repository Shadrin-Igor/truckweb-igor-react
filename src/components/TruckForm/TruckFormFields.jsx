const listField = [
  [
    {label:"Truck id", name: "truckid", size:4 },
    {label:"Make", name: "make", size:4 },
    {label:"Model", name: "model", size:4 },
  ],
  [
    {label:"Year", name: "year", size:4 },
    {label:"Vin", name: "vin", size:4 },
    {label:"Tare", name: "tare", size:4 },
  ],
  [
    {label:"License", name: "license", size:6 },
    {label:"License st.", name: "lic_state", type: "select", options:[], size:6 },
  ],
  [

//    {label:"Capacity", name: "capacity", size:4 },
    {label:"Truck type", name: "truck_type", type: "select", size:6, options:[
      {id:1, text:'Full-size van'},
      {id:2, text:'Truck'},
      {id:3, text:'Refrigerator'},
      {id:4, text:'Tanker'},
      {id:5, text:'Dry bulk'},
      {id:6, text:'Flatbed'},
      {id:7, text:'Car hauler'},
      {id:8, text:'Lowboy'}]
    },
    {label:"Chain length", name: "chained", type: "select", options:[{id:0, text:'None'},{id:1, text:'1'},{id:2, text:'2'},{id:3, text:'3'}], size:6},
  ],
  [
    {label:"Ownership", name: "ownership", type: "select", options:[{id:'company', text:'Company'},{id:'owned', text:'Owned'},{id:'rented', text:'Rented'}], size: 6 },
    {label:"Owned", name: "owned", type: "select", widget : 'SelectDriver', visible:{field:'ownership', value:'owned'}, size: 6 },
  ],
  [
    {label:"Purchase new", name: "purchase_new", type: "checkbox", size: 4 },
    {label:"Purchase date", name: "purchase_date", type: "date", visible:{field:'purchase_new', value:true}, size: 4 },
    {label:"Purchase price", name: "purchase_price", visible:{field:'purchase_new', value:true}, size: 4},
  ],
  [
    {label:"Insurance", name: "insurance", type: "checkbox", size:4 },
    {label:"Insurance's end", name: "insurance_date", type: "date", visible:{field:'insurance', value:true}, size:4 },
    {label:"Insurance price", name: "insurance_value", visible:{field:'insurance', value:true}, size:4 },
  ],
//  {label:"Company", name: "company", type: "select", ajaxUrl : "http://api.truckersys.tk:80/api/orgs/company/", ajaxNameField: "disp_name" },

  {label:"Note", name: "note", type: "textarea" },
];

export default listField
