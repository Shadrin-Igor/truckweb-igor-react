import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import _ from 'lodash';
import $ from 'jquery';
import SelectDriver from 'widgets/SelectDriver/SelectDriver';
import moment from 'moment';
import StateSelect from 'baseComponents/StateSelect/StateSelect'

import Field from 'baseComponents/Field/Field';
import DateField from 'baseComponents/DateField/DateField';

const fields = [
  'id',
  'truckid',
  'make',
  'model',
  'vin',
  'year',
  'tare',
  'license',
  'capacity',
  'ownership',
  'purchase_new',
  'purchase_date',
  'purchase_price',
  'chained',
  'insurance',
  'insurance_date',
  'insurance_value',
  'lic_state',
  'owned',
  'truck_type',
  'note',
  'insvc'
];

const validate = values => {
  const errors = {};
  const validation = {};
  if (_.isNil(values.truckid)) {
    errors['truckid'] = 'Required';
  }

  /**
   * numbers
   * @type {string[]}
     */
  validation.number = [ 'year', 'tare' ];
  if( validation.number && Array.isArray ( validation.number ) )
    validation.number.map( ( value ) => {
      if( values[value] && values[value].toString().search( /^.*([^0-9])+.*$/ ) > -1 )errors[ value ] = "Only numbers are allow here";
    });

  if( values.year && values.year.toString().search( /^([0-9]{4})$/g ) == -1 ){
    errors[ 'year' ] = "Invalid year value";
  }

  /**
   * numbers
   * @type {string[]}
   */
  validation.number = [ 'purchase_price', 'insurance_value' ];
  if( validation.number && Array.isArray ( validation.number ) )
    validation.number.map( ( value ) => {
      if( values[value] && values[value].toString().search( /^.*([^0-9\.])+.*$/ ) > -1 )errors[ value ] = "Only numbers are allow here";
    });

  /**
   * date format
   * @type {string[]}
   */
  validation.date = [ 'purchase_date', 'insurance_date' ];
  if( validation.date && Array.isArray ( validation.date ) )
    validation.date.map( ( value ) => {
      if( values[value] && values[value].toString().search( /^([0-9]{2}\/){3}$/ ) > -1 )
        errors[ value ] = "Invalid date value";
    });

  return errors;
};

class TruckForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    onFormSubmit: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
  }

  handleVinChange( event ) {
      event.target.value = event.target.value.toUpperCase();
      this.setState({vin: event.target.value});
      _.get(this.props.fields, event.target.name).onChange(event);
  }

  handleOwned( event ) {
    _.get(this.props.fields, 'owned').onChange(event);
  }

  handleDateChange(dateTime, field) {

    var fieldName = this.props.name;
    if (!moment(dateTime).isValid()) return;
    var formatted = moment.utc(dateTime).format("YYYY-MM-DD");
    this.props.fields[field].onChange(formatted);
  }

  handleDateChangeInsvc( date ){
    this.handleDateChange( date, 'insvc' )
  }

  handleDateChangePurchase_date( date ){
    this.handleDateChange( date, 'purchase_date' )
  }

  handleDateInsurance_date( date ){
    this.handleDateChange( date, 'insurance_date' )
  }

  handleFormSubmit(data) {
    if( !data.purchase_new ){
      data.purchase_date = null;
      data.purchase_price = null;
    }
    if( !data.insurance ){
      data.insurance_date = null;
      data.insurance_value = null;
    }
    if( data.owned ){
      data.owned = data.owned.id;
    }

    this.props.onFormSubmit(data);
  }

  render() {
    const {
      fields,
      handleSubmit,
      submitting,
      values
      } = this.props;

    return (
      <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <Col sm={7}>
          <div class="text-center"><b>TRUCK INFORMATION</b></div>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>Truck №</Col>
            <Col sm={9}>
              <Field errorMsg={fields.truckid.error ? fields.truckid.error : ''}>
                <input type='text' className="form-control" placeholder='Truck №' {...fields.truckid} />
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>Year</Col>
            <Col sm={9}>
              <Field errorMsg={fields.year.error ? fields.year.error : ''}>
                <input type='text' className="form-control" placeholder='Year' {...fields.year} />
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>Make</Col>
            <Col sm={9}>
              <Field errorMsg={fields.make.error ? fields.make.error : ''}>
                <input type='text' className="form-control" placeholder='Make' {...fields.make} />
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>Model</Col>
            <Col sm={9}>
              <Field errorMsg={fields.model.error ? fields.model.error : ''}>
                <input type='text' className="form-control" placeholder='Model' {...fields.model} />
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>Tare</Col>
            <Col sm={9}>
              <Field errorMsg={fields.tare.error ? fields.tare.error : ''}>
                <input type='text' className="form-control" placeholder='Tare' {...fields.tare} />
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>License</Col>
            <Col sm={4}>
              <Field errorMsg={fields.license.error ? fields.license.error : ''}>
                <input type='text' className="form-control" placeholder='License' {...fields.license} />
              </Field>
            </Col>
            <Col componentClass={ControlLabel} sm={1}>St</Col>
            <Col sm={4}>
              <Field errorMsg={fields.lic_state.error ? fields.lic_state.error : ''}>
                <StateSelect
                  ref='lic_state'
                  style={{width: '100%'}}
                  name='lic_state'
                  value={values.lic_state}
                  {...fields.lic_state}
                />
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>VIN</Col>
            <Col sm={9}>
              <Field errorMsg={fields.vin.error ? fields.vin.error : ''}>
                <input type='text' className="form-control" placeholder='Vin' {...fields.vin} onChange={::this.handleVinChange} />
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={3}>In Svc</Col>
            <Col sm={9}>
              <Field errorMsg={fields.insvc.error ? fields.insvc.error : ''}>
                <DateField value={values.insvc} name='insvc' onChange={::this.handleDateChangeInsvc}/>
              </Field>
            </Col>
          </FormGroup>
        </Col>
        <Col sm={5}>
          <div class="text-center"><b>TAX INFO</b></div>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={5}>New</Col>
            <Col sm={7}>
              <input type='checkbox' {...fields.purchase_new} value="1" checked={values.purchase_new ? "checked" : ""} />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={5}>Purchase Date</Col>
            <Col sm={7}>
              <Field errorMsg={fields.purchase_date.error ? fields.purchase_date.error : ''}>
                <DateField value={values.purchase_date} name='purchase_date' disabled={values.purchase_new ? false : true} onChange={::this.handleDateChangePurchase_date}/>
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={5}>Purchase Price</Col>
            <Col sm={7}>
              <Field errorMsg={fields.purchase_price.error ? fields.purchase_price.error : ''}>
                <div className="input-group">
                  <input type='text' className="form-control" disabled={values.purchase_new ? false : true} placeholder='Purchase price' {...fields.purchase_price} />
                  <span className="input-group-addon">$</span>
                </div>
              </Field>
            </Col>
          </FormGroup>
          <br />
          <div class="text-center"><b>PHYSICAL DAMAGE INSURANCE</b></div>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={5}>Insured</Col>
            <Col sm={7}>
              <input type='checkbox' name="insurance" value="1" checked={values.insurance ? "checked" : ""} {...fields.insurance} />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={5}>Insured Date</Col>
            <Col sm={7}>
              <Field errorMsg={fields.insurance_date.error ? fields.insurance_date.error : ''}>
                <DateField value={values.insurance_date} name='insurance_date' disabled={values.insurance ? false : true} onChange={::this.handleDateInsurance_date}/>
              </Field>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={5}>Insured Value</Col>
            <Col sm={7}>
              <Field errorMsg={fields.insurance_value.error ? fields.insurance_value.error : ''}>
                <div className="input-group">
                  <input type='text' className="form-control" placeholder='Insured Value' disabled={values.insurance ? false : true} {...fields.insurance_value} />
                  <span className="input-group-addon">$</span>
                </div>
              </Field>
            </Col>
          </FormGroup>
        </Col>
        <Col sm={12}>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>Ownership</Col>
            <Col sm={4}>
              <Field errorMsg={fields.ownership.error ? fields.ownership.error : ''}>
                <FormControl componentClass="select"
                    ref='ownership'
                    style={{width: '100%'}}
                    name='ownership'
                    value={values.ownership}
                    {...fields.ownership}>
                  <option value='company'>Company</option>
                  <option value='owned'>Owned</option>
                  <option value='rented'>Rented</option>
                </FormControl>
              </Field>
            </Col>
            <Col componentClass={ControlLabel} sm={2}>Owned</Col>
            <Col sm={4}>
              <SelectDriver
                     ref='owned'
                     style={{width: '100%'}}
                     name='owned'
                     disabled={values.ownership != 'owned' ? true : false}
                     value={fields.owned.value}
                     {...fields.owned}
                     onSelect={::this.handleOwned}
              />
            </Col>
          </FormGroup>
        </Col>
        <Col sm={12}>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>Truck type</Col>
            <Col sm={4}>
              <Field errorMsg={fields.truck_type.error ? fields.truck_type.error : ''}>
                <FormControl componentClass="select"
                             ref='truck_type'
                             style={{width: '100%'}}
                             name='truck_type'
                             value={values.truck_type}
                  {...fields.truck_type}>
                  <option value='1'>Full-size van</option>
                  <option value='2'>Truck</option>
                  <option value='3'>Refrigerator</option>
                  <option value='4'>Tanker</option>
                  <option value='5'>Dry bulk</option>
                  <option value='6'>Flatbed</option>
                  <option value='7'>Car hauler</option>
                  <option value='8'>Lowboy</option>
                </FormControl>
              </Field>
            </Col>
            <Col componentClass={ControlLabel} sm={2}>Chain length</Col>
            <Col sm={4}>
              <Field errorMsg={fields.chained.error ? fields.chained.error : ''}>
                <FormControl componentClass="select"
                             ref='chained'
                             style={{width: '100%'}}
                             name='chained'
                             value={values.chained}
                  {...fields.chained}>
                  <option value='0'>0</option>
                  <option value='1'>1</option>
                  <option value='2'>2</option>
                  <option value='3'>3</option>
                </FormControl>
              </Field>
            </Col>
          </FormGroup>
        </Col>
        <Col sm={12}>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>Note</Col>
            <Col sm={10}>
              <Field errorMsg={fields.note.error ? fields.note.error : ''}>
                <textarea className="form-control" placeholder='Note' {...fields.note} >{values.note}</textarea>
              </Field>
            </Col>
          </FormGroup>
        </Col>
        <FormGroup className="form-buttons">
          <Col sm={12}>
            <Button bsStyle="default" onClick={this.props.onCancel}>Cancel</Button>
            <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'TruckForm',
  fields: fields,
  validate,
})(TruckForm)
