import _ from 'lodash';
import React, { PropTypes } from 'react';
import { Image } from 'react-bootstrap';
import { reduxForm } from 'redux-form';
import classNames from 'classnames';

var Dropzone = require('react-dropzone');

import Field from 'baseComponents/Field/Field';
import AddrBaseFields from 'components/AddrForm/AddrBaseFields';

const fields = ['id', 'disp_name', 'email', 'name_legal', 'address',
  'company_address.addr_country', 'company_address.addr_state', 'company_address.addr_city', 'company_address.addr_street', 'company_address.addr_postalcode',
  'billing_address.addr_country', 'billing_address.addr_state', 'billing_address.addr_city', 'billing_address.addr_street', 'billing_address.addr_postalcode',
  'customer_facing_address.addr_country', 'customer_facing_address.addr_state', 'customer_facing_address.addr_city', 'customer_facing_address.addr_street', 'customer_facing_address.addr_postalcode',
  'legal_address.addr_country', 'legal_address.addr_state', 'legal_address.addr_city', 'legal_address.addr_street', 'legal_address.addr_postalcode',
  'dot', 'ein', 'mc', 'scac',
  'main_phone', 'note', 'site',
  'attributes.mobphone', 'attributes.fax', 'attributes.typerate', 'attributes.contact',
  'attributes.contact_email', 'attributes.routing', 'attributes.account', 'attributes.bank_name', ];

const validate = values => {
  const errors = {};
  errors.attributes = {};

  if (values.dot && !(_.toNumber(values.dot) >=0)) {
    errors.dot = 'Only numbers are allow here'
  }
  if (values.mc && !(_.toNumber(values.mc) >=0)) {
    errors.mc = 'Only numbers are allow here'
  }

  if (values.site && !values.site.match(/.+\..+/)) {
    errors.site = 'Enter a valid site url';
  }

  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  if (values.attributes.contact_email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.attributes.contact_email)) {
    errors.attributes.contact_email = 'Invalid email address'
  }

  return errors;
};

class CompanyForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onFormSubmit: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
    updateMyCompany: PropTypes.func,
    uploadCompanyLogo: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      showCustomerAddress: false,
      showLegalAddress: false,
      showBillingAddress: false,
    };
  }

  componentDidMount() {
    if (!_.isEmpty(this.props.fields.address.value)) {
      _.map(this.props.fields.address.value, (addr) => {
        if (addr.addr_type === 1) {
          this._initAddr('billing_address', addr);
          this.setState({ showBillingAddress: true });
        }
        else if (addr.addr_type === 3) {
          this._initAddr('legal_address', addr);
          this.setState({ showLegalAddress: true });
        }
        else if (addr.addr_type === 4) {
          this._initAddr('company_address', addr);
        }
        else if (addr.addr_type === 5) {
          this._initAddr('customer_facing_address', addr);
          this.setState({ showCustomerAddress: true });
        }
      })
    }
  }

  handleDrop(files) {
    var logo = _.head(files);
    if (!logo) return;

    console.log('File to upload: ', logo);

    this.props.uploadCompanyLogo({
      id: this.props.fields.id.value,
      logo,
    });
  }

  handleFormSubmit(data) {
    var preparedData = {
      ...data,
      billing_address: (this.state.showBillingAddress) ? data.billing_address : undefined,
      legal_address: (this.state.showLegalAddress) ? data.legal_address : undefined,
      customer_facing_address: (this.state.showCustomerAddress) ? data.customer_facing_address : undefined,
    };

    this.props.updateMyCompany(preparedData);
  }

  _initAddr(field, addr) {
    this.props.fields[field].addr_country.onChange(addr.addr_country);
    this.props.fields[field].addr_state.onChange(addr.addr_state);
    this.props.fields[field].addr_city.onChange(addr.addr_city);
    this.props.fields[field].addr_street.onChange(addr.addr_street);
    this.props.fields[field].addr_postalcode.onChange(addr.addr_postalcode);
  }

  render() {
    const {
      fields: { disp_name, email, name_legal,
        dot, ein, mc, scac,
        company_address, customer_facing_address, legal_address, billing_address,
        main_phone, note, site, attributes },
      handleSubmit,
    } = this.props;

    return (
      <Form horizontal
            onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <Row>
          <Col sm={3}>
            <h3>Company name</h3>
            <Image src="/assets/imgs/no-logo.png" thumbnail />

            <Dropzone onDrop={::this.handleDrop} className="company__dropzone" activeClassName="company__drugover">
              <div>Try dropping some files here, or click to select files to upload.</div>
            </Dropzone>
          </Col>
          <Col sm={9}>
            <Row>
              <Col sm={6}>
                <FormGroup>
                  <Col sm={12}>
                    <input type="text" className="form-control" {...name_legal}
                           placeholder="Contact legal name" />
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>DOT #</Col>
                  <Col sm={9}>
                    <Field errorMsg={dot.error}>
                      <input type="text" className="form-control" {...dot} />
                    </Field>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>MC #</Col>
                  <Col sm={9}>
                    <Field errorMsg={mc.error}>
                      <input type="text" className="form-control" {...mc} />
                    </Field>
                  </Col>
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>Employer ID(EIN)</Col>
                  <Col sm={9}>
                    <Field errorMsg={ein.error}>
                      <input type="text" className="form-control" {...ein} />
                    </Field>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>SCAC code</Col>
                  <Col sm={9}>
                    <Field errorMsg={scac.error}>
                      <input type="text" className="form-control" {...scac} />
                    </Field>
                  </Col>
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col sm={6}>
                <FormGroup>
                  <Col sm={12}>
                    <Field errorMsg={attributes.contact_email.error}>
                      <input type="text" className="form-control" placeholder="email Where QuickBooks can contact you" {...attributes.contact_email} />
                    </Field>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col sm={12}>
                    <input type="text" className="form-control" placeholder="Company phone" {...main_phone} />
                  </Col>
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <Col sm={12}>
                    <Field errorMsg={email.error}>
                      <input type="email" className="form-control" placeholder="email Customer-facing email" {...email} />
                    </Field>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col sm={12}>
                    <Field errorMsg={site.error}>
                      <input type="text" className="form-control" {...site}
                             placeholder="Website" />
                    </Field>
                  </Col>
                </FormGroup>
              </Col>
            </Row>

            <AddrBaseFields country={company_address.addr_country}
                            state={company_address.addr_state}
                            city={company_address.addr_city}
                            street={company_address.addr_street}
                            zip={company_address.addr_postalcode} />
            <Row>
              <Col sm={12}>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>Customer-facing address</Col>
                  <Col sm={9}>
                    <Checkbox checked={!this.state.showLegalAddress}
                              onClick={() => this.setState({showLegalAddress: !this.state.showLegalAddress})}>
                      Same as company address
                    </Checkbox>
                    {this.state.showLegalAddress &&
                      <AddrBaseFields country={customer_facing_address.addr_country}
                                      state={customer_facing_address.addr_state}
                                      city={customer_facing_address.addr_city}
                                      street={customer_facing_address.addr_street}
                                      postalcode={customer_facing_address.addr_postalcode} />
                    }
                    <HelpBlock>Address where customers contact you or send payments. Shown on sales forms.</HelpBlock>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>Legal address</Col>
                  <Col sm={9}>
                    <Checkbox checked={!this.state.showCustomerAddress}
                              onClick={() => this.setState({showCustomerAddress: !this.state.showCustomerAddress})}>
                      Same as company address
                    </Checkbox>
                    {this.state.showCustomerAddress &&
                      <AddrBaseFields country={legal_address.addr_country}
                                      state={legal_address.addr_state}
                                      city={legal_address.addr_city}
                                      street={legal_address.addr_street}
                                      postalcode={legal_address.addr_postalcode} />
                    }
                    <HelpBlock>Used for filing taxes.</HelpBlock>
                  </Col>
                </FormGroup>
              </Col>
            </Row>

          </Col>
        </Row>

        <Row>
          <Col sm={3}>
            <div className="pull-right">
              Payment Information
            </div>
          </Col>
          <Col sm={9}>
            <Row>
              <Col sm={6}>
                <FormGroup>
                   <Col componentClass={ControlLabel} sm={3}>Routing #</Col>
                  <Col sm={9}>
                    <input type="text" className="form-control" {...attributes.routing} />
                  </Col>
                </FormGroup>
              </Col>
              <Col sm={6}>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>Account #</Col>
                  <Col sm={9}>
                    <input type="text" className="form-control" {...attributes.account} />
                  </Col>
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col sm={12}>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>Bank Name #</Col>
                  <Col sm={9}>
                    <input type="text" className="form-control" {...attributes.bank_name} />
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3}>Billing address #</Col>
                  <Col sm={9}>
                    <Checkbox checked={!this.state.showBillingAddress}
                              onClick={() => this.setState({showBillingAddress: !this.state.showBillingAddress})}>
                      Same as company address
                    </Checkbox>
                    {this.state.showBillingAddress &&
                      <AddrBaseFields country={billing_address.addr_country}
                                      state={billing_address.addr_state}
                                      city={billing_address.addr_city}
                                      street={billing_address.addr_street}
                                      postalcode={billing_address.addr_postalcode} />
                    }
                  </Col>
                </FormGroup>
              </Col>
            </Row>
          </Col>
        </Row>

        <FormGroup className="form-buttons">
          <Col sm={12}>
            <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'CompanyForm',
  fields: fields,
  validate,
})(CompanyForm)
