import React, { PropTypes } from 'react';
import moment from 'moment';
import _ from 'lodash';

import { confirm } from 'baseComponents/dialog/dialog';
import appConfig from 'app/appConfig';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import InvoiceForm from 'components/InvoiceForm/InvoiceForm';

export default class extends React.Component {
  static contextTypes = {
    location: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
  };

  static propTypes = {
    invoiceList: PropTypes.object,
    myCompany: PropTypes.object,
    invoiceItem: PropTypes.object,
    createdInvoice: PropTypes.object,
    getMyCompany: PropTypes.func,
    getInvoice: PropTypes.func,
    createInvoice: PropTypes.func,
    updateInvoice: PropTypes.func,
    deleteInvoice: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.props.getMyCompany().then(() => {
      this.openCreatedFromUrl();
      this.props.getInvoiceList(this.props.myCompany.company);
    });

    this.state = {
      isItemLoading: false,
      isCreateFromLoad: false,
      createdInvoice: {},
    };


  }

  openCreatedFromUrl() {
    var invoiceId = _.toNumber(this.context.location.query.edit);

    if (_.isNaN(invoiceId)) return;

    this.setState({isCreateFromLoad: true});

    this.refs.addModal.open();

    this.setState({isItemLoading: true});

    this.props.getInvoice(this.props.myCompany.company, invoiceId).then(() => {
      this.setState({createdInvoice: this.props.invoiceItem.invoice});
      this.setState({isItemLoading: false});
    });
  }

  handleEditClick(item, event) {
    if (event) event.stopPropagation();
    this.refs['editModal' + item.id].open();
    this.setState({isItemLoading: true});
    this.props.getInvoice(this.props.myCompany.company, item.id).then(() => {
      this.setState({isItemLoading: false});
    });
  }

  handleAddInvoice() {
    this.setState({isItemLoading: true});
    this.refs.addModal.open();
    var date = moment().format();

    this.props.createInvoice(this.props.myCompany.company, {date}).then(() => {
      this.setState({createdInvoice: this.props.createdInvoice.invoice});
      this.setState({isItemLoading: false});
    });
  }

  handleEditSubmit(formState) {
    this.props.updateInvoice(this.props.myCompany.company, formState).then(() => {
      if (this.refs.addModal) this.refs.addModal.close();
      if (this.refs['editModal' + formState.id]) this.refs['editModal' + formState.id].close();
      if (this.state.isCreateFromLoad) {
        this.setState({isCreateFromLoad: false});
        this.context.router.push(this.context.location.pathname);
      }
    });
  }

  handleRemove(item, event) {
    event.stopPropagation();

    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to delete this invoice?',
      onOk: () => {
        item.removing = true;
        this.forceUpdate();
        this.props.deleteInvoice(this.props.myCompany.company, item);
      },
    });
  }

  handleCloseCreatedModal(invoice) {
    this.props.deleteInvoice(this.props.myCompany.company, invoice);
    this.refs.addModal.close();
  }

  handleCloseCreatedFromLoadModal(invoice) {
    this.props.deleteInvoice(this.props.myCompany.company, invoice);
    this.context.router.push(decodeURIComponent(this.context.location.query.back_url));
    this.refs.addModal.close();
  }

  invoiceModal(invoice, modalName, cancelHandler) {
    return (
      <BaseModal ref={modalName}
                 header={'Edit invoice: ' + ((this.state.isItemLoading) ? '...' : invoice.inv_num)}
                 options={{bsSize: 'extra-large'}}>
        {(this.state.isItemLoading) ?
          <div>...loading</div>
          :
          <InvoiceForm initialValues={invoice}
                       onFormSubmit={::this.handleEditSubmit}
                       onCancel={cancelHandler.bind(this, invoice)} />
        }
      </BaseModal>
    );
  }

  pdfUrl(item) {
    if (_.isEmpty(this.props.myCompany.company)) return;
    return `${appConfig.apiBaseUrl}/${appConfig.apiVersion}/orgs/company/${this.props.myCompany.company.id}/invoices/${item.id}/pdf/`;
  }

  render() {
    return (
      <div>
        <div>
          <div className="list-toolbar clearfix">
            <button className="btn btn-primary pull-right"
                    disabled={this.props.invoiceList.pending}
                    onClick={::this.handleAddInvoice}>Add invoice</button>
          </div>
        </div>

        {/* edit modal for added invoice */}
        {this.invoiceModal(this.state.createdInvoice,
          'addModal',
          (this.state.isCreateFromLoad)
            ? this.handleCloseCreatedFromLoadModal
            : this.handleCloseCreatedModal)}

        <table className="table table-bordered table-hover table-grid" cellSpacing="0" width="100%">
          <thead>
          <tr>
            <th>Date</th>
            <th>No.</th>
            <th>Load#</th>
            <th>Customer</th>
            <th>Memo</th>
            <th>Due Date</th>
            <th>Total</th>
            <th>Date Send</th>
            <th>LAST MODIFIED DATE</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          {_.map(this.props.invoiceList.invoices, (item) => {
            return (
              <tr key={item.id} className="selectable-item">
                <td onClick={this.handleEditClick.bind(this, item)}>{moment(item.date).isValid() && moment(item.date).format('L')}</td>
                <td onClick={this.handleEditClick.bind(this, item)}>{item.inv_num}</td>
                <td onClick={this.handleEditClick.bind(this, item)}>{item.reference}</td>
                <td onClick={this.handleEditClick.bind(this, item)}>{item.broker && item.broker.disp_name}</td>
                <td onClick={this.handleEditClick.bind(this, item)}>{item.memo}</td>
                <td onClick={this.handleEditClick.bind(this, item)}>{moment(item.due_date).isValid() && moment(item.due_date).format('L')}</td>
                <td onClick={this.handleEditClick.bind(this, item)}>{item.total}</td>
                <td onClick={this.handleEditClick.bind(this, item)}></td>
                <td onClick={this.handleEditClick.bind(this, item)}></td>
                <td className="invoice-list__actions">
                  <DropdownButton id={`action-dropdown-${item.id}`} bsStyle={'link'} title="Action">
                    <MenuItem eventKey="1" target="_blank" href={this.pdfUrl(item)}>Download</MenuItem>
                    <MenuItem eventKey="3" onClick={this.handleEditClick.bind(this, item)}>Edit</MenuItem>
                    <MenuItem eventKey="4" onClick={this.handleRemove.bind(this, item)}>Delete</MenuItem>
                  </DropdownButton>

                  {/* item edit modal */}
                  {this.invoiceModal(item, 'editModal' + item.id, () => this.refs['editModal' + item.id].close())}
                </td>
              </tr>
            );
          })}
          </tbody>
        </table>

      </div>
    );
  }
}

