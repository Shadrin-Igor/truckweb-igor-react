import React, { PropTypes } from 'react';
import _ from 'lodash';
import { reduxForm } from 'redux-form';

import SelectBroker from 'widgets/SelectBroker/SelectBroker';
import Field from 'baseComponents/Field/Field';

const fields = ['broker', 'reference', 'rate', 'carrier_pay', 'commodity', 'note'];

const validate = values => {
  const errors = {};
  if (values.carrier_pay && !(_.toNumber(values.carrier_pay) >=0)) {
    errors.carrier_pay = `Only numbers are allow here`;
  }
  if (values.rate && !(_.toNumber(values.rate) >=0)) {
    errors.rate = `Only numbers are allow here`;
  }
  return errors;
};

class FreightParamsForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onFormSubmit: PropTypes.func,
    allowEditBroker: PropTypes.bool,
  };

  static defaultProps = {
    allowEditBroker: true,
  };

  constructor(props) {
    super(props);
  }

  handleFormSubmit(data) {
    this.props.onFormSubmit(data);
  }

  handleBrokerSelect(broker) {
    this.props.fields.broker.onChange(broker);
  }

  render() {
    const {
      fields: { broker, reference, rate, carrier_pay, commodity, note },
      handleSubmit,
    } = this.props;

    return (
      <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>

        {this.props.allowEditBroker &&
          <FormGroup>
            <Col sm={12}>
              <SelectBroker
                placeholder="Select broker"
                value={broker.value}
                clearable={false}
                onSelect={::this.handleBrokerSelect} />
            </Col>
          </FormGroup>
        }
        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Reference #</Col>
          <Col sm={9}>
            <input type='text' className="form-control" {...reference} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Rate #</Col>
          <Col sm={9}>
            <Field errorMsg={rate.error}>
              <input type='text' className="form-control" {...rate} />
            </Field>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Carrier pay</Col>
          <Col sm={9}>
            <Field errorMsg={carrier_pay.error}>
              <input type='text' className="form-control" {...carrier_pay} />
            </Field>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Commodity</Col>
          <Col sm={9}>
            <input type='text' className="form-control" {...commodity} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Note</Col>
          <Col sm={9}>
            <textarea className="form-control" {...note} />
          </Col>
        </FormGroup>

        <FormGroup className="form-buttons">
          <Col sm={12} className="form-footer">
            <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
            <Button bsStyle="default" className="pull-right"
                    onClick={this.props.onCancel}>Cancel</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'FreightParamsForm',
  fields: fields,
  validate,
})(FreightParamsForm)
