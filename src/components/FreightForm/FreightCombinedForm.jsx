import React, { PropTypes } from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import SelectBroker from 'widgets/SelectBroker/SelectBroker';
import RpointForm from 'components/RpointForm/RpointForm';
import FreightParamsDetails from 'components/FreightForm/FreightParamsDetails';
import FreightParamsForm from 'components/FreightForm/FreightParamsForm';
import FreightAddrSelect from 'widgets/FreightAddrSelect/FreightAddrSelect';
import RpointDetails from 'components/RpointForm/RpointDetails';
import AddrForm from 'components/AddrForm/AddrForm';
import BrokerForm from 'components/BrokerForm/BrokerForm';

export default class FreightCombinedForm extends React.Component {
  static propTypes = {
    needUpdate: PropTypes.func,
    createCompany: PropTypes.func,
    createOrderAddress: PropTypes.func,
    updateOrderAddress: PropTypes.func,
    getBrokerList: PropTypes.func,
    freightItem: PropTypes.object,
    updateFreight: PropTypes.func,
    deleteFreight: PropTypes.func,
    createRpoint: PropTypes.func,
    updateRpoint: PropTypes.func,
    deleteRpoint: PropTypes.func,
    allowEditBrokerInPlace: PropTypes.bool,
    allowAddAddress: PropTypes.bool,
  };

  static defaultProps = {
    allowEditBrokerInPlace: true,
    allowAddAddress: true,
  };

  constructor(props) {
    super(props);

    this.state = {
      paramsForm: _.omit(this.props.freightItem.freight, 'origins', 'destinations'),
      originNew: {
        type: 'origin',
        order: this.props.freightItem.freight.id,
      },
      destNew: {
        type: 'dest',
        order: this.props.freightItem.freight.id,
      },
      // originList: (_.isEmpty(this.props.freightItem.freight.origins)) ? [this._newRpoint('origin')] : this.props.freightItem.freight.origins,
      // destList: (_.isEmpty(this.props.freightItem.freight.destinations)) ? [this._newRpoint('dest')] : this.props.freightItem.freight.destinations,
      originList: [],
      destList: [],
    };
  }

  componentDidMount() {
    this._updateLists();
    this._updateParamsForm();
  }

  handleRpointClick(rpoint) {
    if (rpoint.isNew) {
      this.refs['createRpointModal' + rpoint.type].open();
    }
    else {
      this.refs['editRpointModal' + rpoint.id].open();
    }
  }

  saveRpoint(rpoint, formState) {
    if (rpoint.isNew) {
      this.props.createRpoint(formState).then(() => {
        this.refs['createRpointModal' + rpoint.type].close();
        rpoint.toRemove = true;
        this.props.needUpdate().then(() => {
          this._updateLists();
        })
      });
    }
    else {
      this.props.updateRpoint(formState).then(() => {
        this.refs['editRpointModal' + rpoint.id].close();
        this.props.needUpdate().then(() => {
          this._updateLists();
        })
      });
    }
  }

  handleRpointRemove(rpoint) {
    var listName = rpoint.type + 'List';
    var list = [].concat(this.state[listName]);

    if (rpoint.isNew) {
      _.remove(list, rpoint);
      this.setState({[listName]: list});
    }
    else {
      confirm({
        okBtn: 'Delete',
        message: `Operation can't be undone. Do you really want to delete this route point?`,
        onOk: () => {
          this.props.deleteRpoint(rpoint).then(() => {
            this.props.needUpdate().then(() => {
              this._updateLists();
            })
          });
        },
      });
    }
  }

  handleRpointAddrSelect(rpoint, selected) {
    if (rpoint.isNew) {
      rpoint.point = selected;
      this.refs['createRpointModal' + rpoint.type].open();
    }
    else {
      this.props.updateRpoint({
        id: rpoint.id,
        order: rpoint.order,
        point: selected,
      });
    }
  }

  handleRpointCreateMoldalCancel(rpoint) {
    this._updateLists();

    this.refs['createRpointModal' + rpoint.type].close();
  }

  handleParamsModalSubmit(newState) {
    var sendData = {
      ...this.state.paramsForm,
      ...newState,
    };
    this.props.updateFreight(sendData).then(() => {
      this.refs['editFreighParams'].close();
      this.props.needUpdate().then(() => {
        this._updateParamsForm();
      })
    });
  }

  handleParamsModalCancel() {
    this._updateParamsForm();
    this.refs['editFreighParams'].close();
  }

  handleBrokerSelect(broker) {
    if (!broker) return;

    this.setState({paramsForm: {
      ...this.state.paramsForm,
      broker,
    }});
    this.refs['editFreighParams'].open();
    return false;
  }

  handleParamsClick() {
    this.refs['editFreighParams'].open();
  }

  handleCreateBrokerSubmit(formState) {
    var newState = {
      ...formState,
      kind: 'broker',
    };
    this.props.createCompany(newState).then(() => {
      this.props.getBrokerList().then(() => {
        this.refs.createBrokerModal.close();
      });
    });
  }

  handleRpointSearchSelect(shortType, selectValue) {
    var listName = shortType + 'List';
    // get copy of a list
    var list = [].concat(this.state[listName]);
    var newRpoint = this._newRpoint({
      type: shortType,
      point: selectValue,
    });

    // list.unshift(newRpoint);
    // this.setState({[listName]: list});

    this.saveRpoint(newRpoint, newRpoint);

    return false;
  }

  handleAddrEditSumbit() {
    this.refs.updateAddrModal.close();
    this.props.needUpdate().then(() => {
      this._updateLists();
    });
  }

  handleBrokerClear() {
    confirm({
      okBtn: 'Delete',
      message: `Operation can't be undone. Do you really want to clear brocker's information?`,
      onOk: () => {
        var sendData = {
          id: this.props.freightItem.freight.id,
          broker: null,
          carrier_pay: 0,
          commodity: '',
          note: '',
          rate: 0,
        };
        this.props.updateFreight(sendData).then(() => {
          this.props.needUpdate().then(() => {
            this._updateParamsForm();
          })
        });
      },
    });
  }

  _updateParamsForm() {
    this.setState({paramsForm: _.omit(this.props.freightItem.freight, 'origins', 'destinations')});
  }

  _fullTypeName(shortType) {
    return (shortType == 'origin') ? 'origin' : 'destination'
  }

  _fullListName(shortType) {
    return (shortType == 'origin') ? 'origins' : 'destinations'
  }

  _hasNew(list) {
    return list.length && _.head(list).isNew && !_.head(list).toRemove;
  }

  _updateLists() {
    var combineList = (shortType) => {
      var listName = shortType + 'List';
      var fullListName = this._fullListName(shortType);

      var newList = _.clone(this.props.freightItem.freight[fullListName]);

      //if (this._hasNew(this.state[listName])) newList.unshift(this._newRpoint(shortType));
      if (_.isEmpty(newList)) newList.unshift(this._newRpoint({type: shortType}));

      return newList;
    };

    this.setState({
      originList: combineList('origin'),
      destList: combineList('dest'),
    });
  }

  _newRpoint(predefined) {
    return {
      date: null,
      date_end: null,
      note: '',
      order: this.props.freightItem.freight.id,
      point: {},
      //type: shortType,
      isNew: true,
      ...predefined,
    };
  }

  renderRpoint(rpoint) {
    var fullType = this._fullTypeName(rpoint.type);



    return (
      <div>
        <div className="freight__column__tools">
          <div className="freight__column__tools_inner">
            <button type="button" className="btn btn-default btn-xs"
                    onClick={this.handleRpointRemove.bind(this, rpoint)}>
              <span className="glyphicon glyphicon-trash" />
            </button>
            <button type="button" className="btn btn-default btn-xs"
                    onClick={() => this.refs.updateAddrModal.open()}>
              <span className="glyphicon glyphicon-pencil" />
            </button>
          </div>
        </div>

        <BaseModal ref={'updateAddrModal'}
                   header={'Add address'}
                   options={{bsSize: 'lg'}}>
          <AddrForm updateOrderAddress={this.props.updateOrderAddress}
                    initialValues={rpoint.point}
                    onCancel={() => this.refs.updateAddrModal.close()}
                    onFormSubmit={::this.handleAddrEditSumbit} />
        </BaseModal>

        <div className="details">
          <FormGroup className="row" onClick={this.handleRpointClick.bind(this, rpoint)}>
            <Col sm={3} className="freight-big-label"></Col>
            <Col sm={9} className="freight-big-value freight__rpoint-title">
              <div className="form-static">{rpoint.point && rpoint.point.title}</div>
            </Col>
          </FormGroup>
        </div>

        <RpointDetails rpoint={rpoint}
                       onClick={this.handleRpointClick.bind(this, rpoint)} />

        <BaseModal ref={'createRpointModal' + rpoint.type}
                   header={'Create ' + fullType}
                   options={{bsSize: 'medium'}}>
          <RpointForm
            initialValues={rpoint}
            formKey={'create-' + rpoint.type}
            createOrderAddress={this.props.createOrderAddress}
            updateOrderAddress={this.props.updateOrderAddress}
            onCancel={this.handleRpointCreateMoldalCancel.bind(this, rpoint)}
            onFormSubmit={this.saveRpoint.bind(this, rpoint)} />
        </BaseModal>

        <BaseModal ref={'editRpointModal' + rpoint.id}
                   header={'Edit ' + fullType}
                   options={{bsSize: 'medium'}}>
          <RpointForm
            initialValues={rpoint}
            formKey={'edit-' + fullType + rpoint.id}
            createOrderAddress={this.props.createOrderAddress}
            updateOrderAddress={this.props.updateOrderAddress}
            onCancel={() => this.refs['editRpointModal' + rpoint.id].close()}
            onFormSubmit={this.saveRpoint.bind(this, rpoint)} />
        </BaseModal>
      </div>
    );
  }

  createAddrBtn() {
    return (
      <div>
        <button className="btn btn-primary"
                onClick={() => this.refs.createAddrModal.open()}>+</button>
        <BaseModal ref={'createAddrModal'}
                   header={'Add address'}
                   options={{bsSize: 'lg'}}>
          <AddrForm createOrderAddress={this.props.createOrderAddress}
                    updateOrderAddress={this.props.updateOrderAddress}
                    onCancel={() => this.refs.createAddrModal.close()}
                    onFormSubmit={() => this.refs.createAddrModal.close()} />
        </BaseModal>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Row>
          <Col sm={4}>
            <Row>
              <Col sm={3}>
                <h3 className="freight__col_header"><span>Brokers</span></h3>
              </Col>
              <Col sm={9}>
                <FormGroup className="row">
                  <div className="typeahead-group">
                    <div className="typeahead-group__col">
                      <SelectBroker
                        placeholder="Select broker"
                        onSelect={::this.handleBrokerSelect} />
                    </div>
                    <div className="typeahead-group__col">
                      <button className="btn btn-primary"
                              onClick={() => this.refs.createBrokerModal.open()}>+</button>
                      <BaseModal ref={'createBrokerModal'}
                                 header={'Add broker'}
                                 options={{bsSize: 'extra-large'}}>
                        <BrokerForm onCancel={() => this.refs.createBrokerModal.close()}
                                    onFormSubmit={::this.handleCreateBrokerSubmit} />
                      </BaseModal>
                    </div>
                  </div>
                </FormGroup>
              </Col>
            </Row>

            <div className="freight__block">
              <div className="freight__column__tools">
                <div className="freight__column__tools_inner">
                  <button type="button" className="btn btn-default btn-xs"
                          onClick={::this.handleBrokerClear}>
                    <span className="glyphicon glyphicon-trash" />
                  </button>
                  <button type="button" className="btn btn-default btn-xs"
                          onClick={::this.handleParamsClick}>
                    <span className="glyphicon glyphicon-pencil" />
                  </button>
                </div>
              </div>

              <div className="details">
                <FormGroup className="row" onClick={::this.handleParamsClick}>
                  <Col sm={3} className="freight-big-label"></Col>
                  <Col sm={9} className="freight-big-value freight__rpoint-title">
                    <div className="form-static">{this.state.paramsForm.broker && this.state.paramsForm.broker.disp_name}</div>
                  </Col>
                </FormGroup>
              </div>

              <FreightParamsDetails freight={this.state.paramsForm}
                                    onClick={::this.handleParamsClick} />
            </div>

            <BaseModal ref={'editFreighParams'}
                       header={'Load params '}
                       options={{bsSize: 'sm'}}>
              <FreightParamsForm
                initialValues={this.state.paramsForm}
                formKey={'editFreighParams'}
                allowEditBroker={this.props.allowEditBrokerInPlace}
                onCancel={::this.handleParamsModalCancel}
                onFormSubmit={::this.handleParamsModalSubmit} />
            </BaseModal>
          </Col>

          <Col sm={4}>
            <Row>
              <Col sm={3}>
                <h3 className="freight__col_header">
                  <span>Origin</span>
                </h3>
              </Col>
              <Col sm={9}>
                <FormGroup className="row">
                  <div className="typeahead-group">
                    <div className="typeahead-group__col">
                      <FreightAddrSelect placeholder="Select or search Origin / address"
                                         onSelect={this.handleRpointSearchSelect.bind(this, 'origin')} />
                    </div>
                    {this.props.allowAddAddress &&
                      <div className="typeahead-group__col">
                        {this.createAddrBtn()}
                      </div>
                    }
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <ul className="list-unstyled freight__rpoint-col">
              {_.map(this.state.originList, (rpoint) => {
                return (
                  <li key={`${rpoint.type}-${rpoint.id}`} className={classNames('freight__block', {'freight__rpoint_item-prestine': rpoint.isNew})}>
                    {this.renderRpoint(rpoint)}
                  </li>
                );
              })}
            </ul>
          </Col>

          <Col sm={4}>
            <Row>
              <Col sm={3}>
                <h3 className="freight__col_header">
                  <span>Dest</span>
                </h3>
              </Col>
              <Col sm={9}>
                <FormGroup className="row">
                  <div className="typeahead-group">
                    <div className="typeahead-group__col">
                      <FreightAddrSelect placeholder="Select or search Destination"
                                         onSelect={this.handleRpointSearchSelect.bind(this, 'dest')} />
                    </div>
                    {this.props.allowAddAddress &&
                      <div className="typeahead-group__col">
                        {this.createAddrBtn()}
                      </div>
                    }
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <ul className="list-unstyled freight__rpoint-col">
              {_.map(this.state.destList, (rpoint) => {
                return (
                  <li key={`${rpoint.type}-${rpoint.id}`} className="freight__block">
                    {this.renderRpoint(rpoint)}
                  </li>
                );
              })}
            </ul>
          </Col>
        </Row>
      </div>
    );
  }
}
