import React, { PropTypes } from 'react';
import _ from 'lodash';

export default class Totals extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <div className="freight-totals">

        <Row>
          <Col sm={6}>
            Miles to the end:
          </Col>
          <Col sm={6}>
            0000
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            Time to the end:
          </Col>
          <Col sm={6}>
            0000
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            Total Pallets: 0000
          </Col>
          <Col sm={6}>
            Total weight: 0000
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            Total Miles: 0000
          </Col>
          <Col sm={6}>
            Total time: 0000
          </Col>
        </Row>
        <div>
          <Row>
            <Col sm={6}>
              Rate per Miles: 000
            </Col>
            <Col sm={6}>
              <div className="btn-group btn-group-xs" role="group">
                <button type="button" className="btn btn-default">FLT</button>
                <button type="button" className="btn btn-default">LTL</button>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
