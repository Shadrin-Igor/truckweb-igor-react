import React, { PropTypes } from 'react';

export default class FreightParamsDetails extends React.Component {
  static propTypes = {
    freight: PropTypes.object,
    onClick: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="form-horizontal details" onClick={this.props.onClick}>
        {this.props.freight.trip &&
          <div className="form-group">
            <label className="col-sm-3 control-label">Trip#</label>
            <Col sm={9}>
              <p className="form-static freight__details_tripid">{this.props.freight.trip}</p>
            </Col>
          </div>
        }
        <div className="form-group">
          <label className="col-sm-3 control-label">Reference#</label>
          <Col sm={9}>
            <p className="form-static">{this.props.freight.reference}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Rate#</label>
          <Col sm={9}>
            <p className="form-static">{this.props.freight.rate}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Carrier pay</label>
          <Col sm={9}>
            <p className="form-static">{this.props.freight.carrier_pay}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Commodity</label>
          <Col sm={9}>
            <p className="form-static">{this.props.freight.commodity}</p>
          </Col>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">Note</label>
          <Col sm={9}>
            <p className="form-static">{this.props.freight.note}</p>
          </Col>
        </div>
       </div>
    );
  }
}
