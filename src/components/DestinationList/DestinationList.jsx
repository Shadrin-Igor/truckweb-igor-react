import React, { PropTypes } from 'react';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import AddrForm from 'components/AddrForm/AddrForm';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import Pagination from 'baseComponents/Pagination/Pagination';

export default class extends React.Component {
  static propTypes = {
    list: PropTypes.array,
    totalCount: PropTypes.number,
    myCompany: PropTypes.object,
    onSelect: PropTypes.func,
    getMyCompany: PropTypes.func,
    getDestinationList: PropTypes.func,
    createOrderAddress: PropTypes.func,
    updateOrderAddress: PropTypes.func,
    deleteDestination: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.props.getDestinationList();

    this.state = {
      isItemLoading: false,
    };
  }

  handleCreateSubmit(formState) {
      this.refs.addModal.close();
  }

  handleEditSubmit(item, formState) {
    this.props.updateOrderAddress(formState).then(() => {
      this.props.getDestinationList().then(() => {
        this.refs['editModal' + item.id].close();
      });
    });
  }

  handleRemove(item, event) {
    event.stopPropagation();

    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to delete this destination?',
      onOk: () => {
        item.removing = true;
        this.forceUpdate();
        this.props.deleteDestination(item);
      },
    });
  }

  render() {
    var options = {
      sizePerPage: 15,
      noDataText : '...'
    }

    return (
      <div>
        <div>
          <div className="list-toolbar clearfix">
            <button className="btn btn-primary pull-right" onClick={() => this.refs.addModal.open()}>New address</button>
          </div>
          <BaseModal ref="addModal" header="Add address" options={{bsSize: 'large'}}>
            <AddrForm onFormSubmit={::this.handleCreateSubmit}
                       onCancel={() => this.refs.addModal.close()}
                       createOrderAddress={this.props.createOrderAddress} />
          </BaseModal>
        </div>
        <BootstrapTable tableHeaderClass="table-bordered table-hover table-grid margin-no"
                        data={this.props.list}
                        pagination={false}
                        exportCSV={false}
                        hover={true}
                        paginationShowsTotal={true}
                        ignoreSinglePage={true}
                        options={options}>
          <TableHeaderColumn width='60px' isKey={true} dataField="id" dataFormat={ ( cell, row ) => {
              return <div onClick={()=>this.props.onSelect(row)}>{row.id}</div>
          }}>ID</TableHeaderColumn>
          <TableHeaderColumn width='200px' dataSort={true} dataField="title">TITLE</TableHeaderColumn>
          <TableHeaderColumn width='80px' dataField="address">Address</TableHeaderColumn>
          <TableHeaderColumn width='200px' dataField="phone">Phone</TableHeaderColumn>
          <TableHeaderColumn dataField="contract" >Contract</TableHeaderColumn>
          <TableHeaderColumn width='60px' dataField="note"  dataFormat={( cell, row )=>{
              return (
                    <DropdownButton id={`action-dropdown-${row.id}`} bsStyle={'link'} title="Select Action">
                      <MenuItem eventKey="1" onClick={this.handleRemove.bind(this, row)}>Delete</MenuItem>
                      <MenuItem eventKey="2" onClick={this.props.onSelect.bind(this, row)}>Print or preview</MenuItem>
                      <MenuItem eventKey="3" onClick={this.props.onSelect.bind(this, row)}>Transaction journal</MenuItem>
                      <MenuItem eventKey="4" onClick={this.props.onSelect.bind(this, row)}>Download</MenuItem>
                      <MenuItem eventKey="5" onClick={this.props.onSelect.bind(this, row)}>Send</MenuItem>
                    </DropdownButton>
                )
          }}>Action</TableHeaderColumn>
        </BootstrapTable>
        <Pagination totalCount={this.props.totalCount} countPerPage={15} activePage={1} getList={this.props.getDestinationList} />
      </div>
    );
  }
}

