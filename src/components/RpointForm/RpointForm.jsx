import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';

import BaseModal from 'baseComponents/BaseModal/BaseModal';
import FreightAddrSelect from 'widgets/FreightAddrSelect/FreightAddrSelect';
import TimeField from 'baseComponents/TimeField/TimeField';
import DateField from 'baseComponents/DateField/DateField';
import helpers from 'app/helpers';
import Field from 'baseComponents/Field/Field';

import AddrForm from 'components/AddrForm/AddrForm';

const fields = ['id', 'point', 'date', 'date_end', 'note', 'order', 'type',
  'PU', 'commodity', 'pits', 'weight', 'pieces'];

const validate = values => {
  const errors = {};
  if (moment(values.date_end).diff(values.date, 'minutes') < 0) {
    errors.date_end = `End date can't be lower than start date`;
  }
  return errors;
};

class RpointForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    createOrderAddress: PropTypes.func,
    updateOrderAddress: PropTypes.func,
    onCancel: PropTypes.func,
    onFormSubmit: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      startTime: (this.props.fields.date.value) ? helpers.getLocalTime(moment(this.props.fields.date.value).format()) : '',
      endTime: (this.props.fields.date_end.value) ? helpers.getLocalTime(moment(this.props.fields.date_end.value).format()) : '',
    };
  }

  handleAddrSelect(newPoint) {
    this.props.fields.point.onChange(newPoint);
  }

  handleDateChange(dateTime) {
    if (!moment(dateTime).isValid()) return;

    //it converts from 03/11/16 to 2016-03-11T12:00:00Z
    var formatted = moment.utc(dateTime).format();
    this.props.fields.date.onChange(formatted);

    if (!this.state.startTime) {
      this.setState({startTime: helpers.getLocalTime(formatted)});
    }
  }

  handleStartTimeChange(time) {
    var localTimeWithDate = moment(this.props.fields.date.value).format('L') + ' ' + time;
    localTimeWithDate = helpers.localToUtcDate(localTimeWithDate);
    this.props.fields.date.onChange(localTimeWithDate);
  }

  handleEndTimeChange(time) {
    var localTimeWithDate = moment(this.props.fields.date.value).format('L') + ' ' + time;
    localTimeWithDate = helpers.localToUtcDate(localTimeWithDate);
    this.props.fields.date_end.onChange(localTimeWithDate);
  }

  handleFormSubmit(data) {
    this.props.onFormSubmit(data);
  }

  isRequiredFieldFilled() {
    return !this.props.fields.point.value;
  }

  render() {
    const {
      fields: { point, date, date_end, PU, commodity, pits, weight, pieces, note },
      handleSubmit,
    } = this.props;

    return (
      <Form horizontal className="rpoint-form"
            onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <div className="typeahead-group">
          <div className="typeahead-group__col">
            <FreightAddrSelect placeholder="Company name"
                               value={point.value}
                               labelKey="title"
                               clearable={false}
                               onSelect={::this.handleAddrSelect} />
          </div>
          <div className="typeahead-group__col">
            {(_.isEmpty(point.value)) ?
              <a className="btn btn-primary rpoint__add-addr"
                 onClick={() => this.refs.createAddrModal.open()}>+</a>
              :
              <a className="btn btn-primary rpoint__add-addr"
                 onClick={() => this.refs.updateAddrModal.open()}><i className="glyphicon glyphicon-pencil" /></a>
            }

            <BaseModal ref={'createAddrModal'}
                       header={'Add new Origin / Destination'}
                       options={{bsSize: 'lg'}}>
              <AddrForm createOrderAddress={this.props.createOrderAddress}
                        onCancel={() => this.refs.createAddrModal.close()}
                        onFormSubmit={() => this.refs.createAddrModal.close()} />
            </BaseModal>

            <BaseModal ref={'updateAddrModal'}
                       header={'Update address'}
                       options={{bsSize: 'lg'}}>
              <AddrForm updateOrderAddress={this.props.updateOrderAddress}
                        initialValues={point.value}
                        onCancel={() => this.refs.updateAddrModal.close()}
                        onFormSubmit={() => this.refs.updateAddrModal.close()} />
            </BaseModal>
          </div>
        </div>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Tel #</Col>
          <Col sm={9}>
            <p className="form-control-static">{point.value && _.compact([point.value.phone, point.value.mphone]).join(', ')}</p>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Address #</Col>
          <Col sm={9}>
            <p className="form-control-static">{point.value && point.value.addr_full}</p>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Date/Time</Col>
          <Col sm={9}>
            <table className="rpoint__date-row">
              <tr>
                <td className="rpoint__date">
                  <DateField className="rpoint-form__date"
                             value={date.value}
                             disabled={this.isRequiredFieldFilled()}
                             onChange={::this.handleDateChange} />
                </td>
                <td className="rpoint__time">
                  <Field errorMsg={date.error}>
                    <TimeField className="rpoint-form__start"
                               disabled={!date.value}
                               value={this.state.startTime}
                               onChange={::this.handleStartTimeChange} />
                  </Field>
                </td>
                <td className="rpoint__to">
                  <span>to</span>
                </td>
                <td className="rpoint__time">
                  <Field errorMsg={date_end.error}>
                    <TimeField className="rpoint-form__end"
                               value={this.state.endTime}
                               disabled={!date.value}
                               onChange={::this.handleEndTimeChange} />
                  </Field>
                </td>
              </tr>
            </table>
          </Col>
        </FormGroup>



        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>PU #</Col>
          <Col sm={9}>
            <input type='text' className="form-control" {...PU}
                      disabled={this.isRequiredFieldFilled()} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Commodity</Col>
          <Col sm={9}>
            <input type='text' className="form-control" {...commodity}
                   disabled={this.isRequiredFieldFilled()} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Note</Col>
          <Col sm={9}>
            <textarea type='text' className="form-control" {...note}
                      disabled={this.isRequiredFieldFilled()} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Pits:</Col>
          <Col sm={9}>
            <table className="rpoint__item-row">
              <tr>
                <td>
                  <input type='text' className="form-control" {...pits}
                         disabled={this.isRequiredFieldFilled()} />
                </td>
                <td className="rpoint__weight-label">
                  <label className="control-label">Wght:</label>
                </td>
                <td>
                  <input type='text' className="form-control" {...weight}
                         disabled={this.isRequiredFieldFilled()} />
                </td>
                <td className="rpoint__pieces-label">
                  <label className="control-label">Pcs:</label>
                </td>
                <td>
                  <input type='text' className="form-control" {...pieces}
                         disabled={this.isRequiredFieldFilled()} />
                </td>
              </tr>
            </table>
          </Col>
        </FormGroup>

        <FormGroup className="form-buttons">
          <Col sm={12}>
            <Button bsStyle="default" onClick={this.props.onCancel}>Cancel</Button>
            <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'RpointForm',
  fields: fields,
  validate,
})(RpointForm)
