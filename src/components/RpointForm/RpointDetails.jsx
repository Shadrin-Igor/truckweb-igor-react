import React, { PropTypes } from 'react';
import moment from 'moment';

import DateDurationFormatter from 'baseComponents/formatters/DateDurationFormatter';

export default class RpointDetails extends React.Component {
  static propTypes = {
    rpoint: PropTypes.object,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="form-horizontal details" onClick={this.props.onClick}>
        <FormGroup className="rpoint-form__address">
          <Col componentClass={ControlLabel} sm={3}>Address</Col>
          <Col sm={9}>
            <p className="form-static">{this.props.rpoint.point && this.props.rpoint.point.addr_full}</p>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Date / Time</Col>
          <Col sm={9}>
            <p className="form-static mark-color">
              <DateDurationFormatter dateStart={this.props.rpoint.date} dateEnd={this.props.rpoint.date_end} />
            </p>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Tel #</Col>
          <Col sm={9}>
            <p className="form-static">{_.compact([this.props.rpoint.point.phone, this.props.rpoint.point.mphone]).join(', ')}</p>
          </Col>
        </FormGroup>

        <FormGroup className="rpoint-form__address">
          <Col componentClass={ControlLabel} sm={3}>PU #</Col>
          <Col sm={9}>
            <p className="form-static">{this.props.rpoint.PU}</p>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col componentClass={ControlLabel} sm={3}>Note</Col>
          <Col sm={9}>
            <p className="form-static">{this.props.rpoint.note}</p>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
