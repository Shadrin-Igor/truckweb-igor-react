import React from 'react';
import { connect } from 'react-redux'

@connect(
  state => ({
    fetch: state.fetch})
)
export default class FetchPreloader extends React.Component {
  render() {
    var visible = (this.props.fetch.saveInProgress.length > 0) ? 'visible' : 'hidden';
    return (
      <div className="fetch-preloader">
        <span className="rubix-icon icon-feather-loader" style={{visibility: visible}}></span>
      </div>
    );
  }
}
