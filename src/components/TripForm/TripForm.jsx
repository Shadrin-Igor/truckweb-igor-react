import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form'
import _ from 'lodash';

import BaseModal from 'baseComponents/BaseModal/BaseModal';
import SelectDriver from 'widgets/SelectDriver/SelectDriver';
import SelectBroker from 'widgets/SelectBroker/SelectBroker';
import SelectTruck from 'widgets/SelectTruck/SelectTruck';
import DriverForm from 'components/DriverForm/DriverForm';
//import { searchDispatcher } from 'redux/actions/searchDispatcher';

const fields = ['id', 'dispatcher', 'driver', 'driver2', 'carrier', 'truck'];

class TripForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    tripItem: PropTypes.object,
    fields: PropTypes.object.isRequired,
    values: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    updateTrip: PropTypes.func,
    createDriver: PropTypes.func,
    onPlusLoadClick: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      showDriver2: false,
      showCarrier: !!this.props.fields.carrier.value,
    };
  }

  //handleDispatcherSelect(dispatcher) {
  //  this.props.fields.dispatcher.onChange(dispatcher);
  //
  //  // update
  //  this.props.updateTrip({
  //    id: this.props.values.id,
  //    dispatcher: dispatcher,
  //  });
  //}

  handleDriverSelect(driver) {
    this.props.updateTrip({
      id: this.props.values.id,
      driver: driver,
      carrier: null,
      // select driver's truck
      truck: driver && driver.truck,
    });

    this.props.fields.driver.onChange(driver);
    this.props.fields.carrier.onChange(null);
    if (!this.props.fields.truck.value && driver && _.isNumber(driver.truck)) {
      this.props.fields.truck.onChange({id: driver.truck});
    }
  }

  handleDriver2Select(driver2) {
    this.props.updateTrip({
      id: this.props.values.id,
      driver2: driver2,
      carrier: null,
    });
    this.props.fields.driver2.onChange(driver2);
    this.props.fields.carrier.onChange(null);
    if (!driver2) this.setState({showDriver2: false});
  }

  handleCarrierSelect(carrier) {
    this.props.updateTrip({
      id: this.props.values.id,
      carrier: carrier,
      driver: null,
      driver2: null,
      truck: null,
    });
    this.props.fields.carrier.onChange(carrier);
    this.props.fields.driver.onChange(null);
    this.props.fields.driver2.onChange(null);
    this.props.fields.truck.onChange(null);
  }

  handleTruckSelect(truck) {
    this.props.updateTrip({
      id: this.props.values.id,
      truck: truck,
    });
    this.props.fields.truck.onChange(truck);
  }

  handleDriverCreate(formState) {
    this.props.createDriver(formState).then(() => {
      this.refs.createDriverModal.close();
    });
  }

  render() {
    const {
      fields: { id, dispatcher, driver, driver2, carrier, truck},
    } = this.props;

    return (
      <Form>
        <div className="trip-form">
          <div className="trip-form__col-plus-load">
            <div className="trip-form__inner">
              <FormGroup>
                <button className="btn btn-primary"
                        title="Add load to trip"
                        onClick={this.props.onPlusLoadClick}>+ Load</button>
              </FormGroup>
            </div>
          </div>
          <div className="trip-form__col-id">
            <div className="trip-form__inner">
              <FormGroup>
                <Col componentClass={ControlLabel} sm={6} className="trip-from__label">Trip&nbsp;#</Col>
                <Col sm={6}>
                  <FormGroup>
                    <span className="form-static"> {id.value}</span>
                  </FormGroup>
                </Col>
              </FormGroup>
            </div>
          </div>
          <div className="trip-form__col-carrier-chbox">
            <div className="trip-form__inner">
              <FormGroup>
                <Checkbox className="trip-form__carrier-chbox" inline
                          checked={this.state.showCarrier}
                          onClick={() => this.setState({showCarrier: !this.state.showCarrier})}>Carrier</Checkbox>
              </FormGroup>
            </div>
          </div>
          {!this.state.showCarrier &&
            <div className="trip-form__col-drivers">
              <div className="trip-form__inner">
                <div className="trip-form__driver-label">
                  <label className="control-label">Drivers</label>
                  <div>
                    {!driver2.value && !this.state.showDriver2 &&
                    <button className="add-btn" onClick={() => this.setState({showDriver2: !this.state.showDriver2})}>Drivers+</button>
                    }
                  </div>
                </div>
                <div className="trip-form__driver-controls">
                  <FormGroup>
                    <SelectDriver
                      value={driver.value}
                      placeholder="Select driver"
                      clearable={false}
                      onSelect={::this.handleDriverSelect} />
                  </FormGroup>
                  {(driver2.value || this.state.showDriver2) &&
                  <FormGroup>
                    <SelectDriver
                      value={driver2.value}
                      placeholder="Select driver 2"
                      clearable={true}
                      onSelect={::this.handleDriver2Select} />
                  </FormGroup>
                  }
                </div>
                <div className="trip-form__driver-add">
                  <button className="btn btn-primary trip__add-driver"
                          onClick={() => this.refs.createDriverModal.open()}>+</button>
                  <BaseModal ref="createDriverModal"
                             header={'Add driver'}
                             options={{bsSize: 'lg'}}>
                    <DriverForm initialValues={{}}
                                onFormSubmit={::this.handleDriverCreate}
                                onCancel={() => this.refs.createDriverModal.close()} />
                  </BaseModal>
                </div>
              </div>
            </div>
          }
          {this.state.showCarrier &&
            <div className="trip-form__carrier">
              <div className="trip-form__inner">
                <FormGroup>
                  <Col componentClass={ControlLabel} sm={3} className="trip-from__label">Carrier</Col>
                  <Col sm={9}>
                    <FormGroup>
                      <SelectBroker
                        allowEmpty={true}
                        value={carrier.value}
                        onSelect={::this.handleCarrierSelect} />
                    </FormGroup>
                  </Col>
                </FormGroup>
              </div>
            </div>
          }
          {!this.state.showCarrier &&
            <div className="trip-form__col-truck">
              <Row className="trip-form__inner">
                <Col componentClass={ControlLabel} sm={3} className="trip-from__label">Truck&nbsp;#</Col>
                <Col sm={9}>
                  <SelectTruck
                    value={truck.value}
                    onSelect={::this.handleTruckSelect}/>
                </Col>
              </Row>
            </div>
          }
          <div className="trip-form__col-dispatcher">
            <div className="trip-form__dispatcher">
              <div className="trip-form__dispatcher-label">
                Dispatcher:
              </div>
              <div className="trip-form__dispatcher-name">
                {dispatcher.value && dispatcher.value.disp_name}
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'TripForm',
  fields: fields,
})(TripForm)
