import React, { PropTypes } from 'react';
import moment from 'moment';

export default class TruckDetails extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onClick: PropTypes.func
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
    <div className="form-horizontal details" onClick={this.props.onClick}>
      <div className="form-group">
        <label className="col-sm-3 control-label">Addr id</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.id}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Ttitle</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.title}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Phone</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.phone}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Mphone</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.mphone}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Fax</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.fax }</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Contact</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.contact}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Note</label>
        <Col sm={9}>
          <p className="form-control-static">{this.props.item.note}</p>
        </Col>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">Address</label>
        <Col sm={9}>
          <span><p className="form-control-static">{this.props.item.address}</p></span>
        </Col>
      </div>
    </div>
    )
  }
}
