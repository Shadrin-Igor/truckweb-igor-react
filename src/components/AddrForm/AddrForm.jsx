import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import _ from 'lodash';
import GoogleMap from 'google-map-react';

import Field from 'baseComponents/Field/Field';
import AddrExtendedFields from 'components/AddrForm/AddrExtendedFields';

const fields = ['id', 'title', 'phone', 'mphone', 'fax', 'contact', 'note',
  'addr_country', 'addr_state', 'addr_city', 'addr_postalcode', 'addr_street', 'addr_street2',
];

const validate = values => {
  const errors = {};

  // if (!values.addr_street) {
  //   errors.addr_street = 'Required'
  // }
  // if (!values.addr_postalcode) {
  //   errors.addr_postalcode = 'Required'
  // }

  return errors;
};

class AddrForm extends React.Component {
  static propTypes = {
    initialValues: PropTypes.object,
    fields: PropTypes.object.isRequired,
    createOrderAddress: PropTypes.func,
    updateOrderAddress: PropTypes.func,
    handleSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    onFormSubmit: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  handleFormSubmit(newState) {
    if (!_.isNumber(this.props.fields.id.value)) {
      // create
      this.props.createOrderAddress(newState).then(() => {
        this.props.onFormSubmit(newState);
      });
    }
    else {
      // update
      this.props.updateOrderAddress(newState).then(() => {
        this.props.onFormSubmit(newState);
      });
    }
  }

  render() {
    const {
      fields: { title, phone, mphone, fax, contact, note,
        addr_country, addr_state, addr_city, addr_postalcode, addr_street, addr_street2 },
      handleSubmit,
      submitting,
    } = this.props;

    return (
      <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <Row>
          <Col sm={6}>
            <FormGroup>
              <Col sm={12}>
                <div><label className="control-label">Title, company name etc.</label></div>
                <input type='text' className="form-control"
                       placeholder="Title, company name etc." {...title} />
              </Col>
            </FormGroup>

            <AddrExtendedFields dirty={this.props.dirty}
                                country={addr_country}
                                state={addr_state}
                                city={addr_city}
                                addr1={addr_street}
                                addr2={addr_street2}
                                postalcode={addr_postalcode} />

            <FormGroup>
              <Col sm={4}>
                <Field errorMsg={phone.error}>
                  <input type='text' className="form-control" {...phone}
                         placeholder="Phone" />
                </Field>
              </Col>
              <Col sm={4}>
                <Field errorMsg={mphone.error}>
                  <input type='text' className="form-control" {...mphone}
                         placeholder="Mobile" />
                </Field>
              </Col>
              <Col sm={4}>
                <Field errorMsg={fax.error}>
                  <input type='text' className="form-control" {...fax}
                         placeholder="Fax" />
                </Field>
              </Col>
            </FormGroup>

            <FormGroup>
              <Col sm={12}>
                <div><label className="control-label">Contact person</label></div>
                <input type='text' className="form-control"
                       placeholder="Contact person" {...contact} />
              </Col>
            </FormGroup>

            <FormGroup>
              <Col sm={12}>
            <textarea className="form-control" {...note}
                      placeholder="Note" />
              </Col>
            </FormGroup>

            <FormGroup className="form-buttons">
              <Col sm={12}>
                <Button bsStyle="default" onClick={this.props.onCancel}>Cancel</Button>
                <Button bsStyle="primary" className="pull-right" type="submit">Save</Button>
              </Col>
            </FormGroup>
          </Col>
          <Col sm={6}>
            <div style={{height: "300px"}}>
              <GoogleMap
                center={[38.0788336, -92.8672493]}
                zoom={4} />

            </div>
            <div>
              <p>Maybe you are looking for:</p>
              <ul className="list-unstyled">
                <li><a href="">5323 E McKinley Ave, Frecno, CA 93727</a></li>
                <li><a href="">5323 E McKinley Ave, Frecno, CA 93727</a></li>
                <li><a href="">5323 E McKinley Ave, Frecno, CA 93727</a></li>
              </ul>
            </div>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'AddrForm',
  fields: fields,
  validate,
})(AddrForm)
