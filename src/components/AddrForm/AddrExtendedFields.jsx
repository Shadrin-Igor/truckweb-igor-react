import React, { PropTypes } from 'react';

import Field from 'baseComponents/Field/Field';
import StateSelect from 'baseComponents/StateSelect/StateSelect';

export default class AddrExtendedFields extends React.Component {
  static propTypes = {
    dirty: PropTypes.bool,
    addr1: PropTypes.object.isRequired,
    addr2: PropTypes.object.isRequired,
    country: PropTypes.object.isRequired,
    city: PropTypes.object.isRequired,
    state: PropTypes.object.isRequired,
    postalcode: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { addr1, addr2, country, city, state, postalcode } = this.props;

    return (
      <div>
        <div>
          <div><label className="control-label">Address Line 1 (Street address)</label></div>
          <input type="text" className="form-control" placeholder="Street address" {...addr1} />
        </div>

        <Row className="form-row-delimiter">
          <Col sm={8}>
            <div><label className="control-label">Address Line 2</label></div>
            <input type="text" className="form-control" placeholder="# Apartment, unit, suite, building, floor, etc" {...addr2} />
          </Col>
          <Col sm={4}>
            <div><label className="control-label">Postalcode</label></div>
              <input type='text' className="form-control" {...postalcode}
                     placeholder="ZIP" />
          </Col>
        </Row>

        <Row className="form-row-delimiter">
          <Col sm={6}>
            <input type='text' className="form-control" {...city}
                   placeholder="City / Town" />
          </Col>
          <Col sm={3}>
            <StateSelect placeholder="State" {...state} name="state" />
          </Col>
          <Col sm={3}>
            <input type='text' className="form-control" {...country}
                   placeholder="Country" />
          </Col>
        </Row>

      </div>
    );
  }
}
