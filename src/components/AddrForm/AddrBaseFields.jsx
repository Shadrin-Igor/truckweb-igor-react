import React, { PropTypes } from 'react';

import Field from 'baseComponents/Field/Field';
import StateSelect from 'baseComponents/StateSelect/StateSelect';

export default class AddrBaseFields extends React.Component {
  static propTypes = {
    dirty: PropTypes.bool,
    street: PropTypes.object.isRequired,
    country: PropTypes.object.isRequired,
    city: PropTypes.object.isRequired,
    state: PropTypes.object.isRequired,
    postalcode: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { street, country, city, state, postalcode } = this.props;

    return (
      <div>
        <div>
          <FormGroup>
            <Col sm={12}>
              <Field errorMsg={(this.props.dirty) ? street.error : ''}>
                <textarea className="form-control" placeholder="Street" {...street} />
              </Field>
            </Col>
          </FormGroup>
        </div>

        <div className="block-delimiter">
          <FormGroup>
            <Col sm={6}>
              <input type='text' className="form-control" {...city}
                     placeholder="City / Town" />
            </Col>
            <Col sm={6}>
              <StateSelect placeholder="State" {...state} />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col sm={6}>
              <input type='text' className="form-control" {...country}
                     placeholder="Country" />
            </Col>
            <Col sm={6}>
              <Field errorMsg={(this.props.dirty) ? postalcode.error : ''}>
                <input type='text' className="form-control" {...postalcode}
                       placeholder="ZIP" />
              </Field>
            </Col>
          </FormGroup>
        </div>
      </div>
    );
  }
}
