import React, { PropTypes } from 'react';
import _ from 'lodash';

import BrockerBlock from 'baseComponents/table/BrockerBlock';
import AddressBlock from 'baseComponents/table/AddressBlock';
import DateDurationFormatter from 'baseComponents/formatters/DateDurationFormatter';

export default class FreightItem extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onSelect: PropTypes.func,
    onRemove: PropTypes.func,
    onCreateInvoice: PropTypes.func,
    allowActions: PropTypes.bool,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <tr>
        {/* Load# */}
        <td className="table-grid__load-col" onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block">
            <span className="mark-color">{this.props.item.reference}</span>
          </div>
        </td>

        {/* Trip */}
        <td className={!_.isNumber(this.props.item.trip) && 'freight__mark-with-trip'} onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block">{this.props.item.trip}</div>
        </td>

        {/* Broker/tel. */}
        <td className="table-grid__broker-col" onClick={() => this.props.onSelect(this.props.item)}>
          {this.props.item.broker && <div className="table-block" key={'brokers' + this.props.item.id}>
              <BrockerBlock broker={this.props.item.broker} />
            </div>
          }
        </td>

        {/* Origin Address */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          <div>
            {_.map(this.props.item.origins, (rpoint) => {
              return (
                <div className="table-block" key={'origins-rpoint' + rpoint.id}>
                  <AddressBlock rpoint={rpoint} />
                </div>
              )
            })}
          </div>
        </td>

        {/* Pickup Time */}
        <td className="table-grid__date-col" onClick={() => this.props.onSelect(this.props.item)}>
          <div>
            {_.map(this.props.item.origins, (rpoint) => {
              return (
                <div className="table-block" key={'origins-pick-rpoint' + rpoint.id}>
                  {rpoint.date && <DateDurationFormatter dateStart={rpoint.date} dateEnd={rpoint.date_end} />}
                </div>
              )
            })}
          </div>
        </td>

        {/* Destination Address */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          <div>
            {_.map(this.props.item.destinations, (rpoint) => {
              return (
                <div className="table-block" key={'destinations-rpoint' + rpoint.id}>
                  <AddressBlock rpoint={rpoint} />
                </div>
              )
            })}
          </div>
        </td>

        {/* Delivery Time */}
        <td className="table-grid__date-col" onClick={() => this.props.onSelect(this.props.item)}>
          <div>
            {_.map(this.props.item.destinations, (rpoint) => {
              return (
                <div className="table-block" key={'destinations-pick-rpoint' + rpoint.id}>
                  {rpoint.date && <DateDurationFormatter dateStart={rpoint.date} dateEnd={rpoint.date_end} />}
                </div>
              )
            })}
          </div>
        </td>

        {/* Control call time */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block">{this.props.item.cct}</div>
        </td>

        {/* Note */}
        <td onClick={() => this.props.onSelect(this.props.item)}>
          <div className="table-block font-size-90">
            <p title={this.props.item.note}>{this.props.item.note}</p>
          </div>
        </td>

        {/* Action */}
        {this.props.allowActions &&
          <td className="freight-item__actions">
            <DropdownButton id={`action-dropdown-${this.props.item.id}`} bsStyle={'link'} title="Action">
              <MenuItem eventKey="1" onClick={this.props.onCreateInvoice}>Create invoice</MenuItem>
              <MenuItem eventKey="2" onClick={this.props.onSelect.bind(this, this.props.item)}>Edit</MenuItem>
              <MenuItem eventKey="3" onClick={this.props.onRemove}>Delete</MenuItem>
            </DropdownButton>
          </td>
        }
      </tr>
    );
  }
}
