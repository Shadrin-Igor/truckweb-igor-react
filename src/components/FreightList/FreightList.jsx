import React, { PropTypes } from 'react';

import { confirm } from 'baseComponents/dialog/dialog';
import FreightItem from './FreightItem.jsx';

export default class FreightList extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
  };

  static propTypes = {
    list: PropTypes.array,
    allowActions: PropTypes.bool,
    allowCreate: PropTypes.bool,
    onSelect: PropTypes.func,
    myCompany: PropTypes.object,
    createdInvoice: PropTypes.object,
    freightCreated: PropTypes.object,
    updateList: PropTypes.func,
    getMyCompany: PropTypes.func,
    createInvoice: PropTypes.func,
    createFreight: PropTypes.func,
    deleteFreight: PropTypes.func,
  };

  static defaultProps = {
    allowActions: true,
    allowCreate: true,
  };

  constructor(props) {
    super(props);
  }

  handleCreate() {
    var newFreight = {};
    this.props.createFreight(newFreight).then(() => {
      if (!this.props.freightCreated.freight || _.isNaN(_.toNumber(this.props.freightCreated.freight.id)))
        throw new Error(`Error - freight haven't been created.`);

      var freightId = _.toNumber(this.props.freightCreated.freight.id);

      console.log(`---> Redirecting to /loads/${freightId}`);
      this.context.router.push(`/loads/${freightId}?new=true`);
    });
  }

  handleRemove(item, event) {
    event.stopPropagation();
    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to delete this load?',
      onOk: () => {
        this.props.deleteFreight(item).then(() => {
          this.props.updateList();
        });
      },
    });
  }

  handleCreateInvoice(item) {
    this.props.getMyCompany().then(() => {
      this.props.createInvoice(this.props.myCompany.company, {order: item.id}).then(() => {
        this.context.router.push(`/invoices/?edit=${this.props.createdInvoice.invoice.id}&back_url=${encodeURIComponent('/loads')}`);
      })
    });
  }

  render() {
    return (
      <div>
        {this.props.allowCreate &&
          <div className="list-toolbar clearfix">
            <button className="btn btn-primary pull-right" onClick={::this.handleCreate}>Create load</button>
          </div>
        }

        <table className="table table-bordered table-hover table-grid" cellSpacing="0" width="100%">
          <thead>
            <tr>
              <th>Load#</th>
              <th>Trip</th>
              <th>Broker/tel.</th>
              <th>Origin Address</th>
              <th>Pickup Time</th>
              <th>Destination Address</th>
              <th>Delivery Time</th>
              <th>Control call time</th>
              <th>Note</th>
              {this.props.allowActions &&
                <th>Action</th>
              }
            </tr>
          </thead>
          <tbody>
          {this.props.list.map((item) => {
            return (
              <FreightItem key={item.id}
                           item={item}
                           allowActions={this.props.allowActions}
                           onSelect={this.props.onSelect}
                           onCreateInvoice={this.handleCreateInvoice.bind(this, item)}
                           onRemove={this.handleRemove.bind(this, item)} />
            );
          })}
          </tbody>
        </table>
      </div>
    );
  }
}
