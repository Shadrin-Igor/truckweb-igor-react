import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import BrokerForm from 'components/BrokerForm/BrokerForm';

export default class BrokerList extends React.Component {
  static propTypes = {
    companyList: PropTypes.object,
    companyItem: PropTypes.object,
    onSelect: PropTypes.func,
    getCompany: PropTypes.func,
    getBrokerList: PropTypes.func,
    createCompany: PropTypes.func,
    updateCompany: PropTypes.func,
    deleteCompany: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.props.getBrokerList();

    this.state = {
      isItemLoading: false,
    }
  }

  handleEditClick(item, event) {
    event.stopPropagation();
    this.refs['editModal' + item.id].open();
    this.setState({isItemLoading: true});
    this.props.getCompany(item.id).then(() => {
      this.setState({isItemLoading: false});
    });
  }

  handleCreateSubmit(formState) {
    var newState = {
      ...formState,
      kind: 'broker',
    };
    this.props.createCompany(newState).then(() => {
      this.props.getBrokerList().then(() => {
        this.refs.addModal.close();
      });
    });
  }

  handleEditSubmit(item, formState) {
    this.props.updateCompany(formState).then(() => {
      this.props.getBrokerList().then(() => {
        this.refs['editModal' + item.id].close();
      });
    });
  }

  handleRemove(item, event) {
    event.stopPropagation();
    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to delete this broker?',
      onOk: () => {
        item.removing = true;
        this.forceUpdate();
        this.props.deleteCompany(item).then(() => {
          this.props.getBrokerList();
        });
      },
    });
  }

  _getBillingAddress(item) {
    var addr = _.find(item.address, (value) => {
      return value.addr_type === 1;
    });
    if (addr) return addr.addr_full;
  }

  render() {

    return (
      <div>
        <div>
          <div className="list-toolbar clearfix">
            <button className="btn btn-primary pull-right" onClick={() => this.refs.addModal.open()}>Add broker</button>
          </div>
          <BaseModal ref="addModal" header="Customer information" options={{bsSize: 'large'}}>
            <BrokerForm onFormSubmit={::this.handleCreateSubmit}
                        onCancel={() => this.refs.addModal.close()} />
          </BaseModal>
        </div>

        <table className="table table-bordered table-hover table-grid" cellSpacing="0" width="100%">
          <thead>
            <tr>
              <th>Brokers / Company</th>
              <th>Address / Phone</th>
              <th>Note</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.props.companyList.companies.map((item) => {
              return (
                <tr key={item.id} className="selectable-item" onClick={() => this.props.onSelect(item)}>
                  <td>{item.disp_name}</td>
                  <td>
                    {_.compact([this._getBillingAddress(item),
                      item.main_phone || item.attributes.mobphone]).join(' / ')}
                  </td>
                  <td>{item.note}</td>
                  <td className="broker-list__actions">
                    <button type="button" className="btn btn-default btn-xs"
                            onClick={this.handleEditClick.bind(this, item)}><i className="glyphicon glyphicon-pencil" /></button>

                    <BaseModal ref={'editModal' + item.id} header={'Customer information'}  options={{bsSize: 'large'}}>
                      {(this.state.isItemLoading) ?
                        <div>...loading</div>
                        :
                        <BrokerForm initialValues={this.props.companyItem.company}
                                    onFormSubmit={this.handleEditSubmit.bind(this, item)}
                                    onCancel={() => this.refs['editModal' + item.id].close()} />
                      }
                    </BaseModal>

                    <button type="button" className="btn btn-default btn-xs" onClick={this.handleRemove.bind(this, item)}>
                      <span className="glyphicon glyphicon-trash" /></button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>

      </div>
    );
  }
}
