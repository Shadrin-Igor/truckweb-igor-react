import React, { PropTypes } from 'react';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import TruckForm from 'components/TruckForm/TruckForm';
import moment from 'moment';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

export default class extends React.Component {
  static propTypes = {
    list: PropTypes.array,
    myCompany: PropTypes.object,
    truckItem: PropTypes.object,
    onSelect: PropTypes.func,
    getMyCompany: PropTypes.func,
    getTruck: PropTypes.func,
    createTruck: PropTypes.func,
    updateTruck: PropTypes.func,
    deleteTruck: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.props.getMyCompany().then(() => {
      this.props.getTruckList(this.props.myCompany.company);
    });

    this.state = {
      isItemLoading: false,
    };
  }

  handleEditClick(item, event) {
    event.stopPropagation();
    this.refs['editModal' + item.id].open();
    this.setState({isItemLoading: true});
    this.props.getTruck(this.props.myCompany.company, item.id).then(() => {
      this.setState({isItemLoading: false});
    });
  }

  handleCreateSubmit(formState) {
    this.props.createTruck(this.props.myCompany.company, formState).then(() => {
      this.refs.addModal.close();
    });
  }

  handleEditSubmit(item, formState) {
    this.props.updateTruck(this.props.myCompany.company, formState).then(() => {
      this.props.getTruckList(this.props.myCompany.company).then(() => {
        this.refs['editModal' + item.id].close();
      });
    });
  }

  handleRemove(item, event) {
    event.stopPropagation();

    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to delete this truck?',
      onOk: () => {
        item.removing = true;
        this.forceUpdate();
        this.props.deleteTruck(this.props.myCompany.company, item);
      },
    });
  }

  pdiFormat( cell, row ){
    return row.insurance_date ? moment(row.insurance_date).format('L') : '';
  }

  insvcFormat( cell, row ){
    return cell ? moment(cell).format('L') : '';
  }

  render() {

    var options = {
      onRowClick : ( row ) => this.props.onSelect(row),
      noDataText : '...'
    }

    return (
      <div>
        <div>
          <div className="list-toolbar clearfix">
            <button className="btn btn-primary pull-right" onClick={() => this.refs.addModal.open()}>New truck</button>
          </div>
          <BaseModal ref="addModal" header="Create new truck">
            <TruckForm onFormSubmit={::this.handleCreateSubmit}
                       onCancel={() => this.refs.addModal.close()} />
          </BaseModal>
        </div>

        <BootstrapTable tableHeaderClass="table-bordered table-hover table-grid margin-no"
                        data={this.props.list}
                        pagination={false}
                        exportCSV={false}
                        hover={true}
                        paginationShowsTotal={true}
                        ignoreSinglePage={true}
                        options={options}>
          <TableHeaderColumn width='100px' dataField="truckid" isKey={true}>Truck №</TableHeaderColumn>
          <TableHeaderColumn width='80px' dataField="year" >Year</TableHeaderColumn>
          <TableHeaderColumn dataField="make">Make</TableHeaderColumn>
          <TableHeaderColumn dataField="model">Model</TableHeaderColumn>
          <TableHeaderColumn dataField="license">License</TableHeaderColumn>
          <TableHeaderColumn dataField="vin">VIN</TableHeaderColumn>
          <TableHeaderColumn dataField="pdi" dataFormat={this.pdiFormat}>PDI</TableHeaderColumn>
          <TableHeaderColumn dataField="insvc" dataFormat={this.insvcFormat}>In Svc</TableHeaderColumn>
          <TableHeaderColumn width='60px' dataField="note" dataFormat={( cell, row )=>{
              return <span>
                      <button type="button" className="btn btn-default btn-xs"
                        onClick={this.handleEditClick.bind(this, row)}><i className="glyphicon glyphicon-pencil" /></button>
                      <button type="button" className="btn btn-default btn-xs" onClick={this.handleRemove.bind(this, row)}>
                            <span className="glyphicon glyphicon-trash" /></button>
                     </span>
          }}>Note</TableHeaderColumn>
        </BootstrapTable>

      </div>
    );
  }
}
