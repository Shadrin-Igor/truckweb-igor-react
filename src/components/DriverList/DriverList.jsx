import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import { confirm } from 'baseComponents/dialog/dialog';
import BaseModal from 'baseComponents/BaseModal/BaseModal';
import DriverForm from 'components/DriverForm/DriverForm';

export default class DriverList extends React.Component {
  static propTypes = {
    list: PropTypes.object,
    driverItem: PropTypes.object,
    onSelect: PropTypes.func,
    createDriver: PropTypes.func,
    updateDriver: PropTypes.func,
    deleteDriver: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      isItemLoading: false,
    }
  }

  handleEditClick(item, event) {
    event.stopPropagation();
    this.refs['editModal' + item.id].open();
    this.setState({isItemLoading: true});
    this.props.getDriver(item.id).then(() => {
      this.setState({isItemLoading: false});
    });
  }

  handleCreateSubmit(formState) {
    this.props.createDriver(formState).then(() => {
      this.refs.addModal.close();
    });
  }

  handleEditSubmit(item, formState) {
    this.props.updateDriver(formState).then(() => {
      this.refs['editModal' + item.id].close();
    });
  }

  handleRemove(item, event) {
    event.stopPropagation();

    // confirm delete
    confirm({
      okBtn: 'Delete',
      message: 'Do you really want to delete this driver?',
      onOk: () => {
        item.removing = true;
        this.forceUpdate();
        try {
          this.props.deleteDriver(item);
          // TODO: если произошла ошибка нужно item.removing сделать false или сделать это в редьюсере при обновлении
        } catch (err) {
          console.error(err);
        }
      },
    });
  }

  render() {
    return (
      <div>
        <div>
          <div className="list-toolbar clearfix">
            <button className="btn btn-primary pull-right" onClick={() => this.refs.addModal.open()}>Add driver</button>
          </div>
          <BaseModal ref="addModal" header="Create new driver">
            <DriverForm onFormSubmit={::this.handleCreateSubmit}
                        onCancel={() => this.refs.addModal.close()} />
          </BaseModal>
        </div>

        <table className="table table-bordered table-hover table-grid" cellSpacing="0" width="100%">
          <thead>
          <tr>
            <th>Drivers / Name</th>
            <th>Phone</th>
            <th>Truck</th>
            <th>Note</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          {this.props.list.map((item) => {
            return (
              <tr key={item.id} className="selectable-item" onClick={() => this.props.onSelect(item)}>
                <td>{item.disp_name}</td>
                <td>{item.attributes.mobile || item.attributes.phone}</td>
                <td>{item.truck && item.truck.truckid}</td>
                <td>{item.attributes.note}</td>
                <td className="driver-list__actions">
                  {/*<button className="btn btn-link">Create trip</button>*/}
                  <button type="button" className="btn btn-default btn-xs"
                          onClick={this.handleEditClick.bind(this, item)}><i className="glyphicon glyphicon-pencil" /></button>

                  <BaseModal ref={'editModal' + item.id} header={'Edit driver: ' + item.disp_name}>
                    {(this.state.isItemLoading) ?
                      <div>...loading</div>
                      :
                      <DriverForm initialValues={this.props.driverItem.driver}
                                  onFormSubmit={this.handleEditSubmit.bind(this, item)}
                                  onCancel={() => this.refs['editModal' + item.id].close()} />
                    }
                  </BaseModal>

                  <button type="button" className="btn btn-default btn-xs" onClick={this.handleRemove.bind(this, item)}>
                    <span className="glyphicon glyphicon-trash" /></button>
                </td>
              </tr>
            );
          })}
          </tbody>
        </table>

      </div>
    );
  }
}
