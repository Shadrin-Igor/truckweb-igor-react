import React, { PropTypes } from 'react';
import DateTimeField from 'react-bootstrap-datetimepicker';
import moment from 'moment';

export default class DateField extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  handleChange(time) {
    if (time.match(/^\d+$/)) {
      // It's timestamp
      this.props.onChange(moment(parseInt(time)).format('L'));
    }
    else {
      // It's time
      this.props.onChange(time);
    }
  }

  render() {
    return (
      <span className="date-field">
        {(this.props.disabled) ?
          <FormControl componentClass="input" disabled />
        :
          ((this.props.value) ?
            <DateTimeField inputFormat="MM/DD/YY"
                           format="L"
                           dateTime={moment(this.props.value).format('L')}
                           onChange={::this.handleChange} />
            :
            <DateTimeField inputFormat="MM/DD/YY"
                           defaultText=""
                           onChange={::this.handleChange} />
          )
        }
      </span>
    );
  }
}
