import React, { PropTypes } from 'react';

export default class AddressBlock extends React.Component {
  static propTypes = {
    rpoint: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.cellHeight = '48';
  }

  render() {
    return (
      <div>
        <div style={{height: this.cellHeight + 'px'}}>
          <div className="table-block-row">{this.props.rpoint.point.title}</div>
          <div className="table-block-row font-size-90 mark-color">{this.props.rpoint.point.addr_full}</div>
        </div>
      </div>
    );
  }
}
