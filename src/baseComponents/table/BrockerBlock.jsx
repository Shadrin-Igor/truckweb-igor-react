import React, { PropTypes } from 'react';

export default class BrockerBlock extends React.Component {
  static propTypes = {
    broker: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.cellHeight = '48';
  }

  render() {
    return (
      <div style={{height: this.cellHeight + 'px'}}>
        <div className="table-block-row">{this.props.broker.disp_name}</div>
        <div className="table-block-row font-size-90 mark-color">{this.props.broker.attributes.phone}</div>
      </div>
    );
  }
}
