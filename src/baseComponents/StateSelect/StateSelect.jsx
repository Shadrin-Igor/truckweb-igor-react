import React, { PropTypes } from 'react';
import _ from 'lodash';
import Regions from './Regions.jsx'

export default class StateSelect extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    value: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.states = Regions;
  }

  render() {
    return (
      <FormControl componentClass="select" placeholder={this.props.placeholder}
                   onChange={this.props.onChange}>
        <option default></option>
        {_.map(this.states, (value) => {
          return (<option value={value} selected={(value === this.props.value) ? 'selected' : undefined}>{value}</option>);
        })}
      </FormControl>
    );
  }
}
