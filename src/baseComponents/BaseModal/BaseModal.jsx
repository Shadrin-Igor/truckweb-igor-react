import React, { PropTypes } from 'react';
import { Modal } from 'react-bootstrap';
import _ from 'lodash';

export default class BaseModal extends React.Component {
  static propTypes = {
    header: PropTypes.string,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    options: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };

    this.sizeClass = '';
    this.parsedOptions = {};

    if (this.props.options) {
      if (this.props.options.bsSize == 'extra-large') {
        this.sizeClass = 'modal-extra-large';
      }
      else if (this.props.options.bsSize == 'medium') {
        this.sizeClass = 'modal-medium';
      }
      else {
        _.extend(this.parsedOptions, this.props.options);
      }
    }


  }

  open() {
    this.setState({ showModal: true });
    if (this.props.onOpen) this.props.onOpen();
  }

  close() {
    this.setState({ showModal: false });
    if (this.props.onClose) this.props.onClose();
  }

  render() {
    return (
      <Modal show={this.state.showModal}
             onHide={::this.close}
             {...this.parsedOptions}
             dialogClassName={this.sizeClass}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.header}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.props.children}
        </Modal.Body>
      </Modal>
    );
  }
}

