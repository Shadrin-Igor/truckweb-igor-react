import React, { PropTypes } from 'react';
import DateTimeField from 'react-bootstrap-datetimepicker';
import moment from 'moment';

export default class TimeField extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  handleChange(time) {
    if (time.match(/^\d+$/)) {
      // It's timestamp
      this.props.onChange(moment(parseInt(time)).format('LT'));
    }
    else {
      // It's time
      this.props.onChange(time);
    }
  }

  render() {
    return (
      <span className="time-field">
        {(this.props.disabled) ?
          <FormControl componentClass="input" disabled />
        :
          ((this.props.value) ?
            <DateTimeField inputFormat="h:mm A"
                           mode="time"
                           format="LT"
                           dateTime={this.props.value}
                           onChange={::this.handleChange} />
            :
            <DateTimeField inputFormat="h:mm A"
                           mode="time"
                           defaultText=""
                           onChange={::this.handleChange} />
          )
        }
      </span>
    );
  }
}
