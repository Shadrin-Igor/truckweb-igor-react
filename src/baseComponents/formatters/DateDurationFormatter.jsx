import React, { PropTypes } from 'react';
import moment from 'moment';

export default class DateDurationFormatter extends React.Component {
  static propTypes = {
    dateStart: PropTypes.string,
    dateEnd: PropTypes.string,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div>
          <span>{this.props.dateStart && moment(this.props.dateStart).format('L')}</span>
        </div>
        <div className="mark-color font-size-90">
          <span>{this.props.dateStart && moment(this.props.dateStart).format('LT')} </span>
          <span> {this.props.dateEnd && ' - '} </span>
          <span>{this.props.dateEnd && moment(this.props.dateEnd).format('LT')} </span>
        </div>
      </div>
    );
  }
}
