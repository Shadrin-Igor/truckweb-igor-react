import alertify from 'alertify.js';

export function confirm (params) {
  params.okBtn = params.okBtn || 'Ok';
  alertify.okBtn(params.okBtn).confirm(params.message, params.onOk, params.onCancel);
}
