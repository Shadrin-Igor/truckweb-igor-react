import React, { PropTypes } from 'react';

export default class Editable extends React.Component {
  static propTypes = {
    initialValue: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    name: PropTypes.string,
    title: PropTypes.string,
    disabled: PropTypes.bool,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    $(this.refs.editable).editable({
      //savenochange: true,
      //showbuttons: false,
      //validate
      mode: 'popup',
      emptytext: '-/-',
      value: this.props.value,
      title: this.props.title,
      disabled: this.props.disabled,
      type: 'text',
    }).on('save', this.handleChange.bind(this));
  }

  handleChange(event, params) {
    this.props.onChange(event.target.name, params.submitValue);
  }

  render() {
    $(this.refs.editable).editable('setValue', this.props.value);
    return (
      <a href='#'
         className='xeditable'
         name={this.props.name}
         title={this.props.title}
         ref="editable">
        {this.props.value}
      </a>
    );
  }
}
