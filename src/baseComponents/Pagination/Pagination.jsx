import React, { PropTypes } from 'react';

export default class Pagination extends React.Component {

  static propTypes = {
    totalCount: PropTypes.number,
    countPerPage: PropTypes.number,
    deleteDestination: PropTypes.func,
    getList: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1
    };
  }

  render (){

    var list=[];
    var countPage = Math.ceil( this.props.totalCount/this.props.countPerPage );
    for( let i=1;i<=countPage;i++ ){
      if( (!this.state.activePage && i==1 ) || this.state.activePage==i  )list.push( <li className='active'><span>{i}</span></li> );
      else list.push( <li><span onClick={() => { this.setState({activePage : i});this.props.getList(i)}} >{i}</span></li> );
    }

    return (
      <div className="text-right">
        <ul className="pagination">
          <li><span onClick={() => { if( this.state.activePage != 1 ){this.setState({activePage : 1});this.props.getList(1)}}} >«</span></li>
          {list.map( ( item )=>{
            return (
              item
              )
            }
            )}
          <li><span onClick={() => { if( this.state.activePage != countPage){this.setState({activePage : countPage});this.props.getList( countPage)}}} >»</span></li>
        </ul>
      </div>
    )
  }
}
