import React, { PropTypes } from 'react';
import { Popover } from 'react-bootstrap';
import classNames from 'classnames';

export default class Field extends React.Component {
  static propTypes = {
    errorMsg: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    }
  }

  open() {
    this.setState({ showModal: true });
    if (this.props.onOpen) this.props.onOpen();
  }

  close() {
    this.setState({ showModal: false });
    if (this.props.onClose) this.props.onClose();
  }

  render() {
    return (
      <div className="field">
        { this.props.errorMsg &&
          <div className="field__error-wrapper">
            <Popover id="field__error-popover" placement="right" className="field__error-popover">
              {this.props.errorMsg}
            </Popover>
          </div>
        }
        <div className={classNames({ 'field__field-error': this.props.errorMsg})}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
