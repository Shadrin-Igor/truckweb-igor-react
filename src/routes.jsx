import React from 'react';
import { Route, IndexRoute, Router } from 'react-router';

import { logout, checkUser } from 'redux/actions/auth';

import TightLayout from 'layout/TightLayout';
import WideLayout from 'layout/WideLayout';
import NotFound from 'containers/Main/NotFound';
import Home from 'containers/Main/Home';
import Login from 'containers/Main/Login';
import Logout from 'containers/Main/Logout';

import TripIndex from 'containers/Trip/TripIndex';
import TripEdit from 'containers/Trip/TripEdit';
import FreightIndex from 'containers/Freight/FreightIndex';
import FreightEdit from 'containers/Freight/FreightEdit';
import BrokersIndex from 'containers/Broker/BrokerIndex';
import BrokerView from 'containers/Broker/BrokerView';
import DriverIndex from 'containers/Driver/DriverIndex';
import DriverView from 'containers/Driver/DriverView';
import TruckIndex from 'containers/Truck/TruckIndex';
import TruckView from 'containers/Truck/TruckView';
import InvoiceIndex from 'containers/Invoice/InvoiceIndex';
import MapIndex from 'containers/Map/MapIndex';
import CompanySettings from 'containers/Company/CompanySettings';
import DestinationIndex from 'containers/Destination/DestinationIndex';
import DestinationView from 'containers/Destination/DestinationView';

export default class extends React.Component {
  requireLogin(nextState, replace, cb) {
    if (nextState.location.pathname == '/login' || nextState.location.pathname == 'logout') {
      cb();
      return;
    }

    // load user data
    this.props.store.dispatch(checkUser()).then(() => {
      // check for auth
      if (!this.props.store.getState().auth.isAuthenticated) {
        replace(nextState, '/login', {});
      }
      cb();
    }, () => {
      if (!this.props.store.getState().auth.isAuthenticated) {
        replace(nextState, '/login', {});
      }
      cb();
    });
  };

  render() {
    return (
      <Router history={this.props.history} ddd="ggg" onUpdate={this.props.onUpdate}>
        <Route path="/">
          { /* Tight pages */ }
          <Route component={TightLayout} fff="ggg" onEnter={::this.requireLogin}>
            <Route path='trips/:id' component={TripEdit} />
            <Route path='loads/:id' component={FreightEdit} />
            <Route path='brokers' component={BrokersIndex} />
            <Route path='brokers/:id' component={BrokerView} />
            <Route path='drivers' component={DriverIndex} />
            <Route path='drivers/:id' component={DriverView} />
            <Route path='trucks' component={TruckIndex} />
            <Route path='trucks/:id' component={TruckView} />
            <Route path='invoices' component={InvoiceIndex} />
            <Route path='destination' component={DestinationIndex} />
            <Route path='destination/:id' component={DestinationView} />
          </Route>


          { /* Wide pages */ }
          <Route component={WideLayout} onEnter={::this.requireLogin}>
            { /* Home (main) route */ }
            <IndexRoute component={Home}/>

            <Route path='trips' component={TripIndex} />
            <Route path='loads' component={FreightIndex} />
            <Route path='map' component={MapIndex} />
          </Route>

          { /* Common pages */ }
          <Route component={TightLayout}>
            <Route path='login' component={Login} />
            <Route path='logout' component={Logout} />
          </Route>
        </Route>

        { /* Utils pages */ }
        <Route path="" component={TightLayout} onEnter={::this.requireLogin}>
          <Route path="*" component={NotFound} status={404} />
        </Route>

      </Router>
    );
  }

};
