# Dockerfile for build production static files
FROM node:5.11.1

LABEL version="0.0.3"

ADD ./ /repo

RUN cd /repo && \
    npm install --production

VOLUME ["/repo/public/prod"]
CMD cd /repo; npm run prod
