// Webpack config for development
process.env.NODE_ENV = 'development';
var fs = require('fs');
var path = require('path');
var WebpackShellPlugin = require('webpack-shell-plugin');
var envConfig = require('config');

var assetsPath = path.resolve(__dirname, envConfig.get('buildDir'));
var wpCommon = require('./webpack_common.js');

module.exports = {
  plugins: wpCommon.plugins.concat([
    new WebpackShellPlugin({
      onBuildStart: [
        `mkdir -p ${assetsPath}`,
        `cd ${assetsPath}; ln -s ${path.resolve(__dirname, './public/assets')} 2> /dev/null; exit 0`,
      ],
    }),
  ]),
  cache: true,
  output: {
    path: assetsPath,
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js'
  },

  devtool: wpCommon.devtool,
  entry: wpCommon.entry,
  module: wpCommon.module,
  resolve: wpCommon.resolve,
  externals: wpCommon.externals,
};
