var WebpackDevServer = require('webpack-dev-server');
var webpack = require('webpack');
var _ = require('lodash');
var envConfig = require('config');

var config = require('./webpack.dev.config.js');

var babelQuery = {
  'cacheDirectory': true,
  'env': {
    'development': {
      'plugins': [
        ['react-transform', {
          'transforms': [
            {
              'transform': 'react-transform-hmr',
              'imports': ['react'],
              'locals': ['module']
            },
            {
              'transform': 'react-transform-catch-errors',
              'imports': ['react', 'redbox-react']
            },
          ]
        }]
      ]
    }
  }
};

// reconfigure webpack dev config
config.plugins.push(new webpack.HotModuleReplacementPlugin());
config.output.publicPath = '/';
config.entry.app = [
  `webpack-dev-server/client?http://${envConfig.get('devServerHost')}:${envConfig.get('devServerPort')}/`,
  'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
].concat(config.entry.app);

// add react-hot
config.module.loaders = config.module.loaders.map(function (item) {
  if (_.isArray(item.loaders) && item.loaders[0].match(/babel/)) {
    item.loaders = ['react-hot', 'babel?' + JSON.stringify(babelQuery)];
  }
  return item;
});

var compiler = webpack(config);
var server = new WebpackDevServer(compiler, {
  hot: true,
  inline: true,

  // Set this as true if you want to access dev server from arbitrary url.
  // This is handy if you are using a html5 router.
  historyApiFallback: false,

  contentBase: 'public/dev',

  quiet: false,
  noInfo: false,
  lazy: false,
  stats: {
    colors: true,
    progress: true,
  },
});

server.listen(envConfig.get('devServerPort'), envConfig.get('devServerHost'), function(err, result) {
  if (err) {
    console.error(err);
  }

  console.log('Webpack Dev Server started. Compiling...');
  console.log(`Open http://${envConfig.get('devServerHost')}:${envConfig.get('devServerPort')} in your browser.`);
});
