var envConfig = require('config');
var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
require('babel-polyfill');

var packageJson = require('./package.json');

module.exports = {
  module: {
    loaders: [
      {
        loaders: ['babel?cacheDirectory=true'],
        test: /\.jsx?$/,
        include: [
          path.resolve(__dirname, 'src'),
        ],
      },
      {
        test: /\.scss$/,
        loader: 'style!css?sourceMap!sass?sourceMap',
        include: [
          path.resolve(__dirname, 'src'),
          path.resolve(__dirname, 'node_modules/bootstrap-sass'),
          path.resolve(__dirname, 'node_modules/font-awesome'),
        ],
      },
      { test: /\.css$/, loader: "style-loader!css-loader" },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loaders: [
          'file?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
        ]
      },
      { test: /\.woff[a-zA-Z0-9\?#&\-=\.]*$/, loader: "url?limit=1000000&mimetype=application/font-woff" },
      { test: /\.woff2[a-zA-Z0-9\?#&\-=\.]*$/, loader: "url?limit=1000000&mimetype=application/font-woff" },
      { test: /\.ttf[a-zA-Z0-9\?#&\-=\.]*$/, loader: "url?limit=1000000&mimetype=application/octet-stream" },
      { test: /\.svg[a-zA-Z0-9\?#&\-=\.]*$/, loader: "url?limit=1000000&mimetype=image/svg+xml" },
      { test: /\.eot[a-zA-Z0-9\?#&\-=\.]*$/, loader: "file" },
    ],
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js"),
    new HtmlWebpackPlugin({
      title: 'Trucks',
      template: './src/index.html.ejs',
      // TODO: add minifier for prod
      //minify: {...} | false Pass a html-minifier options object to minify the output.
      data: {
        version: packageJson.version,
      },
    }),
    new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js"),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      __APP_CONFIG__: JSON.stringify(envConfig.util.loadFileConfigs()),
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }
    }),
  ].concat(
    (envConfig.get('minimize')) ? [new webpack.optimize.DedupePlugin()] : [],
    (envConfig.get('minimize')) ? [new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })] : []
  ),
  devtool: (envConfig.get('sourceMaps')) ? 'inline-source-map' : undefined,
  entry: {
    app: [
      'babel-polyfill',
      './src/app.jsx',
    ],
    styles: [
      './src/stylesheets.jsx',
    ],
    vendor: [
      './src/vendor.js',
    ],
  },
  resolve: {
    modulesDirectories: [
      './src',
      './node_modules',
    ],
    extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx'],
    alias: {
      // TODO: really need???
      //'redux-async-connect': path.resolve('./node_modules/redux-async-connect/lib/index.js'),
    },
  },
  externals: {
  },
};
