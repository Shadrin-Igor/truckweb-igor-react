#!/usr/bin/env bash

CONTAINER_NAME=truckviewer-prod-nginx
BUILD_PATH=${1-$(pwd)/public/build/}

docker rm -f ${CONTAINER_NAME}
docker run --name ${CONTAINER_NAME} -ti \
    -v ${BUILD_PATH}:/etc/nginx/html:ro \
    nginx:stable-alpine
