# Trucks web ui

## Install

### Prepare

    # Install nvm
    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
    
    # Download and use necessary node version
    nvm install
    nvm use
    

### Install project

    # Install node and bower dependencies
    npm install

## Config

Copy `./config/default.yaml.example` to `./config/default.yaml` and make same for production.yaml


## Run

Run `nvm use` before start the command to switch to necessary node version.

### Development build with dev server

    npm start

After, open [http://localhost:3001](http://localhost:3001).
Dev server doesn't write built files to disk, it operates with them in memory.


### Production build

    npm run prod

It writes compiled files to `public/prod`

For checking this build, you can install `npm install http-server -g`, and run

     http-server ./public/prod


### Other commands

Run development build with watcher, without dev server. See built files in `public/dev`
 
    npm run watch


## Production docker container

To build app inside a container, run

    ./prod-build.sb

It builds to `public/build`, or you can specify your build path

    ./prod-build.sb /build/to

To serve run: (not working at now)

    ./prod-serve.sh /path/to/built/files


## Development

For dialogs used [https://alertifyjs.org](https://alertifyjs.org)
